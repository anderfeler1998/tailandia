<?php
session_start();
require_once __DIR__."/./vendor/autoload.php";
require_once __DIR__."/./routes.php";
use Pecee\SimpleRouter\SimpleRouter as Router;

/* Load external routes file */
// require_once 'routes.php';

/**
 * The default namespace for route-callbacks, so we don't have to specify it each time.
 * Can be overwritten by using the namespace config option on your routes.
 */
Router::get('/', function() {
    require_once './app/view/inicio.php';
});
Router::get('/que_es_la_cocina_tailandesa/{idIconico?}', function($idIconico=false) {
    require_once './app/view/2.php';
});
Router::get('/el_arte_de_la_cocina_tailandesa', function() {
    require_once './app/view/3.php';
});
Router::get('/receta/{idReceta}', function($idReceta){
    require_once './app/view/4.php';
});
Router::get('/turismo_gastronomico_en_tailandia/{idRuta?}', function($idRuta=false) {
    require_once './app/view/5.php';
});
Router::get('/noticias/{pagina?}', function($pagina=1) {
    require_once './app/view/6.php';
})->where(['pagina'=>'[0-9]+']);
Router::get('/noticia/{idNoticia}', function($idNoticia) {
    require_once './app/view/7.php';
});
Router::get('/contactanos', function() {
    require_once './app/view/11.php';
});
// Router::group(['defaultParameterRegex' => '[\w\-]+'], function() {
Router::get('/buscador', function() {
    require_once './app/view/12.php';
});
Router::group(['prefix' => '/admin'], function () {
    Router::get('/', function ()    {
        require_once './app/view/admin_login.php';
    });
    Router::get('/gestion', function ()    {
        require_once './app/view/admin_recetas.php';
    });
});
// });
// Start the routing
Router::start();