<?php
   require_once "../../model/Cn.php";
   $ini=$_POST["pageSize"]*($_POST["pageNumber"]-1);
   $cantidad=$_POST["pageSize"];

   $cn=new Cn();
   $mysqli=$cn->conectar();
   $stm=$mysqli->prepare("call z_readContactanos(?,?)");
   $stm->bind_param("ii",$init,$cantidad);
   $stm->execute();
   $array=[];
   if($stm->{'error'}==""){
      $rs=$stm->get_result();
      if(($rs->num_rows)>0){
            while($row=$rs->fetch_assoc()){
               $array[]=$row;
            }
      }else{
            $array=[
               "error"=>"Sin Registros"
            ];
      }
   }else{
      $array["error"]=$stm->{'error'};
   }
   echo $json=json_encode($array,JSON_FORCE_OBJECT);
?>