<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    // Load Composer's autoloader
    require_once "../../vendor/autoload.php";
    date_default_timezone_set('America/Lima');
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;//SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'servicio.entrega.lib@gmail.com';                     // SMTP username
        $mail->Password   = 'servicioq2w3e4';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('servicio.entrega.lib@gmail.com', 'INFORME DE CONTACTO');
        $mail->addAddress('thaiemb@foodlandia.com.pe', 'Foodlandia');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'CONTACTO '.date("d-m-Y H:i:s");  ;
        $mail->Body    = 'NOMBRE(S): '.$_POST["txtNombres"]."<br>".'APELLIDOS: '.$_POST["txtApellidos"]."<br>".'CORREO: '.$_POST["txtCorreo"]."<br>".'MENSAJE: '.$_POST["txtDescripcion"]."";
        $mail->AltBody = 'Tailandia';

        $mail->send();
        $array["res"]="OK";
        // echo '';
    } catch (Exception $e) {
        // echo "Hubo un error al enviar: {$mail->ErrorInfo}";
        $array=["error"=>$mail->ErrorInfo];
    }
    echo json_encode($array);

    // require_once "../model/Cn.php";
    // $cn=new Cn();
    // $mysqli=$cn->conectar();
    // $stm=$mysqli->prepare("CALL z_registrarContactanos(?,?,?,?)");
    // $stm->bind_param("ssss",$_POST["txtNombres"],$_POST["txtApellidos"],$_POST["txtCorreo"],$_POST["txtDescripcion"]);
    // $stm->execute();
    // $array=[];
    // if($stm->{'error'}==""){
    //     if($stm->{'field_count'}>0)
    //     {
    //         $array["res"]="OK";
    //     }else{
    //         $array["error"]="No se pudo registrar";
    //     }
    // }else{
    //     $array=[
    //         "error"=>$stm->{'error'}
    //         ];
    // }
    // echo json_encode($array);
    
?>
