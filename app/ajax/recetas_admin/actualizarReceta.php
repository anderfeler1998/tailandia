<?php
    // var_dump($_POST);
    require_once "../../model/Cn.php";
    require_once "../../model/Recetas.php";
    $o=new Recetas();
    $imgPlato="";
    $imgPlatoReal="";
    $imgReceta="";
    $imgRecetaReal="";
    $imgPresentacion="";
    $imgPresentacionReal="";
    $tipFile=0;

    if($_POST["img1"]){
        $nombre_archivo=uniqid();
        $arr=explode("/",$_FILES[0]["type"]);
        $imgPlato=$nombre_archivo.".".$arr[1];
        $imgPlatoReal=$_FILES[0]["name"];
        if (move_uploaded_file($_FILES[0]['tmp_name'], "../../../public/img/data/".$imgPlato)){
            // echo "El archivo ha sido cargado correctamente.";
        }
    }
    if($_POST["img2"]){
        $nombre_archivo=uniqid()."1";
        $arr=explode("/",$_FILES[1]["type"]);
        $imgReceta=$nombre_archivo.".".$arr[1];
        $imgRecetaReal=$_FILES[1]["name"];
        if (move_uploaded_file($_FILES[1]['tmp_name'], "../../../public/img/data/".$imgReceta)){
            // echo "El archivo ha sido cargado correctamente.";
        }
    }

    if($_POST["img3"]){
        $nombre_archivo=uniqid()."2";
        $arr=explode("/",$_FILES[2]["type"]);
        $imgPresentacion=$nombre_archivo.".".$arr[1];
        if($arr[1]=='mp4'){
            $tipFile=1;
        }
        $imgPresentacionReal=$_FILES[2]["name"];
        if (move_uploaded_file($_FILES[2]['tmp_name'], "../../../public/presentacion/".$imgPresentacion)){
            // echo "El archivo ha sido cargado correctamente.";
        }
    }else{

    }

    echo $o->actualizarReceta(
        $_POST["id"],
        $_POST["txtTitulo1"],
        $_POST["txtTitulo2"],
        $_POST["txtSubTitulo"],
        $_POST["txtDuracion"],
        $_POST["cboPersonas"],
        $_POST["txtIngredientes1"],
        $_POST["txtIngredientes2"],
        $_POST["txtPreparacion"],
        $_POST["img1"],
        $imgPlato,
        $imgPlatoReal,
        $_POST["img2"],
        $imgReceta,
        $imgRecetaReal,
        $_POST["img3"],
        $imgPresentacion,
        $imgPresentacionReal,
        $tipFile,
        $_POST["txtUrl"]
    );
?>