<?php
    // var_dump($_POST);
    require_once "../../model/Cn.php";
    require_once "../../model/Recetas.php";
    $o=new Recetas();

    $nombre_archivo=time().uniqid();
    $arr=explode("/",$_FILES[0]["type"]);
    $imgPlato=$nombre_archivo.".".$arr[1];
    $imgPlatoReal=$_FILES[0]["name"];
    if (move_uploaded_file($_FILES[0]['tmp_name'], "../../../public/img/data/".$imgPlato)){
        // echo "El archivo ha sido cargado correctamente.";
    }

    $nombre_archivo=time().uniqid()."1";
    $arr=explode("/",$_FILES[1]["type"]);
    $imgReceta=$nombre_archivo.".".$arr[1];
    $imgRecetaReal=$_FILES[1]["name"];
    if (move_uploaded_file($_FILES[1]['tmp_name'], "../../../public/img/data/".$imgReceta)){
        // echo "El archivo ha sido cargado correctamente.";
    }

    $nombre_archivo=time().uniqid()."2";
    $arr=explode("/",$_FILES[2]["type"]);
    $imgPresentacion=$nombre_archivo.".".$arr[1];
    $tipFile=0;
    if($arr[1]=='mp4'){
        $tipFile=1;
    }
    $imgPresentacionReal=$_FILES[2]["name"];
    if (move_uploaded_file($_FILES[2]['tmp_name'], "../../../public/presentacion/".$imgPresentacion)){
        // echo "El archivo ha sido cargado correctamente.";
    }

    echo $o->nuevaReceta($_POST["txtTitulo1"],
                    $_POST["txtTitulo2"],
                    $_POST["txtSubTitulo"],
                    $_POST["txtDuracion"],
                    $_POST["cboPersonas"],
                    $_POST["txtIngredientes1"],
                    $_POST["txtIngredientes2"],
                    $_POST["txtPreparacion"],
                    $imgPlato,
                    $imgPlatoReal,
                    $imgReceta,
                    $imgRecetaReal,
                    $imgPresentacion,
                    $imgPresentacionReal,
                    $tipFile,
                    $_POST["txtUrl"]
                );
?>