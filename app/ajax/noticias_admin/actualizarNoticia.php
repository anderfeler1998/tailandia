<?php
    // var_dump($_POST);
    require_once "../../model/Cn.php";
    require_once "../../model/Noticias.php";
    $o=new Noticias();
    $imgPlato="";
    $imgPlatoReal="";
    $imgReceta="";
    $imgRecetaReal="";
    $imgPresentacion="";
    $imgPresentacionReal="";
    $tipFile=0;

    if($_POST["img1"]){
        $nombre_archivo=uniqid();
        $arr=explode("/",$_FILES[0]["type"]);
        $imgPlato=$nombre_archivo.".".$arr[1];
        $imgPlatoReal=$_FILES[0]["name"];
        if (move_uploaded_file($_FILES[0]['tmp_name'], "../../../public/img/data/".$imgPlato)){
            // echo "El archivo ha sido cargado correctamente.";
        }
    }
    if($_POST["img2"]){
        $nombre_archivo=uniqid()."1";
        $arr=explode("/",$_FILES[1]["type"]);
        $imgReceta=$nombre_archivo.".".$arr[1];
        $imgRecetaReal=$_FILES[1]["name"];
        if (move_uploaded_file($_FILES[1]['tmp_name'], "../../../public/img/data/".$imgReceta)){
            // echo "El archivo ha sido cargado correctamente.";
        }
    }

    echo $o->actualizarNoticia(
        $_POST["id"],
        $_POST["txtTitulo"],
        $_POST["txtSubtitulo"],
        $_POST["txtFecha"],
        $_POST["txtDescripcion"],
        $_POST["img1"],
        $imgPlato,
        $imgPlatoReal,
        $_POST["img2"],
        $imgReceta,
        $imgRecetaReal
    );
?>