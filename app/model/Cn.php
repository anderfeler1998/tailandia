<?php
require_once $_SERVER['DOCUMENT_ROOT']."/app/config/config.php";
class Cn
{
    public $cn;
    private $host;
    private $db;
    private $user;
    private $pass;

    public function __construct(){
        $this->host=DB_HOST;
        $this->db=DB_NAME;
        $this->user=DB_USER;
        $this->pass=DB_PASS;
    }

    public function conectar(){
        $this->cn= new mysqli($this->host,$this->user,$this->pass,$this->db,null,null);
        mysqli_set_charset( $this->cn, 'utf8');
        return $this->cn;
    }
    
     /* Fin General */



    /**
     * Get the value of cn
     */ 
    public function getCn()
    {
        return $this->cn;
    }

    /**
     * Set the value of cn
     *
     * @return  self
     */ 
    public function setCn($cn)
    {
        $this->cn = $cn;

        return $this;
    }

    /**
     * Get the value of host
     */ 
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set the value of host
     *
     * @return  self
     */ 
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get the value of db
     */ 
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Set the value of db
     *
     * @return  self
     */ 
    public function setDb($db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of pass
     */ 
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set the value of pass
     *
     * @return  self
     */ 
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }
}
