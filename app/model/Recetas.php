<?php
   
   Class Recetas{
    private $id;
    private $preTtitulo;
    private $titulo;
    private $subTtitulo;
    private $duracion;
    private $personas;
    private $rightIngredientes;
    private $leftIngredientes;
    private $preparacion;
    private $idImgPlato;
    private $idImgReceta;
    private $idVideos;
    private $estado;
    
    public function readReceta(){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readReceta(?)");
        $stm->bind_param("i",$this->id);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if($rs->num_rows>0){
                $array=$rs->fetch_assoc();
            }else{
                $array["error"]="Sin registros";
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function readRecetas(){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readRecetas()");
        // $stm->bind_param("i",$this->id);
        $stm->execute();
        // $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if(($rs->num_rows)>0){
                while($row=$rs->fetch_assoc()){
                    $array[]=$row;
                }
            }else{
                $array=[
                    "error"=>"Sin Registros"
                ];
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    function readBusqueda($busqueda){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readBusqueda(?)");
        $stm->bind_param("s",$busqueda);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if(($rs->num_rows)>0){
                while($row=$rs->fetch_assoc()){
                    $array[]=$row;
                }
            }else{
                $array=[
                    "error"=>"Sin Registros"
                ];
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function readAdminNumberRecetas(){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("SELECT count(*) as cantidad FROM recetas");
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if($rs->num_rows>0){
                $array=$rs->fetch_assoc();
            }else{
                $array["error"]="Sin registros";
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }
    public function readAdminRecetas($init,$cantidad){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readRecetasAdmin(?,?)");
        $stm->bind_param("ii",$init,$cantidad);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if(($rs->num_rows)>0){
                while($row=$rs->fetch_assoc()){
                    $array[]=$row;
                }
            }else{
                $array=[
                    "error"=>"Sin Registros"
                ];
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function readAdminReceta(){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readRecetaAdmin(?)");
        $stm->bind_param("i",$this->id);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if($rs->num_rows>0){
                $array=$rs->fetch_assoc();
            }else{
                $array["error"]="Sin registros";
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function nuevaReceta($txtTitulo1,
    $txtTitulo2,
    $txtSubTitulo,
    $txtDuracion,
    $cboPersonas,
    $txtIngredientes1,
    $txtIngredientes2,
    $txtPreparacion,
    $imgPlato,
    $imgPlatoReal,
    $imgReceta,
    $imgRecetaReal,
    $imgPresentacion,
    $imgPresentacionReal,
    $tipFile,
    $txtUrl){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call nuevaReceta(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stm->bind_param("ssssisssssssssis",
        $txtTitulo1,
        $txtTitulo2,
        $txtSubTitulo,
        $txtDuracion,
        $cboPersonas,
        $txtIngredientes1,
        $txtIngredientes2,
        $txtPreparacion,
        $imgPlato,
        $imgPlatoReal,
        $imgReceta,
        $imgRecetaReal,
        $imgPresentacion,
        $imgPresentacionReal,
        $tipFile,
        $txtUrl
    );
        $stm->execute();
        if($stm->{'error'}==""){
            if($stm->{'field_count'}>0)
            {
                    $array["res"]="OK";
            }else{
                $array["error"]="No se pudo registrar";
            }
        }else{
            $array=[
                "error"=>$stm->{'error'}
                ];
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function actualizarReceta(
    $id,
    $txtTitulo1,
    $txtTitulo2,
    $txtSubTitulo,
    $txtDuracion,
    $cboPersonas,
    $txtIngredientes1,
    $txtIngredientes2,
    $txtPreparacion,
    $img1,
    $imgPlato,
    $imgPlatoReal,
    $img2,
    $imgReceta,
    $imgRecetaReal,
    $img3,
    $imgPresentacion,
    $imgPresentacionReal,
    $tipFile,
    $txtUrl){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call actualizarReceta(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stm->bind_param("issssisssissississis",
            $id,
            $txtTitulo1,
            $txtTitulo2,
            $txtSubTitulo,
            $txtDuracion,
            $cboPersonas,
            $txtIngredientes1,
            $txtIngredientes2,
            $txtPreparacion,
            $img1,
            $imgPlato,
            $imgPlatoReal,
            $img2,
            $imgReceta,
            $imgRecetaReal,
            $img3,
            $imgPresentacion,
            $imgPresentacionReal,
            $tipFile,
            $txtUrl
        );
        $stm->execute();
        if($stm->{'error'}==""){
            if($stm->{'field_count'}>0)
            {
                    $array["res"]="OK";
            }else{
                $array["error"]="No se pudo registrar";
            }
        }else{
            $array=[
                "error"=>$stm->{'error'}
                ];
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function eliminarReceta($id){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call eliminarReceta(?)");
        $stm->bind_param("i",$id);
        $stm->execute();
        if($stm->{'error'}==""){
            if($stm->{'field_count'}>0)
            {
                    $array["res"]="OK";
            }else{
                $array["error"]="No se pudo registrar";
            }
        }else{
            $array=[
                "error"=>$stm->{'error'}
                ];
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }
    
    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set the value of preTtitulo
     *
     * @return  self
     */ 
    public function setPreTtitulo($preTtitulo)
    {
        $this->preTtitulo = $preTtitulo;

        return $this;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Set the value of subTtitulo
     *
     * @return  self
     */ 
    public function setSubTtitulo($subTtitulo)
    {
        $this->subTtitulo = $subTtitulo;

        return $this;
    }

    /**
     * Set the value of duracion
     *
     * @return  self
     */ 
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Set the value of personas
     *
     * @return  self
     */ 
    public function setPersonas($personas)
    {
        $this->personas = $personas;

        return $this;
    }

    /**
     * Set the value of rightIngredientes
     *
     * @return  self
     */ 
    public function setRightIngredientes($rightIngredientes)
    {
        $this->rightIngredientes = $rightIngredientes;

        return $this;
    }

    /**
     * Set the value of leftIngredientes
     *
     * @return  self
     */ 
    public function setLeftIngredientes($leftIngredientes)
    {
        $this->leftIngredientes = $leftIngredientes;

        return $this;
    }

    /**
     * Set the value of preparacion
     *
     * @return  self
     */ 
    public function setPreparacion($preparacion)
    {
        $this->preparacion = $preparacion;

        return $this;
    }

    /**
     * Set the value of idImgPlato
     *
     * @return  self
     */ 
    public function setIdImgPlato($idImgPlato)
    {
        $this->idImgPlato = $idImgPlato;

        return $this;
    }

    /**
     * Set the value of idImgReceta
     *
     * @return  self
     */ 
    public function setIdImgReceta($idImgReceta)
    {
        $this->idImgReceta = $idImgReceta;

        return $this;
    }

    /**
     * Set the value of idVideos
     *
     * @return  self
     */ 
    public function setIdVideos($idVideos)
    {
        $this->idVideos = $idVideos;

        return $this;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
   }

?>