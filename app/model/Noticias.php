<?php
   Class Noticias {
    private $id;
    private $nombre;
    private $detalles;
    private $idImagen1;
    private $idImagen2;
    private $fecIngreso;
    private $estado;
    
    public function readNoticias($init,$cantidad){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readNoticias(?,?)");
        $stm->bind_param("ii",$init,$cantidad);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if(($rs->num_rows)>0){
                while($row=$rs->fetch_assoc()){
                    $array[]=$row;
                }
            }else{
                $array=[
                    "error"=>"Sin Registros"
                ];
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function countReg(){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("select COUNT(*) AS cantidad from noticias;");
        // $stm->bind_param("ii",$init,$cantidad);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if(($rs->num_rows)>0){
                while($row=$rs->fetch_assoc()){
                    $array=$row;
                }
            }else{
                $array=[
                    "error"=>"Sin Registros"
                ];
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function readNoticia($id){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call readNoticia(?)");
        $stm->bind_param("i",$id);
        $stm->execute();
        $array=[];
        if($stm->{'error'}==""){
            $rs=$stm->get_result();
            if(($rs->num_rows)>0){
                while($row=$rs->fetch_assoc()){
                    $array=$row;
                }
            }else{
                $array=[
                    "error"=>"Sin Registros"
                ];
            }
        }else{
            $array["error"]=$stm->{'error'};
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function nuevaNoticia($txtTitulo,
    $txtSubTitulo,
    $txtFecha,
    $txtDetalle,
    $img1,
    $img1Real,
    $img2,
    $img2Real){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call nuevaNoticia(?,?,?,?,?,?,?,?)");
        $stm->bind_param("ssssssss",
        $txtTitulo,
        $txtSubTitulo,
        $txtFecha,
        $txtDetalle,
        $img1,
        $img1Real,
        $img2,
        $img2Real
    );
        $stm->execute();
        if($stm->{'error'}==""){
            if($stm->{'field_count'}>0)
            {
                    $array["res"]="OK";
            }else{
                $array["error"]="No se pudo registrar";
            }
        }else{
            $array=[
                "error"=>$stm->{'error'}
                ];
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function actualizarNoticia($id,
    $txtTitulo,
    $txtSubTitulo,
    $txtFecha,
    $txtDetalle,
    $i1,
    $img1,
    $img1Real,
    $i2,
    $img2,
    $img2Real){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call actualizarNoticia(?,?,?,?,?,?,?,?,?,?,?)");
        $stm->bind_param("issssississ",
        $id,
        $txtTitulo,
        $txtSubTitulo,
        $txtFecha,
        $txtDetalle,
        $i1,
        $img1,
        $img1Real,
        $i2,
        $img2,
        $img2Real
    );
        $stm->execute();
        if($stm->{'error'}==""){
            if($stm->{'field_count'}>0)
            {
                    $array["res"]="OK";
            }else{
                $array["error"]="No se pudo registrar";
            }
        }else{
            $array=[
                "error"=>$stm->{'error'}
                ];
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    public function eliminarNoticia($id){
        $cn=new Cn();
        $mysqli=$cn->conectar();
        $stm=$mysqli->prepare("call eliminarNoticia(?)");
        $stm->bind_param("i",$id);
        $stm->execute();
        if($stm->{'error'}==""){
            if($stm->{'field_count'}>0)
            {
                    $array["res"]="OK";
            }else{
                $array["error"]="No se pudo registrar";
            }
        }else{
            $array=[
                "error"=>$stm->{'error'}
                ];
        }
        $json=json_encode($array,JSON_FORCE_OBJECT);
        return $json;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Set the value of detalles
     *
     * @return  self
     */ 
    public function setDetalles($detalles)
    {
        $this->detalles = $detalles;

        return $this;
    }

    /**
     * Set the value of idImagen1
     *
     * @return  self
     */ 
    public function setIdImagen1($idImagen1)
    {
        $this->idImagen1 = $idImagen1;

        return $this;
    }

    /**
     * Set the value of idImagen2
     *
     * @return  self
     */ 
    public function setIdImagen2($idImagen2)
    {
        $this->idImagen2 = $idImagen2;

        return $this;
    }

    /**
     * Set the value of fecIngreso
     *
     * @return  self
     */ 
    public function setFecIngreso($fecIngreso)
    {
        $this->fecIngreso = $fecIngreso;

        return $this;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
   }
?>