<?php
   
   Class Iconicos{
        private $id;
        private $titulo;
        private $descripcion;
        private $idImgPlato;
        private $estado;

        public function readIconicos(){
            $cn=new Cn();
            $mysqli=$cn->conectar();
            $stm=$mysqli->prepare("call readIconicos()");
            // $stm->bind_param("i",$this->id);
            $stm->execute();
            // $array=[];
            if($stm->{'error'}==""){
                $rs=$stm->get_result();
                if(($rs->num_rows)>0){
                    while($row=$rs->fetch_assoc()){
                        $array[]=$row;
                    }
                }else{
                    $array=[
                        "error"=>"Sin Registros"
                    ];
                }
            }else{
                $array["error"]=$stm->{'error'};
            }
            $json=json_encode($array,JSON_FORCE_OBJECT);
            return $json;
        }
        public function readAdminNumberIconicos(){
            $cn=new Cn();
            $mysqli=$cn->conectar();
            $stm=$mysqli->prepare("SELECT count(*) as cantidad FROM iconicos");
            $stm->execute();
            $array=[];
            if($stm->{'error'}==""){
                $rs=$stm->get_result();
                if($rs->num_rows>0){
                    $array=$rs->fetch_assoc();
                }else{
                    $array["error"]="Sin registros";
                }
            }else{
                $array["error"]=$stm->{'error'};
            }
            $json=json_encode($array,JSON_FORCE_OBJECT);
            return $json;
        }
        public function readAdminIconicos($init,$cantidad){
            $cn=new Cn();
            $mysqli=$cn->conectar();
            $stm=$mysqli->prepare("call readIconicosAdmin(?,?)");
            $stm->bind_param("ii",$init,$cantidad);
            $stm->execute();
            $array=[];
            if($stm->{'error'}==""){
                $rs=$stm->get_result();
                if(($rs->num_rows)>0){
                    while($row=$rs->fetch_assoc()){
                        $array[]=$row;
                    }
                }else{
                    $array=[
                        "error"=>"Sin Registros"
                    ];
                }
            }else{
                $array["error"]=$stm->{'error'};
            }
            $json=json_encode($array,JSON_FORCE_OBJECT);
            return $json;
        }
        public function readAdminIconico(){
            $cn=new Cn();
            $mysqli=$cn->conectar();
            $stm=$mysqli->prepare("call readIconicoAdmin(?)");
            $stm->bind_param("i",$this->id);
            $stm->execute();
            $array=[];
            if($stm->{'error'}==""){
                $rs=$stm->get_result();
                if($rs->num_rows>0){
                    $array=$rs->fetch_assoc();
                }else{
                    $array["error"]="Sin registros";
                }
            }else{
                $array["error"]=$stm->{'error'};
            }
            $json=json_encode($array,JSON_FORCE_OBJECT);
            return $json;
        }

        public function nuevoIconico($txtTitulo,$txtDescripcion,$imgPlato,$imgPlatoReal){
            $cn=new Cn();
            $mysqli=$cn->conectar();
            $stm=$mysqli->prepare("call nuevoIconico(?,?,?,?)");
            $stm->bind_param("ssss",$txtTitulo,$txtDescripcion,$imgPlato,$imgPlatoReal);
            $stm->execute();
            if($stm->{'error'}==""){
                if($stm->{'field_count'}>0)
                {
                        $array["res"]="OK";
                }else{
                    $array["error"]="No se pudo registrar";
                }
            }else{
                $array=[
                    "error"=>$stm->{'error'}
                    ];
            }
            $json=json_encode($array,JSON_FORCE_OBJECT);
            return $json;
        }

        public function actualizarIconico($id,$txtTitulo,$txtDescripcion,$img1,$imgPlato,$imgPlatoReal){
                $cn=new Cn();
                $mysqli=$cn->conectar();
                $stm=$mysqli->prepare("call actualizarIconico(?,?,?,?,?,?)");
                $stm->bind_param("ississ",$id,$txtTitulo,$txtDescripcion,$img1,$imgPlato,$imgPlatoReal);
                $stm->execute();
                if($stm->{'error'}==""){
                    if($stm->{'field_count'}>0)
                    {
                            $array["res"]="OK";
                    }else{
                        $array["error"]="No se pudo registrar";
                    }
                }else{
                    $array=[
                        "error"=>$stm->{'error'}
                        ];
                }
                $json=json_encode($array,JSON_FORCE_OBJECT);
                return $json;
            }

        public function eliminarIconico($id){
            $cn=new Cn();
            $mysqli=$cn->conectar();
            $stm=$mysqli->prepare("call eliminarIconico(?)");
            $stm->bind_param("i",$id);
            $stm->execute();
            if($stm->{'error'}==""){
                if($stm->{'field_count'}>0)
                {
                        $array["res"]="OK";
                }else{
                    $array["error"]="No se pudo registrar";
                }
            }else{
                $array=[
                    "error"=>$stm->{'error'}
                    ];
            }
            $json=json_encode($array,JSON_FORCE_OBJECT);
            return $json;
        }
         /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
            $this->id = $id;

            return $this;
        }
   }

?>