<?php
    require './app/controller/2.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <title>Foodlandia - Qué es la Cocina Tailandesa</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
    ?>
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v5.9">
    <link rel="stylesheet" href="/public/assets/web/css/2.css?v6.3">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';

        ?>
        <section class='headline'>
            <div class='w-100 d-flex s1'>
                <div class='h-100 c-img-1'>
                    <img src="/public/img/web/2/img-1.jpg" class='w-100 h-100' style='opacity: 1; object-fit: cover;'>
                </div>
                <div class='cc-txt-1 d-flex align-items-center justify-content-end'>
                    <div class='c-txt-1 headline-bottom'>
                        <div class='txt-1'>
                            ¿Qué es la Cocina
                        </div>
                        <div class='txt-2'>
                            Tailandesa? 
                        </div>
                        <div class='txt-3'>
                            La reputación de la cocina tailandesa comienza a nivel comunitario como fuente de alimentos frescos, ingredientes, frutas, hierbas y especias de calidad, que refuerzan a Tailandia como la “Cocina del Mundo”, la cual se ha vuelto ampliamente reconocida por la comunidad internacional. El reconocimiento de la cocina tailandesa ha recibido varios galardones, como la "Comida más deliciosa del mundo" por CNNGO. Que siete platos tailandeses hayan sido votados en esta influyente lista muestra cómo la cocina tailandesa es una experiencia culinaria de clase mundial y Tailandia un gran destino para el turismo gastronómico.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class='w-100 s2'>
                <div>
                    <div class='cc-txt-2 d-flex align-items-center justify-content-start'>
                        <div class='c-txt-2 headline-bottom'>
                            <div class='noupper'>
                                "La comida" es una firma colorida de Tailandia.
                            </div>
                            <br>
                            <div class='txt-3'>
                            La comida no solo comunica la cultura nacional tailandesa que se ha acumulado durante un largo período de tiempo, sino que ha creado numerosas innovaciones en "sabores", "ingredientes", "creatividad de menú" y "cultura gastronómica".
                            </div>
                        </div>
                    </div>
                </div>
                <div class='h-100 c-img-2 headline'>
                    <img src="/public/img/web/2/img-2.jpg" class='w-100 h-100' style='display: block;width: 100vw; height: 100vh; object-fit: cover;'>
                </div>
            </div>
        </section>
        <section class=''>
                <div class='pt-1 pb-1 c-img-3 d-flex justify-content-center align-items-center'style='background-image:url(/public/img/web/2/img-3.png);'>
                    <div class='headline-bottom'>
                        <div class='noupper'>
                            Venir de viaje a Tailandia no estaría completo
                        </div>
                        <div class='txt-3'>
                            si no probaras los increíbles sabores de la cocina tailandesa,una nueva <br> experiencia al viajar por el país que nunca olvidarás.
                        </div>
                    </div>
                </div>
        </section>
        <section id='iconos_de_la_cocina'>
            <div class='w-100 p-5 text-center cc-iconos'>
                <div class='txt-1'>Íconos de la Cocina Tailandesa</div>
            </div>
            <div class='d-flex flex-wrap flex-md-nowrap justify-content-around justify-content-md-between pl-5 pr-5' style='overflow-y:hidden'>
                <!-- linea1 -->
                <div class="pt-2 pr-2 pb-1 text-center text-sm-left headline-bottom" style='max-width:300px'>
                    <div class='p-2 c-icon m-auto m-sm-0'>
                        <img src="/public/img/web/LogoReceta.png" width='100%' height='100%'>
                    </div>
                    <div class='titulo-tarjeta mt-3'>
                        3 Sabores 
                    </div>
                    <div class='txt-3 mt-3'>
                    Si pregunta sobre el auténtico sabor tailandés, estaremos de acuerdo en que lo más importante son los 3 sabores clave: ácido, dulce y picante.
                    </div>
                </div>
                <!-- linea2 -->
                <div class="pt-2 pr-2 pb-1 text-center text-sm-left headline-bottom" style='max-width:300px'>
                    <div class='p-2 c-icon m-auto m-sm-0'>
                        <img src="/public/img/web/LogoReceta.png" width='100%' height='100%'>
                    </div>
                    <div class='titulo-tarjeta mt-3'>
                        La Fama
                    </div>
                    <div class='txt-3 mt-3'>
                    La fama de la cocina tailandesa es mundialmente conocida. Aunque es posible que conozcas platos populares como el Pad Thai, Curry Verde y Curry de Massaman.
                    </div>
                </div>
                <!-- linea3 -->
                <div class="pt-2 pr-2 pb-1 text-center text-sm-left headline-bottom" style='max-width:300px'>
                    <div class='p-2 c-icon m-auto m-sm-0'>
                        <img src="/public/img/web/LogoReceta.png" width='100%' height='100%'>
                    </div>
                    <div class='titulo-tarjeta mt-3'>
                        Comida Callejera
                    </div>
                    <div class='txt-3 mt-3'>
                    Tailandia es famosa por su comida callejera, que es local, a la carta y fácil de encontrar y comer.
                    </div>
                </div>
            </div>
        </section>
        <section style='padding:2rem 0' class='seccion4'>
            <div class='d-flex justify-content-around justify-content-md-between flex-wrap flex-md-nowrap align-items-center'> 
                <div class='c-botones p-5 headline-bottom'>
                    Platos 
                    <br>
                        <h2 class='font-weight-bold'>tailandeses icónicos</h2>
                        <div class='txt-3'>Estos son los 6 platos más populares de todo el país, reconocidos mundialmente.</div>
                    <br>
                        <div class="m-auto cc-btn d-flex">
                            <div class='c-prev clickable noselect' id='s-btn-prev'>
                                <svg class="bi" width="20" height="20" fill="currentColor">
                                    <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-left"/>
                                </svg>
                            </div>
                            <div class='c-next clickable noselect' id='s-btn-next'>
                                <svg class="bi" width="20" height="20" fill="currentColor" >
                                    <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-right"/>
                                </svg>
                            </div>
                        </div>
                </div>
                <div class='c-slide-three' id='slide' style='overflow-y:hidden'>
                    <div class="swiper-container swiper3 headline-bottom">
                        <div class="swiper-wrapper ">
                            <?php
                               if(array_key_exists("error",$json)){
                            ?>

                            <?php
                               }else{
                                   foreach($json as $key=>$value){
                            ?>
                                <div class="swiper-slide <?php echo (count($json)==($key+1))?"":"" ?>">
                                    <!-- Slide 1 -->
                                    <div class='s-receta ml-4 noselect' style='z-index:<?php echo count($json)-intval($key);?>'>
                                        <div class='s-img-logo'>
                                            <img src="/public/img/web/LogoReceta.png">
                                        </div>
                                        <div class='s-img-receta'>
                                            <img src="/public/img/data/<?php echo $value["imgPlato"] ?>">
                                        </div>
                                        <!-- -------------------------------------- -->
                                        <div class='s-detalle'>
                                            <div class='w-100'></div>
                                            <div class='txt-detalle w-100'>
                                                <!-- <li>
                                                    <img src="/public/img/web/reloj.png">
                                                    <text>10 min</text>
                                                </li>
                                                <li>
                                                    <img src="/public/img/web/persona.png">
                                                    <text>2 personas</text>
                                                </li> -->
                                            </div>
                                            <div class='w-100'>
                                                <div class='mt-3 w-100 txt-nameReceta'>
                                                    <?php
                                                       echo $value["titulo"]
                                                    ?>
                                                </div>
                                                <div class='mb-4 w-100 txt-desReceta'>
                                                    <?php
                                                       echo $value["descripcion"]
                                                    ?>
                                                </div>
                                                <!-- <button class='btn btn-verRecta'>VER RECETA</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                   }
                               }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class='s4 d-flex justify-content-between' id='regional_tailandes'>
            <div style='max-width:400px;width:100%' class='pl-1 pr-3 cmd-1'>
                <div class='ccc1'>
                    <div>Cocina</div>
                    <div style='font-size:30px;line-height:35px;font-weight:500'>Regional <br>Tailandesa</div>
                    <div class='mt-2' style='font-size:14px;line-height:17px; padding-top: 40px;'>
                        Las especialidades regionales culinarias tailandesas son el verdadero encanto de la cocina tailandesa. Los ingredientes, las hierbas, las especias y las técnicas de preparación cuentan una historia que refleja la identidad, el modo de vida y el sustento de la gente de cada localidad.
                        <br><br>
                        Hay 5 regiones con su propia identidad culinaria: 
                        <br><br>
                    </div>
                </div>
                <div class='mt-2 mb-2 ccc2'>
                    <button data-num='1' class='btn-region btn btn-region-activate'>NORTE DE TAILANDIA</button>
                    <button data-num='2' class='btn-region btn'>NORESTE DE TAILANDIA</button>
                    <button data-num='3' class='btn-region btn'>REGIÓN CENTRAL</button>
                    <button data-num='4' class='btn-region btn'>ESTE DE TAILANDIA</button>
                    <button data-num='5' class='btn-region btn'>SUR DE TAILANDIA</button>
                </div>
            </div>
            <div class='d-flex flex-wrap justify-content-around ter-img-content'>
                <div style='height:100%;width:100%;margin:4px;' class='clickable img-region ter-img-1' id='img-1' >
                    <img src="/public/img/web/2/regiones/01_NORTE DE TAILANDIA/img-1.jpg" width='100%' height='100%'>
                </div>
                <div class='d-flex justify-content-between ter-img-content2'>
                    <div style='' class='clickable img-region img-event' id='img-2' data-actual='2'>
                        <img src="/public/img/web/2/regiones/01_NORTE DE TAILANDIA/img-2.jpg" width='100%' height='100%'>
                        <div class='img-sm'>+</div>
                    </div>
                    <div style='' class='clickable img-region img-event' id='img-3' data-actual='3'>
                        <img src="/public/img/web/2/regiones/01_NORTE DE TAILANDIA/img-3.jpg" width='100%' height='100%'>
                        <div class='img-sm'>+</div>
                    </div>
                    <div style='' class='clickable img-region img-event' id='img-4' data-actual='4'>
                        <img src="/public/img/web/2/regiones/01_NORTE DE TAILANDIA/img-4.jpg" width='100%' height='100%'>
                        <div class='img-sm'>+</div>
                    </div>
                </div>
            </div>
            <div style='max-width:400px;' class='pl-3 pr-1 clo'>
                <div style='font-size:14px;line-height:17px;font-weight:400' id='ter-1'>
                    EL TERRENO ES MONTAÑOSO CON UN CLIMA FRESCO Y DIVERSAS ESPECIES DE FLORES, HORTALIZAS Y HIERBAS NATURALES QUE SE PRESENTAN COMO ELEMENTOS CLAVE.
                </div>
                <br>
                <div style='font-size: 14px;line-height: 17px;' id='ter-2'>
                    La comida del norte tiene un sabor medio intenso y no agrega azúcar.
                    <br><br>
                    Una forma típica de comer del norte de Tailandia es "Khan Tok", donde te sientas en círculo alrededor de una bandeja de teca o una mesa de mimbre con patas altas y compartes platos locales con los demás.
                </div>
                <div class='mt-5'>
                    <div class='ter-titulo'></div>
                    <div class='ter-subtitulo'></div>
                    <div class='ter-texto mt-3'>
                    </div>
                </div>
            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>
    <script>
        let json=["01_NORTE DE TAILANDIA","02_NORESTE","03_CENTRAL","04_ESTE","05_SUR"]
        var s3 = new Swiper('.swiper3', {
            type: 'bullets',
            slidesPerView: 'auto',
            grabCursor: true,
            // loop: true,
        });
        let jtertitulo={
            0:{
                ter1:"EL TERRENO ES MONTAÑOSO CON UN CLIMA FRESCO Y DIVERSAS ESPECIES DE FLORES, HORTALIZAS Y HIERBAS NATURALES QUE SE PRESENTAN COMO ELEMENTOS CLAVE.",
                ter2:`
                    La comida del norte tiene un sabor medio intenso y no agrega azúcar.
                    <br><br>
                    Una forma típica de comer del norte de Tailandia es "Khan Tok", donde te sientas en círculo alrededor de una bandeja de teca o una mesa de mimbre con patas altas y compartes platos locales con los demás.
                `
            },
            1:{
                ter1:"LA COMIDA DE “ISAN” ES RECONOCIDA POR SUS SABORES FUERTES DE SAL, PICANTE Y AGRIO QUE ES MUY “SAP ILI” (MUY DELICIOSO).",
                ter2:`
                    Tres platos tailandeses de la Región Noreste que debes probar.
                `
            },
            2:{
                ter1:"ESTA REGIÓN FÉRTIL ES ABUNDANTE EN VERDURAS Y MARISCOS. POR LO TANTO, EXISTEN INNUMERABLES CLASES DE ALIMENTOS Y SABORES. GENERALMENTE SE UTILIZA LA LECHE DE COCO Y ESPECIAS COMO INGREDIENTES PRINCIPALES. ALGUNOS PLATOS HAN RECIBIDO INFLUENCIAS DE LA CORTE REAL DE TAILANDIA, ASÍ COMO DE LA CULTURA GASTRONÓMICA DE CHINA, LA INDIA Y EL OESTE.",
                ter2:`
                    Tres platos tailandeses de la Región Central que debes probar.
                `
            },
            3:{
                ter1:"LA MAYORÍA DE LOS PLATOS DEL ESTE NO PUEDEN ESCAPAR DE INCLUIR COMIDA MARINA PORQUE ESTA REGIÓN SE CONECTA CON EL MAR, TALES COMO CAMARONES FRESCOS, MARISCOS, CANGREJOS Y PESCADOS. LA ABUNDANCIA DE FRUTAS COMO EL DURIÁN, LA SALACCA, EL MANGOSTÁN Y EL RAMBUTAN SE UTILIZAN COMO INGREDIENTES PARA CREAR PLATOS LOCALES CON UN GUSTO EXTREMADAMENTE DELICIOSO.",
                ter2:`
                    Tres platos tailandeses de la Región Central que debes probar.
                `
            },
            4:{
                ter1:"LA COCINA DEL SUR ES BIEN CONOCIDA POR EL PICANTE DE LAS ESPECIAS AROMÁTICAS Y HIERBAS MEZCLADAS CON EL SALADO DE LA PASTA DE CAMARONES. SE HA DICHO QUE ESTA COMIDA ES MÁS PICANTE QUE OTRAS REGIONES. LA REGIÓN ES TAMBIÉN UNA FUENTE DE PLATOS DE MARISCOS FRESCOS. LA MAYORÍA DE LOS PLATOS TIENEN UN SABOR PICANTE, SALADO Y ÁCIDO, PERO NO DULCE.",
                ter2:`
                    Tres platos tailandeses del Sur que debes probar.
                `
            },
        }
        let jdata={
            0:{
                0:{
                    terTitulo:"",
                    terSubTitulo:``,
                    terTexto:``
                },
                1:{
                    terTitulo:"KHAO SOI",
                    terSubTitulo:`Tallarines al curry`,
                    terTexto:`
                        Los fideos de huevo en rodajas con curry aromático hechos de chile seco, pasta de camarones, ajo y comino, crean un hermoso color amarillo. La versión más popular es Khao Soi Kai (con pollo) que se come con col encurtida y chalotas en rodajas.
                        <br><br>
                        Sabor: Un poco picante, dulce por la leche de coco y ligeramente salado.
                    `
                },
                2:{
                    terTitulo:"KHANOM JEEN NAM NGIAO",
                    terSubTitulo:`Fideos tailandeses en salsa de curry`,
                    terTexto:`
                        Un platillo del Norte muy delicioso que debes probar con Khanon Jeen (fideos finos de arroz). El menú está cubierto con salsa de curry rojo o sopa que tiene un sabor fuerte aceitoso. Lo más destacado de este plato es el uso del polen seco del árbol del algodón como ingrediente que desprende un aroma fragante único. Este platillo se come con guarniciones de verduras como brotes de soja cruda, col encurtida, limón, chile seco frito y chicharrón de cerdo espolvoreado con ajo frito, cebolla y cilantro. 
                        <br><br>
                        Sabor: Salado, y se puede cocinar para tener otros sabores dependiendo del gusto individual.
                    `
                },
                3:{
                    terTitulo:"NAM PHRIK NUM-SAI UA",
                    terSubTitulo:`Pasta de chile verde - Salchicha del norte de Tailandia`,
                    terTexto:`
                        Sai Ua es una salchicha a la parrilla en la que el interior es carne de cerdo picada mezclada con hierbas que se comen con arroz glutinoso. Nam Phrik Num es una pasta de chile verde machacado con hierbas, cebolla roja, cilantro en rodajas, salsa de pescado y ajo. Se come con verduras frescas, chicharrones y arroz glutinoso.
                        <br><br>
                        Sabor: Salado y un poco picante. 
                    `
                },
            },
            1:{
                0:{
                    terTitulo:"",
                    terSubTitulo:``,
                    terTexto:``
                },
                1:{
                    terTitulo:"GAI YANG",
                    terSubTitulo:`Pollo a la parrilla`,
                    terTexto:`
                        Uno de los alimentos más populares y fáciles de comer. El pollo a la parrilla de cada local tiene una receta especial como resultado de la fermentación de las especias e ingredientes principales como sal, pimienta y ajo antes de insertar un pincho de madera y luego se asa a la parrilla hasta que tenga un color amarillo dorado con una fragancia aromática, interior suave y exterior crujiente. Hay varias salsas para seleccionar que son picantes y dulces.
                        <br><br>
                        Sabor: Salado y picante.
                    `
                },
                2:{
                    terTitulo:"GAENG OM",
                    terSubTitulo:`Curry al estilo Isan`,
                    terTexto:`
                        El curry Isan con verduras y carne es una sopa de un color blanco cremoso. El aroma proviene del eneldo condimentado con pescado fermentado, que es parecido al curry de Laos. Se come con arroz al vapor caliente o arroz glutinoso. 
                        <br><br>
                        Sabor: Salado, un poco picante.
                    `
                },
                3:{
                    terTitulo:"SAI KROK ISAN",
                    terSubTitulo:`Salchicha Isan`,
                    terTexto:`
                        Una salchicha de cerdo picada, grasa de cerdo y arroz cocido que se mezclan con hierbas como ajo, pimienta y cilantro que han sido fermentados y secados al sol para iniciar la acidez antes de asarlos. Se come con jengibre fresco, chile y ajo para reducir la grasa. 
                        <br><br>
                        Sabor: Agrio seguido de salado.
                    `
                },
            },
            2:{
                0:{
                    terTitulo:"",
                    terSubTitulo:``,
                    terTexto:``
                },
                1:{
                    terTitulo:"GAENG CHU CHI",
                    terSubTitulo:`Curry de coco`,
                    terTexto:`
                        Un curry de coco tailandés de la Región Central que utiliza carnes fáciles de cocinar; como la de pescado o langostinos. El atractivo proviene del aroma dulce del chile y la leche de coco sazonado con salsa de pescado y azúcar de palma espolvoreado con hojas de lima kaffir. 
                        <br><br>
                        Sabor: Salado e igualmente dulce con algo de picante del chile.
                    `
                },
                2:{
                    terTitulo:"PASTA DE CAMARONES",
                    terSubTitulo:``,
                    terTexto:`
                        Una comida tailandesa excepcional que es indispensable para la gente de la Región Central. Es una pasta de chile que utiliza camarones como ingrediente clave. La pasta de camarón se cocina a fuego abierto y sazonado con chalotas, chile y ajo antes de agregar los camarones secos y la salsa de pescado, azúcar y limón. Luego se adorna con bayas de pavo y chile y se puede comer con caballa frita y verduras hervidas.
                        <br><br>
                        Sabor: Picante suave, ácido, salado y ligeramente dulce.
                    `
                },
                3:{
                    terTitulo:"LON",
                    terSubTitulo:`Salsa de pasta de soja`,
                    terTexto:`
                        Una salsa antigua al estilo tailandés, que se cocina hirviendo leche de coco con elementos que se pueden guisar; como pasta de soja, cangrejo salado y langostinos. Tiene un aroma fragante y dulce por la leche de coco. El sabor salado se origina en el miso y el picante en el chile. Los acompañamientos vegetales incluyen frijoles, flor de plátano, berenjena, hierba sosa y pepino. 
                        <br><br>
                        Sabor: Dulce, aceitoso, salado con un suave sabor a la leche de coco.
                    `
                },
            },
            3:{
                0:{
                    terTitulo:"",
                    terSubTitulo:``,
                    terTexto:``
                },
                1:{
                    terTitulo:"GAENG MOO CHAMUANG",
                    terSubTitulo:`Curry de cerdo con Chamuang`,
                    terTexto:`
                        Plato autóctono de varias provincias de Este. El cerdo se saltea con curry, ajo, galangal, hierba luisa , chile seco, pasta de camarones y se sazona con salsa de pescado y azúcar. El ingrediente clave son las hojas de Chamuang (Garcinia cowa Roxb.), una planta local. Se debe dar tiempo para que la carne de cerdo se ablande. 
                        <br><br>
                        Sabor: Aceitoso seguido de ácido por las hojas de Chamuang, y salado.
                    `
                },
                2:{
                    terTitulo:"SEN CHAN PAD PU",
                    terSubTitulo:`Tallarines salteados con cangrejo`,
                    terTexto:`
                        Una antigua receta local de la provincia de Chanthaburi en la que lo más destacado son los fideos suaves y viscosos. Se parece al Pad Thai pero difiere en los ingredientes que son una mezcla de chile seco, ajo, pasta de camarones y cebolla morada. Se come junto con verduras locales, flor de plátano, brotes de soja y cebollines. 
                        <br><br>
                        Sabor: Fideos suaves y viscosos, sopa dulce seguida de sabores agrios y salados.
                    `
                },
                3:{
                    terTitulo:"PLA KRAPONG THOT NAM PLA",
                    terSubTitulo:`Tilapia frita en salsa de pescado`,
                    terTexto:`
                        La tilapia se fríe hasta que esté dorada y crujiente y se come con Yam Mamuang (ensalada de mango), que se compone de mango fresco cortado en rodajas finas. La salsa de la ensalada es una mezcla de salsa de pescado, azúcar, jugo de limón, cebolla morada en rodajas y chile para darle un sabor ácido, dulce, aceitoso y picante. 
                        <br><br>
                        Sabor: Salado, agrio y dulce.
                    `
                },
            },
            4:{
                0:{
                    terTitulo:"",
                    terSubTitulo:``,
                    terTexto:``
                },
                1:{
                    terTitulo:"GAENG SOM PAK TAI",
                    terSubTitulo:`Sopa picante agria del sur`,
                    terTexto:`
                        También conocido como Gaeng Lueang (sopa de curry amarillo) es diferente de otros platos del mismo nombre en otras regiones. Esto se debe a que usa chile agrio mezclado con comino para darle un color amarillo y un aroma suave. También constará de brotes de bambú, papaya y flor de loto, además puedes añadir langostinos o lubina según tu preferencia. 
                        <br><br>
                        Sabor: Agrio, salado y muy picante.
                    `
                },
                2:{
                    terTitulo:"KHUA KHLING",
                    terSubTitulo:`Curry de cerdo seco del sur`,
                    terTexto:`
                        Una comida sureña picante muy deliciosa que puede usar carne de res, pollo o cerdo. Las especias incluyen los sabores completos de curry, pimienta asada y hierbas, y lo que te hipnotizará es el aroma de la pasta de camarones que se fríe o se tuesta hasta que se seque. Se come con verduras frescas del Sur; tales como, hojas jóvenes de Man Pu (Glochidion wallichianum Muell.) y hojas jóvenes de mango Himaphan.
                        <br><br>
                        Sabor: Salado por el curry, aromático por el comino y los ingredientes. 
                    `
                },
                3:{
                    terTitulo:"NAM PHRIK GUNG SIAP",
                    terSubTitulo:`Pasta de camarones del sur`,
                    terTexto:`
                        También conocido por los tailandeses del sur como Nam Soup Gung Siap (sopa de pasta de camarones). Lo más destacado es el uso de langostinos en un pincho de madera y asados a la parrilla hasta que se sequen. El aroma perfumado proviene de la pasta de camarones y los langostinos. Otros ingredientes incluyen chile, pasta de camarones, sal, salsa de pescado y cebolla morada, y se puede comer con verduras frescas como el sato (Parkia speciose) y bambú hervido. 
                        <br><br>
                        Sabor: picante seguido de dulce.
                    `
                },
            },
            
        }
        let num=1;
        $(".btn-region").on("click",function(){
            num=$(this).data("num")
            $(".btn-region").removeClass("btn-region-activate")
            $(this).addClass("btn-region-activate")
            $("#ter-1").html(jtertitulo[num-1]["ter1"]);
            $("#ter-2").html(jtertitulo[num-1]["ter2"]);
            $(".ter-titulo").hide().html("").fadeIn(500)
            $(".ter-subtitulo").hide().html("").fadeIn(500)
            $(".ter-texto").hide().html("").fadeIn(500)
            $("#img-1").hide().html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-1.jpg" width='100%' height='100%'>`).fadeIn(500)
            $("#img-2").hide().html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-2.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`).fadeIn(500)
            $("#img-2").attr("data-actual","2")
            $("#img-3").hide().html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-3.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`).fadeIn(500)
            $("#img-3").attr("data-actual","3")
            $("#img-4").hide().html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-4.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`).fadeIn(500)
            $("#img-4").attr("data-actual","4")
        })
        let img=[0,1,2,3]
        // let contents=[0,1,2]
        $(".img-event").on("click",function(){
            let actual=$(this).attr("data-actual")
            $("#img-1").hide().html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-${actual}.jpg" width='100%' height='100%'>`).fadeIn(500)
            $(".ter-titulo").hide().html(jdata[num-1][actual-1]["terTitulo"]).fadeIn(500)
            $(".ter-subtitulo").hide().html(jdata[num-1][actual-1]["terSubTitulo"]).fadeIn(500)
            $(".ter-texto").hide().html(jdata[num-1][actual-1]["terTexto"]).fadeIn(500)
            if(actual==1){
                $("#img-2").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-2.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-2").attr("data-actual","2")
                $("#img-3").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-3.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-3").attr("data-actual","3")
                $("#img-4").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-4.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-4").attr("data-actual","4")
            }
            if(actual==2){
                $("#img-2").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-1.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-2").attr("data-actual","1")
                $("#img-3").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-3.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-3").attr("data-actual","3")
                $("#img-4").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-4.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-4").attr("data-actual","4")
            }
            if(actual==3){
                $("#img-2").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-1.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-2").attr("data-actual","1")
                $("#img-3").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-2.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-3").attr("data-actual","2")
                $("#img-4").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-4.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-4").attr("data-actual","4")
            }
            if(actual==4){
                $("#img-2").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-1.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-2").attr("data-actual","1")
                $("#img-3").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-2.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-3").attr("data-actual","2")
                $("#img-4").html(`<img src="/public/img/web/2/regiones/${json[num-1]}/img-3.jpg" width='100%' height='100%'><div class='img-sm'>+</div>`)
                $("#img-4").attr("data-actual","3")
            }
            // $.each(contents,function(key,value){
            //     let hechos=[]
            //     $.each(img,function(k,v){
            //         const found = hechos.find(element => element == );
            //         if(v!=(parseInt(actual)-1) && ){

            //         }
            //     })
            // })
        })

        $(".s-receta").on("mouseenter",function(){
            $(".c-slide-three .swiper-slide").removeClass("swiper-slide-active")
            $(this).parent().addClass("swiper-slide-active")
        })
        let posicion=0;
        <?php
            if(isset($posicion)){
                echo "posicion=".$posicion.";";
            }
        ?>
        // alert(posicion);
        $(document).ready(function(){
            s3.slideTo(posicion);
        })

        $(".c-prev").on("click",function(){
            // alert(1);
            s3.slidePrev()
        })
        $(".c-next").on("click",function(){
            s3.slideNext()
        })

        
  </script>
</body>
</html>