<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Contactanos</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
       echo ALERTIFYJS;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="./public/assets/web/css/general.css?v3.2">
    <link rel="stylesheet" href="./public/assets/web/css/11.css?v3.6">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <style>
        .ajs-header,.ajs-body *{
            color:#000 !important;
        }
        .capa-oscura{
            width:100vw;
            height:100vh;
            position:fixed;
            background-color: rgb(0,0,0,0.4);
            display:none;
            z-index: 2000 !important;
        }
    </style>
</head>
<body>
    <div class='capa-oscura'>
        <div class='d-flex justify-content-center align-items-center w-100 h-100'>
            <div class="spinner-grow m-1" style='' role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-grow m-1" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-grow m-1" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <section class='headline'>
            <div class='w-100 d-flex s1'>
                <div class='cc-all d-block'>
                    <div class='c-img-1' style='background-image:url(./public/img/web/11/img-1.jpg)'>
                        <!-- <img src="./public/img/web/11/img-1.png" class='w-100 h-100' style='opacity: 0.70;'> -->
                    </div>
                    <div class='c-form d-flex justify-content-center align-items-center pl-5 pr-5' style=''>
                        <div class='w-100 c-f'>
                            <div class='txt-1'>
                                    CONTÁCTANOS
                            </div>
                            <div>
                                <div class='pt-3 pb-2'>
                                    <input type="text" id='txtNombres' class='form-control' placeholder='Nombre(s)'>
                                </div>
                                <div class='pt-3 pb-2'>
                                    <input type="text" id='txtApellidos' class='form-control' placeholder='Apellidos'>
                                </div>
                                <div class='pt-3 pb-2'>
                                    <input type="email" id='txtCorreo' class='form-control' placeholder='Correo'>
                                </div>
                                 <div class='pt-3 pb-2'>
                                    <textarea type="text" id='txtDescripcion' class='form-control' placeholder='Mensaje' cols="30" rows="6"></textarea>
                                </div>
                                <div class='pt-2'>
                                    <button id='btnEnviar' class='btn btn-enviar pr-5 pl-5'>Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>
        <script>
            function showLoading(){
                $(".capa-oscura").fadeIn("fast")
            }

            function hideLoading(){
                $(".capa-oscura").fadeOut("fast")
            }
            $("#btnEnviar").on("click",function(){
                $("input,textarea").removeClass("is-invalid")
                let txtNombres=$("#txtNombres")
                let txtApellidos=$("#txtApellidos")
                let txtCorreo=$("#txtCorreo")
                let txtDescripcion=$("#txtDescripcion")
                switch(false){
                    case (txtNombres.val())?true:false:
                        txtNombres.addClass("is-invalid").focus()
                    break;
                    case (txtApellidos.val())?true:false:
                        txtApellidos.addClass("is-invalid").focus()
                    break;
                    case (txtCorreo.val())?true:false:
                        txtCorreo.addClass("is-invalid").focus()
                    break;
                    case (txtCorreo.val().indexOf("@")!=-1)?true:false:
                        txtCorreo.addClass("is-invalid").focus()
                        alertify.error("Debe ingresar un correo valido")
                    break;
                    case (txtDescripcion.val())?true:false:
                        txtDescripcion.addClass("is-invalid").focus()
                    break;
                    default:
                        alertify.confirm("Registrar","Se enviará la información ingresada<br> ¿Desea continuar?",
                        function(){
                            showLoading();
                            $.ajax({
                                url:"/app/ajax/registrarContactanos.php",
                                method:"POST",
                                dataType:"JSON",
                                data:{txtNombres:txtNombres.val(),txtApellidos:txtApellidos.val(),txtCorreo:txtCorreo.val(),txtDescripcion:txtDescripcion.val()}
                            }).done(function(res){
                                if(!res.error){
                                    alertify.success("Se envió correctamente")
                                    $("input,textarea").val("");
                                }else{
                                    alertify.error("Hubo un error al enviar")
                                }
                                hideLoading();
                            })
                        },
                        function(){

                        }).set('movable', false); 
                    break;
                }
            })
        </script>
    </main>
    <script>
  </script>
</body>
</html>