<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Comida callejera tailandesa</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v6.5">
    <link rel="stylesheet" href="/public/assets/web/css/5.css?v8.5">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <section class='' style=''>
            <div class='w-100 d-flex s1'>
                <div class='cc-all d-block'>
                    <div class='c-img-1' id='slide-1'>
                        <img src="/public/img/web/5/slide-1.jpg" style='display: block;width: 100vw;height: 100vh;object-fit: cover;' class='w-100 h-100'>
                    </div>
                    <div class='c-botones-auxiliar'>
                        <div class='d-flex'>
                            <button class='btn-prev'>
                                <svg class="bi" width="20" height="20" fill="currentColor">
                                    <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-left"/>
                                </svg>
                            </button>
                            <button class='btn-next'>
                                <svg class="bi" width="20" height="20" fill="currentColor">
                                    <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-right"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class='c-form d-flex justify-content-center align-content-between pl-5 pr-5 flex-wrap' style=''>
                        <div class='mb-2 w-100 text-right d-flex justify-content-center'>
                            <div class='c-f cct w-100' style='font-size:20px;line-height:22px;margin-top:50px'>
                                COMIDA CALLEJERA <br>TAILANDESA 
                            </div>
                        </div>
                        <div class='w-100 c-f text-right' style='margin-top:-230px'>
                            <div style='min-height:180px;padding-top:20px;padding-bottom:20px'>
                                <div class='c-f cct-movile w-100' style='font-size:20px;line-height:22px;margin-top:50px;margin-bottom:20px;display:none'>
                                    COMIDA CALLEJERA <br>TAILANDESA 
                                </div>
                                <div class='txt-2' id='t1' style='font-size:52px;font-weight: 700;line-height: 50px;'>
                                    KHAO PAD PU
                                </div>
                                <div class='txt-1' id='t2' style='font-size: 24px;font-weight: 200;'>
                                    ARROZ FRITO CON CANGREJO
                                </div>
                                <div class="txt-3" id='t3' style='margin-top:20px'>
                                Lo más destacado de este plato es la carne de cangrejo, que se saltea bien con arroz y huevo.
                                </div>
                            </div>
                        </div>
                        <div class='w-100'></div>
                        <div class='pt-3 d-block headline-bottom sc' style='float:right;position:absolute;bottom:0;margin-bottom:50px;z-index:-1'>
                            <!-- <div class='d-flex c-imgs'>
                                <div class='c-img-2' id='slide-2' style='background-image:url(./public/img/web/5/slide-2.jpg)'></div>
                                <div class='c-img-2' id='slide-3' style='background-image:url(./public/img/web/5/slide-3.jpg)'></div>
                                <div class='c-img-2' id='slide-4' style='background-image:url(./public/img/web/5/slide-4.jpg)'></div>
                            </div> -->
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-1.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-2.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-3.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-4.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-5.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-6.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-7.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-8.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-9.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-10.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-11.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-12.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-13.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-14.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>
                                    <div class="swiper-slide"><img src="/public/img/web/5/slide-15.jpg" style='display: block;object-fit: cover;'  width="100%" height="100%"></div>                             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='c-botones'>
                <div class='d-flex'>
                <button class='btn-prev'>
                    <svg class="bi" width="20" height="20" fill="currentColor">
                        <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-left"/>
                    </svg>
                </button>
                <button class='btn-next'>
                    <svg class="bi" width="20" height="20" fill="currentColor">
                        <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-right"/>
                    </svg>
                </button>
            </div>
        </section>
        <section class='s2 p-6' style='background-image:url(/public/img/web/5/img-1.jpg)' id='turismo'>
            <div class='txt-s2-titulo'>
                Turismo Gastronómico <br>en Tailandia 
            </div>
            <div class='position-relative mt-4 mb-2 d-flex justify-content-center flex-wrap'>
                <button data-ruta='1' class='btn btn-ruta btn-active'><div class='position-relative'>RUTA 1</div></button>
                <button data-ruta='2' class='btn btn-ruta'><div class='position-relative'>RUTA 2</div></button>
                <button data-ruta='3' class='btn btn-ruta'><div class='position-relative'>RUTA 3</div></button>
                <button data-ruta='4' class='btn btn-ruta'><div class='position-relative'>RUTA 4</div></button>
                <button data-ruta='5' class='btn btn-ruta'><div class='position-relative'>RUTA 5</div></button>
                <button data-ruta='6' class='btn btn-ruta'><div class='position-relative'>RUTA 6</div></button>
                <button data-ruta='7' class='btn btn-ruta'><div class='position-relative'>RUTA 7</div></button>
            </div>
            <div class='mt-5 d-flex justify-content-center' id='guia'>
                <button class='btn btn-descargar'><a style="text-decoration:none; color:var(--color1) !important" href="/public/guia_turistica/ThainessThroughThaiFood.pdf" download>DESCARGAR Thainess<br> through Thai Food</a></button>
                <button class='btn btn-descargar'><a style="text-decoration:none; color:var(--color1) !important" href="/public/guia_turistica/LonelyPlanetParaisosTai.pdf" download>DESCARGAR Lonely <br> Planet Paraisos Tai</a></button>
            </div>
        </section>
        <style>
            *{
                --column-standar:420px;
            }
            .container-white{
                background-color:white;
            }
            .container-white *{
                color:var(--color1) !important;
            }
            .column-triple{
                width:100%;
                /* max-width:var(--column-standar); */
                max-width:30%;
                margin-right:10px;
            }
            .column-doble{
                width:100%;
                /* max-width:var(--column-standar); */
                max-width:50%;
                margin-right:10px;
            }
            .container-standar{
                max-width:1400px;
                width:100%;
                padding:20px;
                /* background-color:red; */
                
            }
            .container-imagen-1{
                max-width:50%;
                min-width:50%;
                /* min-height:650px; */
                max-height:650px;
                overflow:hidden
            }
            .container-texto-1{
                width:100%;
                max-width:calc(50% - 20px);
                max-height:650px;
                margin-left:25px;
                padding:5px;
                background-color:white
            }
            .capa-1{
                display:flex;
            }
            .texto-ruta-cuerpo{
                font-size:14px;
                line-height:17px;
            }
            .ruta{
                font-size:18px;
            }
            .ttt-ruta-1{
                font-size:30px;
                line-height:30px;
                min-height:90px;
            }
            .container-texto-2{
                max-width:550px;
                /* margin-top:1rem; */
                min-height:190px;
            }
            .content-fotos-1{
                display:flex;
                /*position:absolute;*/
            }
            .capa-d-imagenes{
                min-height:270px;
            }
            .c-d-img{
                width: 235px;
                height: 270px;
                margin:8px;
                filter: drop-shadow(20px 20px 40px rgba(0, 0, 0, 0.25));
            }
            .ccn{
                padding-left:15px;
                padding-right:15px;
            }
            .ccn .texto-ruta-cuerpo{
                max-width:750px !important
            }
            /*---------------------- CAPA 2 ------------------------*/
            .capa-2{
                display:flex;
                margin-top:20px;
                justify-content:space-between;
            }
            .subtitulo-d{
                font-weight:600;
                font-size:17px;
                line-height:30px;
            }
            .texto-d{

            }

            /* ------------------------DD-2 CAPA - 3 ----------------- */
            .capa-3{
                display:flex;
                /* margin-top:20px; */
                justify-content:space-between;
            }
            .column-n-extra{
                width:100%;
                /* max-width: 880px; */
                max-width: 65.5%;
            }
            .container-n-imagen1{
                min-height:400px;
                max-height:400px;
                height:100%;
                width:100%;
                overflow:hidden;
                filter: drop-shadow(20px 20px 40px rgba(0, 0, 0, 0.25));
            }
            
            .c-txt-d-1{
               display:flex;
               align-items:flex-end;
            }
            .ttt-d-titulo{
                font-size:27px;
                line-height:29px;
            }
            /* --------------------------CAPA 4 ----------------------- */
            .capa-4{
                display:flex;
                margin-top:20px;
                margin-bottom:20px;
                justify-content:space-between;
            }
            /* --------------------------CAPA 5 ------------------------ */
            .capa-5{
                display:flex;
                /* margin-top:20px; */
                justify-content:space-between;
            }
            .capa-6{
                display:flex;
                /* margin-top:20px; */
                justify-content:space-between;
            }
            .capa-7{
                display:flex;
                /* margin-top:20px; */
                justify-content:space-between;
            }
            .container-n-imagen2{
                min-height:400px;
                max-height:1000px;
                height:520px;
                width:100%;
                overflow:hidden;
            }
            .container-n-imagen2 img{
                position:absolute;
                display: block;
                width: 75%;
                height:520px;
                object-fit: cover;
            }

            @media (max-width: 1100px){
                .capa-1{
                    flex-wrap:wrap;
                    justify-content:space-around;
                }
                .capa-2{
                    margin-top:0px;
                    flex-wrap:wrap;
                    justify-content:space-around;
                }
                .capa-3{
                    flex-wrap:wrap;
                }
                .capa-4{
                    flex-wrap:wrap;
                    justify-content:space-around;
                    margin-bottom:0px;
                }
                .capa-5{
                    flex-wrap:wrap;
                    justify-content:space-around;
                }
                .capa-6{
                    flex-wrap:wrap;
                    justify-content:space-around;
                }
                .capa-7{
                    flex-wrap:wrap;
                    justify-content:space-around;
                }
                .container-imagen-1{
                    /* width:100%; */
                    max-width:100%
                }
                .container-texto-1{
                    margin-top:1rem;
                    max-width:100%;
                    margin-left:0px;
                    max-height:initial;
                }
                .capa-d-imagenes{
                    min-height:initial;
                    margin-top:1rem;
                    margin-bottom:1rem;
                }
                .container-texto-2{
                    max-width:100%;
                    min-height:initial;
                }
                .content-fotos-1{
                    display:flex;
                    position:initial;
                    justify-content:space-around;
                }
                .c-d-img{
                    max-width: 260px;
                    width:100%;
                    height: 340px;
                }
                /* .c-d-img{
                    width: 180px;
                    height: 220px;
                } */
                /* ------- */
                .column-triple{
                    margin-right:0px;
                    max-width:100%;
                }
                /* -------- */
                .column-n-extra{
                    max-width:100%;
                }
                .container-n-imagen1{
                    margin-top:20px;
                    /* position:absolute; */
                }
                .container-n-imagen2{
                    padding-top:15px;
                    min-height:400px;
                    max-height:500px;
                    height:500px;
                    width:100%;
                    overflow:hidden;
                }
                .container-n-imagen2 img{
                    margin-top:0px !important;
                    position:initial;
                    width: 100%;
                    height:500px;
                    object-fit: cover;
                }
                .column-doble{
                    max-width:100%;
                    margin-left:0px !important; 
                }
                .ccn{
                    padding-left:0px;
                    padding-right:0px;
                }
                .subtitulo-d{
                    margin-top:1rem;
                }
            }
            @media (max-width: 550px){
                .container-texto-1{
                    max-height:1300px;
                }
                .content-fotos-1{
                    flex-wrap:wrap;
                }
                .c-d-img{
                    width: 140px;
                    height: 200px;
                }
            }
        </style>
        <section id='ruta'>
            <div class='container-white p-3' style='overflow-x:hidden'>
                <div class='container-standar m-auto'>
                    <div class='w-100 capa-1'>
                        <div class='container-imagen-1' id='mapa'>
                            <img src="/public/img/web/5/r1/mapa.jpg" style='object-fit: cover;' width="100%" height='100%'>
                        </div>
                        <div class='container-texto-1'>
                            <div class='ruta'>Ruta <text id='txtNumRuta'>1</text></div>
                            <div class='ttt-ruta-1' id='txt-titulo-1'>BANGKOK,<br> CASCO ANTIGUO CON ENCANTO</div>
                            <div class='container-texto-2 texto-ruta-cuerpo' id='txt-1'>
                                El casco antiguo de Bangkok alberga algunas de las mejores comidas de la capital, desde comida callejera hasta recetas reales. Se le conoce como el paraíso de los amantes de la comida por una razón. En esta área, podrá experimentar el auténtico estilo tailandés mientras está rodeado de edificios antiguos con una abundancia de sabrosa comida callejera. En la comida callejera de Bang Rak y Bang Lamphu se puede encontrar una mezcla de culturas culinarias china, musulmana y tailandesa.
                            </div>
                            <div class='capa-d-imagenes'>
                                <div class='content-fotos-1'>
                                    <div class='c-d-img' id='d-1'>
                                        <img src="/public/img/web/5/r1/d-img-1.jpg" width="100%" height='100%' style='object-fit: cover;'>
                                    </div>
                                    <div class='c-d-img' id='d-2'>
                                        <img src="/public/img/web/5/r1/d-img-2.jpg" width="100%" height='100%' style='object-fit: cover;'>
                                    </div>
                                    <div class='c-d-img' id='d-3' >
                                        <img src="/public/img/web/5/r1/d-img-3.jpg" width="100%" height='100%' style='object-fit: cover;'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='w-100 capa-2'>
                        <div class='column-triple'>
                            <div class='subtitulo-d'>Mañana</div>
                            <div class='texto-ruta-cuerpo '>
                            Experimente la vida por la mañana con locales de comida callejera de hace décadas en el área de Bang Rak. Desde la estación de BTS Saphan Taksin, camine por Charoen Krung Road y pruebe las delicias tailandesas-chinas; como congee (sopa de arroz), arroz con pato asado, fideos con bolas de pescado y auténtica comida tailandesa-musulmana.
                            </div>
                        </div>
                        <div class='column-triple'>
                            <div class='subtitulo-d'>Tarde</div>
                            <div class='texto-ruta-cuerpo '>
                            Tome el barco expreso Chao Phraya hasta el muelle Phra Athit en el casco antiguo. Allí, puede visitar y disfrutar delicias famosas como roti mataba (masa frita rellena de pollo), sopa de fideos con bolas de pescado (Yentafo), fideos de barco Sukhothai, crepes crujientes al estilo tailandés y varias bebidas que se sirven en cafés elegantes y retro. Puede tomar un bote y cruzar al muelle de Wang Lang, donde hay una amplia selección de comida callejera para disfrutar.
                            </div>

                        </div>
                        <div class='column-triple'>
                            <div class='subtitulo-d'>Noche</div>
                            <div class='texto-ruta-cuerpo '>
                            Deje que su aventura gastronómica comience en Yaowarat o China Town, uno de los mejores lugares del mundo para la comida callejera. Desde dulce hasta salado, puedes comer todo lo que quieras. No se pierda los mariscos recién asados a un muy buen precio.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='p-3' style='overflow:hidden'>
                <div class='container-standar m-auto'>
                    <div class='w-100 capa-3'>
                        <div class='column-triple c-txt-d-1'>
                            <!-- <div></div> -->
                            <div id='titulo2'>
                                <text class='ruta'>Día 2</text>
                                <br>
                                <text class='ttt-d-titulo'>
                                    CAMINE POR THA TIAN,<br> DONDE PODRÁ CAMINAR<br> POR LOS ANTIGUOS TEMPLOS, PALACIOS Y MUSEOS
                                </text>
                            </div>
                        </div>
                        <div class='column-n-extra'>
                            <div class='container-n-imagen1' id='n-img-1'>
                                <img src="/public/img/web/5/r1/n-img-1.jpg" width="100%" height='100%'>
                            </div>
                        </div>
                    </div>
                    <div class='w-100 capa-4'>
                        <div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Descanse en cualquiera de los elegantes cafés del vecindario. ¿Necesitas almorzar? Visite Err, un restaurante tailandés clásico que sirve comida tailandesa contemporánea con una presentación interesante y un auténtico sabor tailandés.
                                </div>
                        </div>
                        <div class='column-n-extra ccn'>
                            <div class='subtitulo-d'>Tarde</div>
                            <div class='texto-ruta-cuerpo'>
                                Para una experiencia cosmopolita, diríjase a Siam Square, el barrio más de moda de la ciudad. Siam Center es el hogar de Food Republic, un patio de comidas contemporáneo que ofrece una amplia selección de cocina. ¿Te apetece algo tailandés con un toque especial? Lo encontrará en restaurantes modernos y contemporáneos ubicados en Siam Paragon. Aquí, quedará impresionado por la presentación creativa y los deliciosos sabores.
                            </div>
                        </div>
                    </div>
                    <div class='w-100 capa-5'>
                        <div class='column-triple' id='s-capa-5'>
                            <!-- <div></div> -->
                            <!-- <div class=''> -->
                                <div class='subtitulo-d'>Noche</div>
                                <div class='texto-ruta-cuerpo '>
                                El crucero Supanniga es una nueva experiencia de cena, que le ofrece una vista majestuosa del río Chao Phraya y una deliciosa cocina tailandesa. El interior del crucero está lujosamente decorado para reflejar la cultura tailandesa.
                                <br><br>
                                <div class='cancel'>
                                    1. Sabrosos platos de comida callejera fáciles de comer, como fideos, junto a la carretera de Bang Rak.
                                    <br><br>
                                    2. Una mezcla de culturas culinarias chinas, musulmanas y tailandesas de la comida callejera de Bang Rak y Bang Lamphu.
                                    <br><br>
                                    3. Regrese al pasado en la atmósfera de Err en Tha Tian.
                                    <br><br>
                                    4. El patio de comidas en Siam Center que tiene una gran variedad de deliciosa comida callejera.
                                    <br><br>
                                    5. Delicias de Pratunam. No se pierda Khao Khao Man Kai en Kai Ton Pratunam en Phetchaburi Soi 30 con el ajetreo y el bullicio de las dos tiendas antes del mediodía.
                                    <br><br>
                                    6. Restaurantes modernos y contemporáneos de comida tailandesa.
                                    <br><br>
                                    7. Disfrute de un crucero con cena por el río Chao Phraya con el crucero Supanniga.
                                    </div>
                                </div>
                            <!-- </div> -->
                        </div>
                        <div class='column-n-extra'>
                            <div class='container-n-imagen2 mt-1' id='n-img-2'>
                                <img src="/public/img/web/5/r1/n-img-2.jpg" width="100%" height='100%'>
                            </div>
                            <div class='capa-6'>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>
    <script>
        <?php
            if($idRuta){
                echo "const idRuta=$idRuta;";
            }
        ?>
       var swiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            // spaceBetween: 25,
            // slidesPerGroup: 3,
            loop: true,
            loopFillGroupWithBlank: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
        let json={
            0:{
                "titulo1":`BANGKOK,<br> CASCO ANTIGUO CON ENCANTO`,
                "txt1":`El casco antiguo de Bangkok alberga algunas de las mejores comidas de la capital, desde comida callejera hasta recetas reales. Se le conoce como el paraíso de los amantes de la comida por una razón. En esta área, podrá experimentar el auténtico estilo tailandés mientras está rodeado de edificios antiguos con una abundancia de sabrosa comida callejera. En la comida callejera de Bang Rak y Bang Lamphu se puede encontrar una mezcla de culturas culinarias china, musulmana y tailandesa.`,
                "capa2":` <div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                Experimente la vida por la mañana con locales de comida callejera de hace décadas en el área de Bang Rak. Desde la estación de BTS Saphan Taksin, camine por Charoen Krung Road y pruebe las delicias tailandesas-chinas; como congee (sopa de arroz), arroz con pato asado, fideos con bolas de pescado y auténtica comida tailandesa-musulmana.
                                </div>
                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'>Tarde</div>
                                <div class='texto-ruta-cuerpo '>
                                Tome el barco expreso Chao Phraya hasta el muelle Phra Athit en el casco antiguo. Allí, puede visitar y disfrutar delicias famosas como roti mataba (masa frita rellena de pollo), sopa de fideos con bolas de pescado (Yentafo), fideos de barco Sukhothai, crepes crujientes al estilo tailandés y varias bebidas que se sirven en cafés elegantes y retro. Puede tomar un bote y cruzar al muelle de Wang Lang, donde hay una amplia selección de comida callejera para disfrutar.
                                </div>

                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'>Noche</div>
                                <div class='texto-ruta-cuerpo '>
                                Deje que su aventura gastronómica comience en Yaowarat o China Town, uno de los mejores lugares del mundo para la comida callejera. Desde dulce hasta salado, puedes comer todo lo que quieras. No se pierda los mariscos recién asados a un muy buen precio.
                                </div>
                            </div>`,
                "titulo2":`<text class='ruta'>Día 2</text>
                            <br>
                            <text class='ttt-d-titulo'>
                                CAMINE POR THA TIAN,<br> DONDE PODRÁ CAMINAR<br> POR LOS ANTIGUOS TEMPLOS, PALACIOS Y MUSEOS
                            </text>`,
                "capa4":`<div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Descanse en cualquiera de los elegantes cafés del vecindario. ¿Necesitas almorzar? Visite Err, un restaurante tailandés clásico que sirve comida tailandesa contemporánea con una presentación interesante y un auténtico sabor tailandés.
                                </div>
                        </div>
                        <div class='column-n-extra ccn'>
                            <div class='subtitulo-d'>Tarde</div>
                            <div class='texto-ruta-cuerpo'>
                                Para una experiencia cosmopolita, diríjase a Siam Square, el barrio más de moda de la ciudad. Siam Center es el hogar de Food Republic, un patio de comidas contemporáneo que ofrece una amplia selección de cocina. ¿Te apetece algo tailandés con un toque especial? Lo encontrará en restaurantes modernos y contemporáneos ubicados en Siam Paragon. Aquí, quedará impresionado por la presentación creativa y los deliciosos sabores.
                            </div>
                        </div>`,
                "capa5":`<div class='subtitulo-d'>Noche</div>
                                <div class='texto-ruta-cuerpo '>
                                El crucero Supanniga es una nueva experiencia de cena, que le ofrece una vista majestuosa del río Chao Phraya y una deliciosa cocina tailandesa. El interior del crucero está lujosamente decorado para reflejar la cultura tailandesa.
                                <br><br>
                                <div class='cancel'>
                                1. Sabrosos platos de comida callejera fáciles de comer, como fideos, junto a la carretera de Bang Rak.
                                <br><br>
                                2. Una mezcla de culturas culinarias chinas, musulmanas y tailandesas de la comida callejera de Bang Rak y Bang Lamphu.
                                <br><br>
                                3. Regrese al pasado en la atmósfera de Err en Tha Tian.
                                <br><br>
                                4. El patio de comidas en Siam Center que tiene una gran variedad de deliciosa comida callejera.
                                <br><br>
                                5. Delicias de Pratunam. No se pierda Khao Khao Man Kai en Kai Ton Pratunam en Phetchaburi Soi 30 con el ajetreo y el bullicio de las dos tiendas antes del mediodía.
                                <br><br>
                                6. Restaurantes modernos y contemporáneos de comida tailandesa.
                                <br><br>
                                7. Disfrute de un crucero con cena por el río Chao Phraya con el crucero Supanniga.
                                </div>
                            </div>`,
                "content4":`<div> 
                            </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
            1:{
                "titulo1":`MERCADO FLOTANTE DE FIN DE SEMANA EN SAMUT SONGKRAM, CAMINO DEL PASADO, UN ESTILO DE VIDA AL RÍO, DE AMPHAWA A MAE KLONG<br><br>`,
                "txt1":`En algunas partes del país, la gente aún conserva su estilo de vida tradicional junto al río. Puede encontrar una forma de vida tan idílica alrededor del río Mae Klong, la "Venecia del Este", en Samut Songkhram. Los mercados flotantes lo llevarán atrás en el tiempo a su glorioso pasado, mientras que el estilo de vida de la gente local del mercado flotante de Tha Kha gira en torno a la comida de los barcos, donde los vendedores en barcos venden curiosidades culinarias locales, verduras y frutas, a menudo caseras`,
                "capa2":` <div class='column-triple'>
                                <div class='subtitulo-d' nowrap>Mañana - 3 MERCADOS FLOTANTES</div>
                                <div class='texto-ruta-cuerpo '>
                                    El mercado de Tha Kha es un verdadero mercado de agricultores en su forma más pura. Ubicados entre cocoteros y huertos de frutas, los agricultores llevan sus productos en un bote para venderlos a bajo precio en un ambiente de ritmo lento.
                                </div>
                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'> </div>
                                <div class='texto-ruta-cuerpo '>
                                    Al final de la mañana: el mercado Bang Noi Floating es un intento de devolver la vida a la comunidad junto al canal. Las delicias locales incluyen el mini roti, el café antiguo y los fideos de hierbas. Las tiendas tienen un toque retro y hay servicios de alojamiento en casas de familia reconfortantes para aquellos que buscan una escapada tranquila
                                </div>
                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'>Noche</div>
                                <div class='texto-ruta-cuerpo '>
                                    El mercado de Amphawa es donde literalmente puedes comer hasta que te canses. Los mariscos, la comida local y los postres tailandeses se venden en barcos y en tierra, y puede disfrutarlos sentado junto al canal. No se pierda el Proyecto Amphawa Chaipattananurak y el Museo de Postres Tailandeses. También puede optar por un crucero por el río y entrar en el encantador mundo de las luciérnagas pululando en los árboles a lo largo de la orilla del río.
                                </div>
                            </div>`,
                "titulo2":`<text class='ruta'>Día 2</text>`,
                "capa4":`<div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Visite Tao Tan, donde podrá ver cómo los lugareños hacen azúcar de coco utilizando estufas tradicionales como en el pasado.
                                    <br><br>
                                    Al final de la mañana: explore la comunidad de Bang Phlap, una comunidad verde con exuberantes jardines verdes y una vida tranquila sin pretensiones. Puede disfrutar comiendo frutas frescas y procesadas durante su visita.
                                </div>
                        </div>
                        <div class='column-n-extra ccn'>
                            <div class='subtitulo-d'>Tarde</div>
                            <div class='texto-ruta-cuerpo'>
                                Pase por la comunidad de Khlong Khon para aprender sobre la forma de vida de los pescadores. Visite las cabañas de la granja de conchas en el mar utilizando los servicios de botes locales, o pruebe el esquí en barro. Estas actividades seguramente abrirán su apetito, haciendo que los mariscos frescos sean aún más deliciosos después.
                            </div>
                        </div>`,
                "capa5":`<style>.container-n-imagen2{
                            height:550px;
                        }
                        .container-n-imagen2 img{
                            height:550px;
                        }
                        </style>
                            
                            <div class='subtitulo-d'>Noche</div>
                                <div class='texto-ruta-cuerpo '>
                                Vaya a Don Hoi Lot en Samut Songkhram, un destino gastronómico para los amantes de los mariscos. Aquí, puede encontrar mariscos frescos y secos y restaurantes donde puede disfrutar de mariscos recién capturados y divinamente cocinados.
                                <br><br>
                                <div class='cancel'>
                                    1. El estilo de vida de la población local del mercado flotante de Tha Kha.
                                    <br><br>
                                    2. Mercado Flotante de Bang Noi, una pequeña comunidad llena de encanto con tiendas que contienen recuerdos de calidad y comida deliciosa.
                                    <br><br>
                                    3. Khao Nam Phrik Pla Thu, otro delicioso menú.
                                    <br><br>
                                    4. Cocina de renombre de Samut Songkhram que incluye mariscos y leche de coco.
                                    <br><br>
                                    5. Delicias al final de la noche en el mercado flotante Amphawa.
                                    <br><br>
                                    6. Cada paso para hacer azúcar de coco se puede ver en Amphawa Chaipattananurak (Granja de demostración agrícola)
                                    <br><br>
                                    7. Comunidad Bang Phlap, una forma de vida que se puede experimentar fácilmente.
                                    <br><br>
                                    8. Don Hoi Lot, en la desembocadura del estuario de Mae Khlong. Este es probablemente el lugar más famoso de la provincia para comer mariscos, y una docena de restaurantes ocupan el área alrededor del muelle cercano, muchos de los cuales ofrecen vistas al Golfo.
                                </div>
                            </div>`,
                "content4":`<div> 
                            </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
            2:{
                "titulo1":`LANNA INCREÍBLE`,
                "txt1":`El reino montañoso de Lanna es una tierra de té, café y los Proyectos Reales del Rey Rama IX.
                        <br><br>
                        Rastree las rutas del té y el café, esta es una de las mejores fuentes del mundo, y explore la forma de vida local, coma frutas frescas en los huertos. Disfrute de la cocina del norte de Tailandia y la influencia china de Yunnan.`,
                "capa2":` <div class='container-imagen-1'>
                                <div class='subtitulo-d'>Día 1</div>
                                <div class='texto-ruta-cuerpo '>
                                    Viaje a Doi Chang y Doi Wawi, una de las fuentes de café más famosas del mundo. Déjese cautivar por la vista de las plantaciones de café y vea cómo se hace el café, desde el suelo hasta la taza. Tome un café recién hecho mientras aprecia la vista panorámica desde la cima de la montaña. No se pierda la cocina china de Yunnan; como el estofado de pierna de cerdo al estilo de Yunnan con pan al vapor que se sirve en un restaurante ubicado en una plantación de té.
                                </div>
                            </div>
                            <div class='container-texto-1'>
                                <div class='subtitulo-d'>Día 2</div>
                                <div class='texto-ruta-cuerpo '>
                                    Disfrute de un té de primera calidad en la plantación de té Choui Fong, un nuevo hito en Mae Chan, Chiang Rai. En la plantación de té en terrazas, puede recoger hojas de té y degustar varios tipos. Después de eso, visite el Proyecto de Variedades Vegetales Príncipe Chakraband Pensiri y el Restaurante Chan Ka Phak en Mae Sai, que se encuentra entre los Proyectos Reales del Rey Rama IX. Experimente las maravillosas propiedades del aceite de semilla de té y pruebe las verduras frescas en su origen. En la temporada de fresas, pruebe la ensalada de aceite de semillas de fresa y té, que sin duda le levantará el ánimo.
                                </div>
                            </div>`,
                "titulo2":`<text class='subtitulo-d'>Día 3</text>`,
                "capa4":`
                        <div class='column-doble'>
                            <div class='subtitulo-d'></div>
                            <div class='texto-ruta-cuerpo'>
                                Viva como un local en Ban Saeo en Chiang Saen. Siéntese en un vehículo i-tak y visite huertos frutales y servicios de alojamiento en familia junto al río. Pruebe las delicias del noreste o de Isan preparadas por los descendientes de Isan que emigraron aquí. Antes de partir, no olvide visitar Ma Long Der en Khua Silapa, donde podrá probar la auténtica cocina del norte de Tailandia bellamente presentada mientras disfruta del arte.
                            </div>
                        </div>`,
                "capa5":`<style>.container-n-imagen2{
                            height:450px;
                        }
                        .container-n-imagen2 img{
                            height:450px;
                        }
                        </style>
                        
                        <div class='subtitulo-d'></div>
                                <div class='texto-ruta-cuerpo '>
                                    <div class='cancel'>
                                    1. Café Doi Chang, uno de los cafés de mejor calidad del mundo. La cafetería insignia es un lugar cómodo para beber y ofrece un menú breve de refrigerios y comidas ligeras.
                                        <br><br>
                                    2. Productos de la naturaleza: en Kat Doi Tung se pueden comprar taro y nuez del Inca.
                                        <br><br>
                                    3. El sabor de la cocina china de Yunnan se puede pedir en Ying Pong Yunnan
                                        <br><br>
                                    4. Para disfrutar de deliciosos platos de té verde y té de calidad para preparar, vaya a la plantación de té Choui Fong, un punto de referencia turístico local enormemente popular. Además del hermoso paisaje del campo de té que se extiende hasta donde alcanza la vista.
                                        <br><br>
                                    5. La delicia de los menús llenos de fruta fresca en Chan Ka Phak, donde puede detenerse para disfrutar de una ensalada de papaya verde fresca, som tam, pollo a la parrilla, kai yang, arroz glutinoso y muchas verduras crujientes recogidas del jardín adyacente.
                                        <br><br>
                                    6. Vea el estilo de vida de la población local en la comunidad de Ban Saeo, Amphoe Chiang Saen y disfrute de la degustación de algunos platos locales.
                                    </div>
                                </div>`,
                "content4":`<div> 
                            </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
            3:{
                "titulo1":`CHANTHABURI, MÁS ALLÁ DE FRUTAS FRESCAS Y MARISCOS`,
                "txt1":`¿Qué mejor lugar para disfrutar de frutas frescas que esta capital de frutas? Chanthaburi es el hogar de cientos de huertos frutales, así como mariscos frescos. Su cultura bien conservada y la amabilidad de su gente, así como su increíble comida, harán que te enamores de esta serena ciudad.`,
                "capa2":`   <div class='container-imagen-1'>
                                <div class='subtitulo-d'>Día 1 - Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Haz un viaje de 1 día a los huertos de frutas y no pierdas la oportunidad de probar el Rey de las Frutas: el durián. Puede optar por un tour de buffet de frutas (desde finales de abril hasta finales de junio, que es la mejor temporada para el durián, el mangostán y el rambután).
                                </div>
                            </div>
                            <div class='container-texto-1'>
                                <div class='subtitulo-d'>Noche</div>
                                <div class='texto-ruta-cuerpo '>
                                    Saboree la comida callejera en el mercado de Namphu cerca de la Torre del Reloj, donde podrá disfrutar de una deslumbrante variedad de comidas callejeras. Una de las delicias que debe probar es el arroz con sal y chile, que sabe a salsa de mariscos, servido con camarones, calamares, cerdo y huevo de pato hervido. ¿Alguien pidió una segunda ración?
                                </div>
                            </div>`,
                "titulo2":`<text class='ruta'>Día 2</text>`,
                "capa4":`<div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Viaje al pasado en la comunidad de Chanthabun, que se remonta a más de 3 siglos. Ubicada en el río Chanthabun, la comunidad alberga antiguas casas de estilo tailandés, edificios euro-chinos y una iglesia de 200 años de antigüedad. Los lugareños son de ascendencia tailandesa, vietnamita y china. Come como un local en casas de madera junto al río.
                                </div>
                        </div>
                        <div class='column-n-extra ccn'>
                            <div class='subtitulo-d'>Tarde</div>
                            <div class='texto-ruta-cuerpo'>
                                Reabastezca su estómago en los restaurantes antiguos y disfrute de platos locales como el curry de cerdo con hojas de Chamuang, fideos salteados con cangrejo y frutas procesadas como snacks de durián, pasta de mango y bebidas a base de hierbas.
                            </div>
                        </div>`,
                "capa5":`<style>.container-n-imagen2{
                            height:425px;
                        }
                        .container-n-imagen2 img{
                            height:425px;
                        }
                        </style>
                
                        <div class='subtitulo-d'>Día 3 - Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    visite el mercado de postres raros, un pequeño mercado en la aldea de Nong Bua, Amphoe Mueang. Aquí puede encontrar postres tailandeses raros y platos antiguos en una comunidad que ha conservado su identidad durante más de un siglo.
                                        <br><br>
                                    Al mediodía: pruebe recetas reales y pastas de chile antiguas en el restaurante Ban Nam Phrik Khao Suai, que vende los favoritos locales de Chanthaburi; como arroz mezclado con chile y sal con camarones mantis y cangrejo, Pad Thai de camarones mantis, arroz y caballa con pasta de chile, y más. Los platos se presentan espléndidamente, utilizando utensilios locales del pasado. Los servidores también están vestidos con trajes tradicionales tailandeses.
                                        <br><br>
                                    En la tarde: disfrute de mariscos frescos en los restaurantes junto al mar y en casas tranquilas donde podrá relajarse y disfrutar de un buffet all you can eat de cangrejo. Thong Natee Seafood, justo al lado de Oasis World en Laem Sing, es donde puede disfrutar de mariscos frescos y delicias locales de Chanthaburi. Si desea acercarse a la naturaleza o es un gran fanático del cangrejo de caparazón blando, visite la granja de cangrejos de caparazón blando en Khlung. Al llegar allí, pasará por un bosque de manglares, donde podrá presenciar el auténtico estilo de vida de los pescadores.   
                                    <br><br>
                                    <div class='cancel'>
                                        1. Disfrute del agroturismo visitando huertos frutales y degustando frutas frescas del árbol como durián, rambután y long kong.
                                        <br><br>
                                        2. Para una comida fácil por la mañana, hay una selección que incluye Khao Kaeng (arroz y curry), Mu Ping (brochetas de cerdo a la parrilla) y Pad Thai (fideos fritos tailandeses).
                                        <br><br>
                                        3. Dos menús que no debes perderte son Khao Phrik con sal (arroz con chile con sal) y Kuai Tiao Mu Liang (fideos con cerdo).
                                    </div>
                                </div>
                            </div>`,
                "capa6":`
                <div class='column-doble'>
                                    <div class='subtitulo-d'></div>
                                    <div class='texto-ruta-cuerpo '>
                                        <br>
                                        <div class='cancel'>
                                            4. El camino fluvial de la comunidad de Chanthabun con su ambiente bullicioso lleno de gente y tiendas.
                                            <br><br>
                                            5. Se pueden comprar recuerdos en Chanthorn Pochana. Los platos sobresalientes que debe probar incluyen Kaeng Mu Chamuang (curry de cerdo con Chamuang) y Lon Pu Khai (estofado de huevos de cangrejo).
                                            <br><br>
                                            6. Si desea comer la comida local de Chanthaburi, pase por Ban Nam Phrik Khao Suai.
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class='column-doble' style='margin-left:7%'>
                                    <div class='subtitulo-d'></div>
                                    <div class='texto-ruta-cuerpo'>
                                        <div class='cancel'>
                                            <br>
                                            7. Mariscos frescos en Thong Natee Seafood. Si vienes durante la temporada de frutas, prueba algunos platos elaborados con frutas.
                                            <br><br>
                                            8. Nong Bua Village, el raro mercado de postres.
                                        </div>
                                    </div>
                                </div>
                `,
                "content4":`<div> 
                            </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
            4:{
                "titulo1":`UDON THANI - NONG KHAI NAKHON PHANOM, UN SABOR DE ISAN`,
                "txt1":`UDON THANI: Empiece la mañana con comida tailandesa-vietnamita: Khai Katha (huevos en una sartén), Pho y pan vietnamita. Udon Thani es conocida como la ciudad de los fideos porque hay muchos platos de fideos: fideos planos, enrollados, de arroz y vietnamitas. Para el almuerzo, no te decepcionará el Je Kai Som Tam, un famoso restaurante local en Udon Thani conocido por sus ardientes platos de Isan. Es particularmente famoso por su pla ra (pasta de pescado fermentada) hecha con pescado capturado en los ríos Chi, Mun y Khong. Para la cena, disfrute de una comida saludable a base de vegetales en VT Naem Nueang, uno de los mejores lugares para el naem nueang en la región.`,
                "capa2":`   <div class='container-imagen-1'>
                                <div class='subtitulo-d'>NONG KHAI</div>
                                <div class='texto-ruta-cuerpo '>
                                    La vida a lo largo del río Khong, la carretera a lo largo del río Khong, desde el primer puente de la amistad entre Tailandia y Laos hasta Phra That Klang Nam, cerca de la playa de Chommani, está llena de muchos restaurantes de Isan para explorar. El restaurante Som Tam Yai See (cerca del Puente de la Amistad) es tan famoso que tendrás que hacer una reserva antes de tu visita. Prueba el marisco Tam Thai allí y te olvidarás de todo lo demás.Som Tam Tim Ja es otro gran lugar para som tam, y si tienes la oportunidad de visitarlo, no olvides probar Tam Sua.
                                </div>
                            </div>
                            <div class='container-texto-1'>
                                <div class='subtitulo-d'> </div>
                                <div class='texto-ruta-cuerpo' style='padding-top: 24px;'>¿Te apetece la comida vietnamita? Daeng Naem Nueang es tu mejor opción. Cuando el sol se aleje del horizonte, dé un paseo por la calle peatonal del mercado Tha Sadet, donde podrá ver casas antiguas a lo largo del río Khong, comprar artículos OTOP y probar la comida local. Para la cena, visite el restaurante Chai Khong para disfrutar de pescado fresco y una hermosa vista del río Khong. Es una casa de madera en la orilla del río. La mayoría de los ingredientes son de origen local y el pescado se captura en el río.
                                </div>
                            </div>`,
                "titulo2":`<div class='subtitulo-d'>NAKHON PHANOM</div>
                            <div class='texto-ruta-cuerpo '>
                                Tome un desayuno vietnamita en Pornthep, donde podrá probar Khai Katha, fideos vietnamitas, bollos rellenos, sopa de arroz, gachas de arroz, mu yo frito (salchicha de cerdo) y sopa de sangre de cerdo. No se pierda las albóndigas de arroz al vapor en Sri Thep, u obtenga una solución rápida en la tienda de fideos Rim Khong, que es muy recomendada por los lugareños, famosa por sus fideos de cerdo y ternera. Al final de la tarde, relájese en el restaurante junto al río Ruen Rim Nam, donde podrá echar un vistazo a lo impresionante que puede ser el río Khong. Disfrute de sabrosos platos elaborados con pescado recién capturado del río; como el bagre gigante del Mekong en Tom Yam, el bagre de cola roja picante salteado y el pescado picante con cabeza de serpiente con hierbas. Por la noche, dé un paseo por la calle peatonal Nakhon Phanom, donde puede comprar recuerdos y disfrutar de bocadillos rápidos en el camino.
                            </div>`,
                "capa4":`<div class='column-triple'>
                                <div class='subtitulo-d'></div>
                                <div class='texto-ruta-cuerpo '>
                                    <div class='cancel'>
                                        1. VT Naem Nueang, la provincia de Udon Thani tiene todos los platos que muestran la cultura vietnamita.
                                        <br><br>                           
                                        2. El desayuno debe tener fideos, Khai Katha (huevo en una sartén) y baguettes.
                                    </div>
                                </div>
                        </div>`,
                "capa5":`<style>.container-n-imagen2{
                            height:425px;
                        }
                        .container-n-imagen2 img{
                            margin-top:-50px;
                            height:425px;
                        }
                        </style>
                        <div class='subtitulo-d'></div>
                                <div class='texto-ruta-cuerpo '>
                                    <div class='cancel'>
                                        3. Som Tam (ensalada de papaya verde), una comida que se puede comer todo el día y que es un plato estrella de Isan.
                                        <br><br>                           
                                        4. La mayoría de los platos de Isan son fáciles de comer; por ejemplo, Kai Yang (pollo frito), Som Tam que incluye Som Tam Thai (ensalada de papaya verde al estilo tailandés), Som Tam Pu (ensalada de papaya verde con cangrejo) y Som Tam Pla Ra (ensalada de papaya verde con pescado fermentado).
                                        <br><br>
                                        5. Nong Khai y el ambiente junto al río Khong con numerosos restaurantes y cocina isan-vietnamita que no debe perderse.
                                        <br><br>
                                        6. Khai Katha (huevo en una sartén), el desayuno de Nakhon Phanom que es fácil de comer a un precio razonable.
                                        <br><br>
                                        7. Chai Khong, un restaurante de la ribera de Khong con menús excepcionales; por ejemplo, Tom Yam Pla Nam Khong (sopa de pescado picante del río Khong) y Pla Thot (pescado frito).
                                    </div>
                                </div>
                            </div>`,
                "content4":`<div> 
                            </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
            5:{
                "titulo1":`HAT YAI- SONGKHLA`,
                "txt1":`En el centro económico de Songkhla, Hat Yai, se puede encontrar una hermosa combinación de diversas cocinas. Las influencias tailandesas, malayas y chinas se unen en este casco antiguo. Explore la comida callejera en establecimientos antiguos y observe el vanguardista arte callejero antes de probar mariscos frescos con vista al lago Songkhla.`,
                "capa2":` <div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    La belleza de pasar tiempo en Hat Yai es disfrutar de un desayuno estilo casa de té, dim sum y Bak Kut Teh (caldo de costilla de cerdo). Hay decenas de restaurantes establecidos para elegir, como Chokdee Tae Tiam, Kuk Chai Dim Sum y Ama Dim Sum.
                                    <br><br>
                                    También puede comenzar el día con comida callejera como masa frita, panecillo frito, sopa de arroz, arroz con pollo y cerdo
                                </div>
                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'> </div>
                                <div class='texto-ruta-cuerpo' style='padding-top: 30px;'>
                                     rojo con arroz, o disfrutar de la comida tailandesa y china que se encuentra a lo largo de las calles principales como Ratyindee y Niphatsongkhro Roads.
                                    <br><br>
                                    A última hora de la mañana: vaya al mercado de Kim Yong, donde podrá encontrar todos los bocadillos famosos de varios países, frutas importadas, mariscos secos del lago Songkhla, comida callejera y restaurantes tailandeses y chinos. 
                                </div>

                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'> </div>
                                <div class='texto-ruta-cuerpo ' style='padding-top: 30px;'>
                                    Recomendamos Ta Yen Betong, un restaurante chino que ha estado en el negocio durante medio siglo, y el restaurante Ar. A última hora de la tarde: una joya escondida, Nai Roo ("en un agujero") es un pequeño restaurante chino escondido en un pequeño callejón (accesible desde Niphat-uthit 3 Soi 1).
                                </div>
                            </div>`,
                "titulo2":`<div class='subtitulo-d'>Día 2</div>`,
                "capa4":`<div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Para el almuerzo: tome un almuerzo rápido en Khanomjeen Pa Chuen 2 en Ratuthit Soi 18, donde podrá probar el auténtico pollo Hat Yai servido con un sabroso chapuzón y cebolla frita.
                                </div>
                        </div>
                        <div class='column-n-extra cnn'>
                            <div class='subtitulo-d'>Tarde</div>
                            <div class='texto-ruta-cuerpo'>
                                El mercado flotante de Khlong Hae es el primer mercado flotante de la región sur que ofrece comida sureña, tanto tailandesa como musulmana. La comida está recién hecha en los barcos. A última hora de la tarde: pruebe la sabrosa y picante comida sureña en Kan Eng Hat Yai, que ofrece comida china, tailandesa y del sur de Tailandia. La cola es larga, pero definitivamente vale la pena esperar. Una vez allí, no olvide probar el cangrejo ho mok (cangrejo al vapor en hojas de plátano y sopa agria con coco).
                            </div>
                        </div>`,
                "capa5":`<div class='subtitulo-d'>Día 3</div>
                                <div class='texto-ruta-cuerpo '>
                                    Al final de la mañana: camine por el casco antiguo de Songkhla para conocer el estilo de vida de los lugareños y disfrute de una serie de refrigerios en el camino en Nang Ngam Road. Tome fotografías del arte callejero que se encuentra en las paredes y edificios antiguos. Esta zona también alberga antiguos restaurantes que existen desde hace 60 años, transmitidos de generación en generación. Estas publicaciones clásicas existen junto a cafés modernos y vanguardistas, por lo que puede encontrar todo tipo de alimentos para comer en el camino, desde fideos hasta postres tradicionales tailandeses. Tae Hiang Aew Songkhla parece ser una tienda ordinaria. Este restaurante de 85 años sirve deliciosa comida tailandesa y china, como ensalada picante de mango y tofu con carne de cangrejo.
                                    <br><br>
                                    Sukiyaki Nakhon Nai es donde puede disfrutar del auténtico sukiyaki y un delicioso dip de tofu fermentado. Khao Stew Kiat Fang vende sabroso cerdo guisado con hierbas chinas y bollos suaves y tiernos recién horneados al vapor frente a la tienda. Pig's Tail Noodle Shop usa azúcar de palma para darle a la sopa un sabor distintivo, mientras que en Ko Ban Noodle Shop, el helado se sirve en un mini frasco, mientras que el helado Jew, que es un nombre famoso y establecido en este vecindario, encabeza su hielo. nata con yema de huevo y cacao en polvo. Si desea comprar bocadillos y artesanías locales, vaya a la tienda de postres Song Saeng y a la tienda Thai Kul Prakhong.
                                    <br><br>
                                    Al final de la tarde: cene mariscos frescos y disfrute de la vista del lago Songkhla en la isla Yo. El restaurante Sirada es famoso tanto por sus vistas como por su comida. Nam Khiang Din te ofrece una vista panorámica de la isla Yo y una puesta de sol perfecta.
                                    <br><br>
                                    <div class='cancel'>
                                        1. Chok Di Dim Sum, un artículo de Tae Tiam que tiene varios tipos de alimentos para elegir, incluidos Khanom Chip (dulce tailandés), Sala Pao (pan) y Bak Kut Teh (caldo de costilla de cerdo).
                                        <br><br>
                                        2. Ar Restaurant, un reconocido restaurante chino en el centro de Hat Yai.
                                        <br><br>
                                        3. Kai Sap Betong (pollo picado en Betong), un plato famoso del Tayern Betong, un antiguo restaurante Chiness en Hat Yai.
                                    </div>
                                </div>`,
                "capa6":`<div class='column-doble'>
                                    <div class='subtitulo-d'></div>
                                    <div class='texto-ruta-cuerpo '>
                                        <br>
                                        <div class='cancel'>
                                            4. Una combinación de comida deliciosa en el mercado flotante de Khlong Haeng, el único mercado flotante del sur.
                                            <br><br>
                                            5. El casco antiguo de Songkhla, una fuente de recetas antiguas integradas con comida callejera a lo largo de varios caminos.
                                            <br><br>
                                            6. Yam Mamuang, un plato de autor del restaurante Tae Hiang Aew en el casco antiguo de Song Kha. (Carretera Nang Ngam)
                                            <br><br>
                                            7. Postres tailandeses antiguos que son difíciles de encontrar y Khanom Khai que se vende caliente en una estufa de carbón.
                                        </div>
                                    </div>
                                </div>
                                <div class='column-doble' style='margin-left:7%'>
                                    <div class='subtitulo-d'></div>
                                    <div class='texto-ruta-cuerpo'>
                                        <br>
                                        
                                    </div>
                                </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
            6:{
                "titulo1":`PHUKET, PERLA DEL ANDAMAN`,
                "txt1":`Hay más en la "Perla de Andaman" que sus playas vírgenes y sus aguas cristalinas. Su diversa cultura local, una fusión de influencias chinas, hokkien, islámicas, peranakan y del sur de Tailandia, lo convierte en un gran centro gastronómico.`,
                "capa2":` <div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Visite el casco antiguo de Phuket para experimentar un desayuno al estilo de Phuket. Puede disfrutar del dim sum al estilo de Hokkien o desafiar su paladar con el picante Khanom Chin. El curry roti es otra opción si te apetece un plato musulm    
                                </div>
                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'> </div>
                                <div class='texto-ruta-cuerpo '>
                                    Al final de la mañana: tome una taza de café en Kopi Tiam (cafetería de estilo chino) y disfrute de los bocadillos locales de Phuket; como A-pong, Ao Io, Tao So y Bi Fang. Para el almuerzo, pruebe platos locales sencillos como Bi Hun con costillas de cerdo o fideos al estilo Hokkien.    
                                </div>
                            </div>
                            <div class='column-triple'>
                                <div class='subtitulo-d'>Tarde</div>
                                <div class='texto-ruta-cuerpo '>
                                    Tome un largo paseo por el casco antiguo y observe la arquitectura chino-portuguesa, el arte callejero y la cultura Peranakan en un nuevo museo, el Museo Peranakan Phuket. Pruebe una mezcla ecléctica de cocinas china y malaya.
                                </div>
                            </div>`,
                "titulo2":`<div class='ruta'>Día 2</div>`,
                "capa4":`<div class='column-triple'>
                                <div class='subtitulo-d'>Mañana</div>
                                <div class='texto-ruta-cuerpo '>
                                    Visite el punto escénico de Phuket para disfrutar de una hermosa vista de Khao Rang. Almuerce en Thung Kha Coffee, donde podrá disfrutar de cafés especiales y lo más destacado de Phuket; como pasta de chile de camarones, camarones salteados con frijoles amargos y curry de coco con hoja de liang.
                                </div>
                        </div>
                        <div class='column-n-extra capa-7'>
                            <div class='column-doble'>
                                    <div class='subtitulo-d'>Tarde</div>
                                    <div class='texto-ruta-cuerpo '>
                                        A final de la tarde: cuando el sol comience a ponerse, dé un paseo para explorar el estilo de vida local en Lat Yai, una calle peatonal que vende recuerdos, comida callejera y platos locales de Phuket. Puede comprar y comer hasta más no poder.
                                    </div>
                            </div>
                            <div class='column-doble' style='margin-left:6%'>
                                    <div class='subtitulo-d'>Noche</div>
                                    <div class='texto-ruta-cuerpo '>
                                        ¿Necesita una recarga? Consiga papilla de arroz o sopa de fideos en Ko Benz Khao Tom Hang, un famoso local con una larga cola y gran calidad.
                                    </div>
                            </div>
                        </div>`,
                "capa5":`<style>.container-n-imagen2{
                            
                        }
                        .container-n-imagen2 img{
                            margin-top:32px;
                        }
                        </style>
                        <div class='subtitulo-d'>Día 3</div>
                                <div class='texto-ruta-cuerpo '>
                                    Al mediodía: Hay muchos restaurantes en Phuket que venden platos locales de Phuket; como cerdo salteado con sal, caballa rellena, cangrejo negro salteado con jugo de limón, y Khanom Chin y cangrejo al curry. 
                                    <br><br>
                                    Al final de la tarde: navegue hacia el mar para disfrutar de mariscos frescos en Kruvit Raft. Los platos imperdibles de este restaurante son el mero de punta negra frito, el cangrejo al vapor y el mero de punta negra en sopa de soja fermentada. O pruebe un toque moderno de la comida de fusión del sur de Tailandia en Dibuka, donde los platos locales reciben un toque contemporáneo y una presentación creativa.
                                    <br><br>
                                    <div class='cancel'>
                                        1. Antiguo dim sum al estilo de Phuket que se puede comer por la mañana en Bunyarat o Juan Hiang.
                                        <br><br>
                                        2. Postres locales de Ah Pong Mae Sunee en Yaowarat Road / Khanom Chin Kaeng Pu (fideos de arroz con curry de cangrejo) en Khanom Chin Saphan Hin.
                                        <br><br>
                                        3. En Phuket, la ciudad de la cultura, el desayuno puede incluir Roti Kaeng Nam y Pad Mi Hokkien (fideos fritos al estilo Hokkien).
                                        <br><br>
                                        4. La elegante arquitectura de las cafeterías y el estilo de vida local de Phuket.
                                        <br><br>
                                        5. Lat Yai es una calle peatonal que es una fuente de diversas delicias locales de Phuket y el sur que puede seleccionar para comprar y comer.
                                        <br><br>
                                        6. Kruvit Raft, un restaurante de mariscos que flota en el mar en Phuket y tiene mariscos frescos de las canastas.
                                        <br><br>
                                        7. Comida local adaptada de Phuket que es similar a Local Phuket Fusion en Dibuka en Dibuk Road.
                                    </div>
                                </div>`,
                "capa6":`<div>
                            </div>`,
                "content5":`<div>
                            </div>`,
                "content6":`<div>
                            </div>`
            },
        }
        $(".btn-ruta").on("click",function(){
            let ruta=$(this).data("ruta")
            $(".btn-ruta").removeClass("btn-active")
            $(this).addClass("btn-active")
            // console.log(json[ruta-1])
            // <img src="/public/img/web/5/r1/mapa.jpg" width="100%" height='100%'>

            $("#mapa").hide().html(`<img src="/public/img/web/5/r${ruta}/mapa.jpg" width="100%" height='100%'>`).fadeIn(500)
            $("#d-1").hide().html(`<img src="/public/img/web/5/r${ruta}/d-img-1.jpg" width="100%" height='100%' style='object-fit: cover;'>`).fadeIn(500)
            $("#d-2").hide().html(`<img src="/public/img/web/5/r${ruta}/d-img-2.jpg" width="100%" height='100%' style='object-fit: cover;'>`).fadeIn(500)
            $("#d-3").hide().html(`<img src="/public/img/web/5/r${ruta}/d-img-3.jpg" width="100%" height='100%' style='object-fit: cover;'>`).fadeIn(500)
            // <img src="/public/img/web/5/r1/d-img-1.jpg" width="100%" height='100%'>

            $("#n-img-1").hide().html(`<img src="/public/img/web/5/r${ruta}/n-img-1.jpg" width="100%" height='100%'>`).fadeIn(500)
            if(ruta==7){
                $("#n-img-2").hide().html(`<img src="/public/img/web/5/r${ruta}/n-img-2.JPG" width="100%" height='100%'>`).fadeIn(500)
            }else{
                $("#n-img-2").hide().html(`<img src="/public/img/web/5/r${ruta}/n-img-2.jpg" width="100%" height='100%'>`).fadeIn(500)
            }
            $("#txtNumRuta").html(ruta);
            $("#txt-titulo-1").html(json[ruta-1]["titulo1"]);
            $("#txt-1").html(json[ruta-1]["txt1"]);
            
            // $("#content-imagenes").html(json[ruta-1]["content-imagenes"])
            $("#titulo2").html(json[ruta-1]["titulo2"]);
            // $("#txt-dia").empty();
            // if(ruta!=3 && ruta!=5 && ruta!=6){
            //     $("#txt-dia").html("Día 2")
            // }
            // $("#txt-titulo-2").html(json[ruta-1]["titulo2"]);
            // $("#text-titulo-2").empty()
            // if(json[ruta-1]["text-titulo2"]){
            //     $("#text-titulo-2").html(json[ruta-1]["text-titulo2"])
            // }
            $(".capa-6").empty();

            $(".capa-2").html(json[ruta-1]["capa2"]);
            $(".capa-4").html(json[ruta-1]["capa4"]);
            $("#s-capa-5").html(json[ruta-1]["capa5"]);
            $(".capa-6").html(json[ruta-1]["capa6"]);

            
            // $("#content-3").html(json[ruta-1]["content3"]);
            // $("#content-4").html(json[ruta-1]["content4"]);
            // $("#content-5").html(json[ruta-1]["content5"]);
            // $("#content-6").html(json[ruta-1]["content6"]);
        })
        let jsonImagenes={
            0:{
                t1:"KHAO PAD PU",
                t2:"ARROZ FRITO CON CANGREJO",
                t3:"Lo más destacado de este plato es la carne de cangrejo, que se saltea bien con arroz y huevo.",
            },
            1:{
                t1:"MOO PING",
                t2:"CERDO A LA PLANCHA",
                t3:"La tierna carne de cerdo se adoba en especias y adquiere un sabroso aroma de la parrilla. Por lo general, se come con arroz glutinoso caliente.",
            },
            2:{
                t1:"KHAO KAPRAO <br> GAI KAI DAO",
                t2:"POLLO FRITO CON ALBAHACA Y HUEVO FRITO",
                t3:"El sabor picante con la aromática albahaca lo convierte en una deliciosa comida típica tailandesa que es fácil de preparar.",
            },
            3:{
                t1:"KHAO MUN GAI",
                t2:"ARROZ DE POLLO HAINANÉS",
                t3:"Arroz y pollo tierno cocinado con jengibre y pasta de frijoles.",
            },
            4:{
                t1:"PAD THAI",
                t2:"FIDEO FRITOS TAILANDESES",
                t3:"Un plato de fideos delicioso que es mundialmente conocido. Quien lo intente se enamorará de los fideos suaves fritos mezclados con salsa de tamarindo y especias. El plato también se puede complementar con langostinos frescos.",
            },
            5:{
                t1:"HOI TOD",
                t2:"MEJILLONES FRITOS",
                t3:"Un plato apetitoso de mejillones grandes y huevos que se cubren con harina de arroz y se fríen en una sartén grande hasta que estén suaves y crujientes. Se sirve caliente cubierto con brotes de frijol junto con una salsa picante y adornado con cilantro y chile.",
            },
            6:{
                t1:"MOO SATAY",
                t2:"SATÉ DE CERDO",
                t3:"Un bocadillo muy popular. Es una brocheta de cerdo a la parrilla con hierbas tailandesas a la parrilla en una estufa de carbón caliente. La leche de coco se vierte sobre el cerdo mientras se asa a la parrilla hasta que el aroma dulce lo invita a disfrutarlo con salsa de maní, guarniciones de pepino, chile y vinagre.",
            },
            7:{
                t1:"GUAY TIEW KUA GAI",
                t2:"FIDEOS ASADOS CON POLLO",
                t3:"Un plato a la carta de gran éxito que tiene un sabor excepcional al usar una estufa de carbón caliente. Los aromáticos fideos asados, crujientes por fuera y suaves por dentro, se cuecen con pollo, calamares crujientes y huevo.",
            },
            8:{
                t1:"GUAY TIEW RUEA",
                t2:"FIDEOS EN BARCO",
                t3:"Tallarines con carne de res o cerdo con guarnición. Su nombre se deriva de la forma tradicional en que se solían cocinar y servir los fideos en los barcos en los mercados flotantes en la antigüedad. Se come con chicharrón de cerdo y albahaca para darle un aroma agradable.",
            },
            9:{
                t1:"KHANOM JEEN<br>NAM YA",
                t2:"FIDEOS DE ARROZ CON PASTA DE CURRY",
                t3:"Khanom Jeen (fideos de arroz vermicelli) que se comen con pasta de curry o pasta de chile. El Khanom Chin más común es el de la Región Central, que se come con pasta de curry de pescado y verduras.",
            },
            10:{
                t1:"GAI YANG",
                t2:"POLLO FRITO",
                t3:"El pollo picante con hierbas se asa a la parrilla en una estufa de carbón hasta que se cocine, lo que desprende un aroma fragante. Se come con salsa dulce en la Región Central o salsa agria en la Región Noreste. El pollo es fácil de comer como bocadillo o con arroz cocido al vapor o arroz glutinoso.",
            },
            11:{
                t1:"PAD SEE EW ",
                t2:"FIDEOS FRITOS EN SALSA DE SOJA",
                t3:"Una comida rápida al estilo chino que está hecha con fideos grandes o fideos vermicelli, que se saltean con carne de cerdo tierna, pollo, ternera o mariscos. El aroma fragante de la salsa de soja cuando se fríe en la sartén junto con la col rizada china la hace popular.",
            },
            12:{
                t1:"KHAO NIEW MAMUANG",
                t2:"ARROZ GLUTINOSO CON MANGO",
                t3:"Un postre tailandés popular de la temporada de calor conocido en todo el mundo. El fragante aroma del arroz glutinoso con la leche fresca de coco, se puede comer con varios aderezos como natillas, camarones o pescado. Los tailandeses disfrutarán de una bola de arroz glutinoso con frutas como mango, durián, etc.",
            },
            13:{
                t1:"NAM SAMUNPRAI",
                t2:"BEBIDAS HERBALES PARA LA SALUD",
                t3:"Una parte importante de la vida tailandesa. Las hierbas se han transformado en una bebida para saciar la sed a través de su delicioso sabor y propiedades terapéuticas. Estos se pueden comprar como bebida saludable embotellada. Son de varios sabores como hierbaluisa, guisante de mariposa, maracuyá, roselle y fruta bael.",
            },
            14:{
                t1:"CHA YEN",
                t2:"TÉ HELADO TAILANDÉS",
                t3:"Un helado de estilo tailandés de renombre mundial, endulzado con leche condensada y que le da un delicioso sabor que lo refrescará del calor.",
            },
        }
        let slideActual=0;
        const max=4;
        $(document).ready(function(){
            swiper.slideTo(1);
            if(idRuta){
                // alert(idRuta)
                $(".btn-ruta[data-ruta='"+idRuta+"']").trigger("click");
                // console.log($(".btn-ruta[data-ruta='"+idRuta+"']"))
            }
        })
        $(".btn-prev").on("click",function(){
            swiper.slidePrev();
            slideActual=(swiper.realIndex!=0)?swiper.realIndex:15;
            $("#slide-1").hide().html(`<img src="/public/img/web/5/slide-${slideActual}.jpg" style='display: block;width: 100vw;height: 100vh;object-fit: cover;' class='w-100 h-100' style='opacity: 0.70;'>`).fadeIn(500)
            setDatos(jsonImagenes[slideActual-1])
        })
        $(".btn-next").on("click",function(){
            swiper.slideNext();
            slideActual=(swiper.realIndex!=0)?swiper.realIndex:15;
            $("#slide-1").hide().html(`<img src="/public/img/web/5/slide-${slideActual}.jpg" style='display: block;width: 100vw;height: 100vh;object-fit: cover;' class='w-100 h-100' style='opacity: 0.70;'>`).fadeIn(500)
            setDatos(jsonImagenes[slideActual-1])
        })

        function setDatos(info){
            $("#t1").hide().html(info["t1"]).fadeIn(300)
            $("#t2").hide().html(info["t2"]).fadeIn(300)
            $("#t3").hide().html(info["t3"]).fadeIn(300)
        }
    </script>
</body>
</html>