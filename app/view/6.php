<?php
    require './app/controller/6.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Noticias</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v3.2">
    <link rel="stylesheet" href="/public/assets/web/css/6.css?v4.5">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>

</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <section class='s1'>
            <div class='w-100 d-flex'>
                <div class='c-img-1' style='background-image:url(/public/img/web/6/img-1.jpg)'>
                    <div class='h-100 d-flex align-items-center position-relative' style='margin-left:5%'>
                        <div class='c-titulo-main headline-bottom'>
                                <div class='txt-1'></div>
                                    <div class='txt-2'>NOTICIAS</div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class='p-4 s2 pt-5'>
            <div class='d-flex justify-content-center flex-wrap w-100 h-100' style='min-height:200px'>
                <div class='d-flex w-100 m-3 comodin pk1' style='max-width:1500px;min-height:750px'>
                    <div class='c1 cp mr-3 headline-bottom-fixed d-none' id='n1'>
                        <!-- <img class='img-cover' src="/public/img/data/img-1Noticia.png">
                        <div class='c-info'>
                            <div class='m-fecha'>
                                <div class='mes'>Marzo</div>
                                <div class='num'>19</div>
                            </div>
                            <div class='m-data w-100'>
                                <div class='pt-1 pb-3'>
                                    <div>
                                        <button class='btn n-categoria'>Gastronomia</button>
                                    </div>
                                    <text class='n-titulo'><a href='/noticia/1?noticia=FOODLANDIA ES LANZADO EN LATINOAMERICA'>FOODLANDIA ES LANZADO EN LATINOAMERICA</a></text>
                                    <div class='n-descripcion'>Lanzamiento oficial en Lima</div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class='c2 cp ml-3 headline-bottom-fixed d-none' id='n2'>
                       
                    </div>
                </div>
                <div class='d-flex w-100 m-3 comodin pk2 mt-3' style='max-width:1500px;min-height:380px'>
                    <div class='c3 cp mr-3 headline-bottom d-none' id='n3'>
                    </div>
                    <div class='c4 cp ml-3 headline-bottom d-none' id='n4'>
                    </div>
                </div>

                <!-- ---------------------- -->
                <div class='d-flex w-100 m-3 comodin pk3' style='max-width:1500px;min-height:750px'>
                    <div class='c1 cp mr-3 headline-bottom-fixed d-none' id='n5'>

                    </div>
                    <div class='c2 cp ml-3 headline-bottom-fixed d-none' id='n6'>
                       
                    </div>
                </div>
                <div class='d-flex w-100 m-3 comodin pk4 mt-4' style='max-width:1500px;min-height:380px'>
                    <div class='c3 cp mr-3 headline-bottom d-none' id='n7'>
                    </div>
                    <div class='c4 cp ml-3 headline-bottom d-none' id='n8'>
                    </div>
                </div>
                
            </div>
            <div class='paginado d-flex justify-content-center align-items-center w-100'>
                <div class='d-flex'>
                    <button class='btn btn-p' style='color:#fff;background-color:var(--color1)'><</button>
                        <div style='margin:10px'>1</div>
                    <button class='btn btn-n' style='color:#fff;background-color:var(--color1)'>></button>
                </div>
            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>

    </main>
    <script>
        let json=<?php echo $json?>;
        let jsonContador=<?php echo $jsonContador?>;
        let pagina=<?php echo $pagina?>;
        // console.log(json)
        moment.locale("es"); 
        if(!json.error){
            let contador=0;
            $.each(json,function(k,v){
                contador++;
                let img=$("<img>",{class:"img-cover",src:"/public/img/data/"+v.img1})
                let divInfo=$("<div>",{class:"c-info"})
                // ------
                let divFecha=$("<div>",{class:"m-fecha"})
                let divMes=$("<div>",{class:"mes",text:moment(v.fecIngreso).format("MMMM")})
                let divNum=$("<div>",{class:"num",text:moment(v.fecIngreso).format("D")})
                // ------
                let divData=$("<div>",{class:"m-data w-100"})
                let divAux=$("<div>",{class:"pt-1 pb-3"})
                // ------
                let div=$("<div>").append($("<button>",{class:"btn n-categoria",text:"Gastronomia"}))
                let text=$("<text>",{class:"n-titulo"}).append($("<a>",{href:"/noticia/"+v.id+"?noticia="+v.nombre.replace(" ","_"),
                                                                     text:v.nombre}))
                let divDescripcion=$("<div>",{class:"n-descripcion",text:v.subTitulo});
                // -------
                divFecha.append(divMes,divNum);
                divAux.append(div,text,divDescripcion)
                divData.append(divAux)

                divInfo.append(divFecha,divData)
                $("#n"+(parseInt(k)+1)).append(img,divInfo).removeClass("d-none")
            })
            $(".s2 d-none").remove();
            
            $( window ).resize(function() {
                // $( "body" ).prepend( "<div>" + $( window ).width() + "</div>" );
                if(contador==1 && $(window).width()<=1000){
                    $(".pk1").addClass("cv")
                }else{
                    $(".pk1").removeClass("cv")
                }
            });
            if(contador==1 && $(window).width()<=1000){
                $(".pk1").addClass("cv")
            }else{
                $(".pk1").removeClass("cv")
            }

            // ----------------------------------------
            if(contador==1 || contador==2){
                $(".pk2,.pk3,.pk4").remove();
            }
            if(contador==3 || contador==4){
                $(".pk3 ,pk4").remove();
            }
            if(contador==5 || contador==6){
                $(".pk4").remove();
            }
        }

        if(pagina*8<jsonContador.cantidad){
            
        }else{
            $(".btn-n").prop("disabled",true)
        }

        if(pagina==1){
            $(".btn-p").prop("disabled",true)
        }

        $(".btn-n").click(function(){
            location.href="/noticias/"+(pagina+1);
        })
        $(".btn-p").click(function(){
            location.href="/noticias/"+(pagina-1);
        })

    </script>
</body>
</html>