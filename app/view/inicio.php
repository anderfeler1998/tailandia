<?php
        require './app/controller/inicio.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Disfruta del sabor de Tailandia</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
       echo VIDEOJS;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="./public/assets/web/css/general.css?v5.0">
    <link rel="stylesheet" href="./public/assets/web/css/inicio.css?v7.8">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <style>
            #fm-video[poster]{
                object-fit: cover;
                /* height: 100% !important;
                width: 100% !important; */
            }
            .video-js {
                position: relative !important;
                width: 100% !important;
                height: 100% !important;
                display:none;
            }
            #frame-video{
                display:none;
            }
        </style>
        <section class='headline'>
            <div class='c-slide-first'>
                <div class='w-100 h-100'>
                    <!-- Swiper -->
                    <div class="swiper-container">
                        <div class="swiper-wrapper sll">
                            <!-- <iframe width="1799" height="664" src="https://www.youtube.com/embed/3ZANAcZtrs0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                            <video id='poster-video' src="/public/video/web/loop.mp4" autoplay='true' loop="true" muted='true' style='object-fit: cover;' width='100%' height='100%'></video>
                            <iframe id='frame-video'  width="100%" height="100%" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <!-- <video id='fm-video' class='video-js vjs-big-play-centered'>
                                <source src="/public/video/web/presentacion.mp4" type="video/webm" >
                            </video> -->
                            <!-- <div class="swiper-slide" style="background-image:url('./public/img/web/1.png');"></div>
                            <div class="swiper-slide" style="background-image:url(https://ep01.epimg.net/elcomidista/imagenes/2017/12/11/articulo/1513007223_057537_1513088739_noticia_normal.jpg)"></div>
                            <div class="swiper-slide" style="background-image:url(https://international-experience.es/wp-content/uploads/2019/08/comidas-mundo.jpg)"></div> -->
                        </div>
                    </div>
                    <div class='w-100 d-flex capas-slide-1'>
                        <div class='c-capa-oscura'>
                            <div class='capa-oscura'></div>
                        </div>
                        <div class='capa-info d-flex align-content-between flex-wrap' style='z-index:4'>
                            <div class='w-100'>
                            </div>
                            <div class='w-100' style='color:white;z-index:5'>
                                <div class='c-titulo-main'>
                                    <div style='' class='h-100 d-3'>
                                        <div class='titulo-1' id='tt-1'>A quien venga a</div>
                                        <div class='titulo-2' id='tt-2'>Tailandia,</div>
                                        <div class='d-flex justify-content-end'>
                                            <div class='t-gion'>
                                                <div id='t-gion'>
                                                ____________
                                                </div>
                                            </div>
                                            <div class='titulo-3 ml-2' id='tt-3'>no hay nadie que no ame<br> los exquisitos sabores y<br> la gran diversidad de la <br>cocina tailandesa.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='w-100 d-flex'>
                                <div id='btn-verVideo' class='d-flex justify-content-center align-items-center c-siguiente clickable'>
                                    VER VIDEO
                                </div>
                                <!-- <div class='c-prev clickable' id='s1-btn-prev'>
                                    <svg class="bi" width="20" height="20" fill="currentColor">
                                        <use xlink:href="public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-left"/>
                                    </svg>
                                </div>
                                <div class='c-next'>
                                    <div class='capa-c-next clickable' id='s1-btn-next'>

                                    </div>
                                    <svg class="bi fn clickable" width="20" height="20" fill="currentColor" id='s1-icon-next'>
                                        <use xlink:href="public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-right"/>
                                    </svg>
                                </div> -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class='c-slide-second'>
                <!-- <div class="swiper-container swiper2">
                    <div class="swiper-wrapper"> -->
                        <div class="swiper-slide overflow-hidden d-flex flex-wrap">
                            <div class='d-comodin-platos d-flex justify-content-around align-items-center ml-5 mb-5 mt-5 flex-wrap w-100'>
                                <div class='col-md-5 p-3 headline-bottom'>
                                    <div class='s-Title'>
                                        Una forma <br>de felicidad al viajar
                                    </div>
                                    <br>
                                    <div class='s-Text'>
                                        por Tailandia que abrirá nuevas experiencias sobre el país y hará que quieras seguir viajando es “comer”.
                                        <br><br>
                                        Te sorprenderán las maravillas de la cocina tailandesa que te permitirán experimentar el estilo de vida tailandés, que refleja claramente la identidad de cada región de Tailandia.
                                        <br><br>
                                        Este sitio web lo llevará a un viaje increíble para conocer más sobre los deliciosos platos de Tailandia, la comida local que no debe perderse, independientemente de si se trata de comida callejera, fruta tailandesa, famosos platos antiguos o platos de la Corte Real. 


                                    </div>
                                </div>
                                <!-- <div class='pt-3 pb-3 pl-3'> -->
                                <div class='col-md-6 p-0 d-flex align-items-center c-c-platos headline-bottom'>
                                        <div class='c-platos'>
                                            <img src="./public/img/web/inicio/plato1.png" alt="">
                                        </div>
                                        <div class='c-platos'>
                                            <img src="./public/img/web/inicio/plato2.png" alt="">
                                        </div>
                                        <div class='c-platos c-plato-3'>
                                            <img src="./public/img/web/inicio/plato3.png" alt="">
                                        </div>
                                </div>
                            </div>
                            <div class='d-flex justify-content-md-around flex-wrap flex-md-nowrap justify-content-center align-items-center w-100 c-ld-2'>
                                <div class='c-img headline-bottom'>
                                    <!-- <img src="./public/img/web/inicio/platos.png" width='100%' height='100%'> -->
                                    <div class="swiper-container swiper1 w-100 h-100">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <img src="./public/img/web/inicio/sw1.jpg" width='100%' height='100%' style='object-fit: cover;'>
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="./public/img/web/inicio/sw2.jpg" width='100%' height='100%' style='object-fit: cover;'>
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="./public/img/web/inicio/sw3.jpg" width='100%' height='100%' style='object-fit: cover;'>
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="./public/img/web/inicio/sw4.jpg" width='100%' height='100%' style='object-fit: cover;'>
                                            </div>
                                            <!-- <div class="swiper-slide" style="background-image:url(https://ep01.epimg.net/elcomidista/imagenes/2017/12/11/articulo/1513007223_057537_1513088739_noticia_normal.jpg)"></div>
                                            <div class="swiper-slide" style="background-image:url(https://international-experience.es/wp-content/uploads/2019/08/comidas-mundo.jpg)"></div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class='c-text headline-bottom'>
                                    <div>
                                        <div class='s-Title'>
                                            Definitivamente te enamorarás de Tailandia y de la cocina tailandesa.
                                        </div>
                                        <br>
                                        <div class='s-Text'>
                                            <p>
                                                La Autoridad de Turismo de Tailandia (TAT), con el apoyo de las Embajadas del Reino de Tailandia en todo el mundo, desempeña un papel importante en la promoción de la cocina tailandesa a través de varias iniciativas, incluido el lanzamiento de la campaña Amazing Thai Taste en el 2016, varias publicaciones sobre platos e ingredientes tailandeses y los mejores lugares para encontrarlos, así como patrocinando una gran cantidad de eventos basados en alimentos en todo el mundo.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="swiper-slide">Slide 2</div> -->
                    <!-- </div>
                </div> -->
            </div>
            <div class='cinta-btns'>
                <div class='c-btn2'>
                    <div class='c-prev clickable'>
                        <svg class="bi" width="20" height="20" fill="currentColor">
                            <use xlink:href="public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-left"/>
                        </svg>
                    </div>
                    <div class='c-next clickable'>
                        <div class='capa-c-next'>

                        </div>
                        <svg class="bi fn clickable" width="20" height="20" fill="currentColor">
                            <use xlink:href="public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-right"/>
                        </svg>
                    </div>
                </div>
            </div>
        </section>
        <section style='padding:2rem 1rem' class='seccion4' id='descubre_la_cocina_tailandesa'>
            <div class='d-flex justify-content-between s4-comodin align-items-center'> 
                <div style='color:white;padding-left:6rem;padding-right:6rem' class='headline-bottom c-descubre'>
                    Descubra la <br>
                    <h2>Cocina<br>Tailandesa</h2>
                    <div class="swiper-pagination-3"></div>
                </div>
                <div class='c-slide-three' style='overflow-y:hidden'>
                    <div class="swiper-container swiper3 headline-bottom">
                        <div class="swiper-wrapper">
                            <?php
                               if(array_key_exists("error",$json)){
                            ?>

                            <?php
                               }else{
                                   foreach($json as $key=>$value){
                            ?>
                                <div class="swiper-slide <?php echo (count($json)==($key+1))?"mr-1":"" ?>" >
                                    <!-- Slide 1 -->
                                    <div class='s-receta noselect'  style='margin-left:11px;z-index:<?php echo count($json)-intval($key);?>'>
                                        <div class='s-img-logo'>
                                            <img src="./public/img/web/LogoReceta.png">
                                        </div>
                                        <div class='s-img-receta'>
                                            <img src="./public/img/data/<?php echo $value["imgPlato"] ?>">
                                        </div>
                                        <!-- -------------------------------------- -->
                                        <div class='s-detalle'>
                                            <div class='w-100'></div>
                                            <div class='txt-detalle w-100'>
                                                <li>
                                                    <img src="./public/img/web/reloj.png">
                                                    <text><?php echo $value["duracion"] ?></text>
                                                </li>
                                                <li>
                                                    <img src="./public/img/web/persona.png">
                                                    <text><?php echo ($value["personas"])?($value["personas"]." "."persona(s)"):"" ?></text>
                                                </li>
                                            </div>
                                            <div class='w-100'>
                                                <div class='mt-3 w-100 txt-nameReceta'>
                                                    <?php
                                                       echo $value["preTitulo"]." ".$value["titulo"]
                                                    ?>
                                                </div>
                                                <div class='mb-4 w-100 txt-desReceta'>
                                                    <?php
                                                       echo ($value["subTitulo"])?$value["subTitulo"]:"";
                                                    ?>
                                                </div>
                                                <?php
                                                   $parametroAdorno=str_replace(" ","_",$value["preTitulo"]." ".$value["titulo"]);
                                                ?>
                                                <a class='btn btn-verRecta' href='/receta/<?php echo $value["id"]."?".$parametroAdorno?>' >VER RECETA</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                   }
                               }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>
    <script>
        $("#btn-verVideo").on("click",function(){
            $("#poster-video").fadeOut(300);
            $(".capa-info").fadeOut(300)
            $(".c-buscador").fadeOut(300)
            $(".capa-info").removeClass("d-flex")
            $(".capa-info").addClass("d-none")
            $(".c-capa-oscura").fadeOut(300)

            setTimeout(() => {
                $("#frame-video").fadeIn(500);
                $("#frame-video").attr("src","https://www.youtube.com/embed/3ZANAcZtrs0?autoplay=1&rel=0")
                // $("#fm-video").fadeIn(300)
                // $("#fm-video").addClass("d-block")
                // let reproductor=videojs('fm-video', {
                //     CloseButton:true
                // });
                // reproductor.play()
                // reproductor.controls(true)
                // reproductor.CloseButton.on("click",function(){
                //     reproductor.dispose()
                //     $("#poster-video").fadeIn(300);
                //     $(".capa-info").fadeIn(300)
                //     $(".c-buscador").fadeIn(300)
                //     $(".c-capa-oscura").fadeIn(300)
                //     $(".capa-info").addClass("d-flex")
                //     $(".capa-info").removeClass("d-none")
                //     setTimeout(function(){
                //     },100)
                //     $(".sll").append(`<video id='fm-video' class='video-js vjs-big-play-centered'>
                //                             <source src="/public/video/web/presentacion.mp4" type="video/webm" >
                //                         </video>`)
                // })
            }, 500);

        })
        let json={
            0:{
                t1:"A quien venga a",
                t2:"Tailandia,",
                t3:"no hay nadie que no ame<br> los exquisitos sabores y<br> la gran diversidad de la <br>cocina tailandesa."
            },
            1:{
                t1:"Lo mejor de la",
                t2:"Cocina,",
                t3:"variedad y calidad en cada <br> receta de Tailandia."
            },
            2:{
                t1:"Unase a la",
                t2:"Revolución,",
                t3:"de la cocina Tailandesa, disfrutalo<br> solo en nuestro recetario."
            }
        }
        let numJsonActual=0;
        var s1 = new Swiper('.swiper1', {
            spaceBetween: 30,
            loop:true,
            effect: 'fade',
            speed:'600ms',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
        })
        var sl1 = document.querySelector('.swiper1').swiper
        $(".c-prev").on("click",function(){
            sl1.slidePrev();
            // if(json[numJsonActual-1]){
            //     numJsonActual--;
            //     cambiarTtitulos();
            // }
        })
        $(".capa-c-next,.fn").on("click",function(){
            sl1.slideNext();
            // if(json[numJsonActual+1]){
            //     numJsonActual++;
            //     cambiarTtitulos();
            // }
        })
        // function cambiarTtitulos(){
            
        //     $("#tt-1").hide().html(json[numJsonActual]["t1"]).fadeIn(500);
        //     $("#tt-2").hide().html(json[numJsonActual]["t2"]).fadeIn(500);
        //     $("#tt-3").hide().html(json[numJsonActual]["t3"]).fadeIn(500);
        //     $("#t-gion").hide().fadeIn(500);
        // }
        var s2 = new Swiper('.swiper2', {
            spaceBetween: 30,
            // effect: 'fade',
        });
        var s3 = new Swiper('.swiper3', {
            type: 'bullets',
            slidesPerView: 'auto',
            // slidesPerView: 4,
            <?php
                if(!array_key_exists("error",$json)){
                    if(count($json)>3){
                        // echo "loop: true,";
                    }
                }
            ?>
            // loop:true,
            // spaceBetween: 10,
            // lazyLoading:true,
            // centeredSlides: true,
            // dynamicBullets:true,
            // dynamicMainBullets:4,
            // paginationType: "custom",
            pagination: {
                el: '.swiper-pagination-3',
                clickable: true,
            },
        });
        setTimeout(function(){
            s3.update(true);
            s3.slideTo(0, 0)
        }, 1);
        $(".s-receta").on("mouseenter",function(){
            $(".c-slide-three .swiper-slide").removeClass("swiper-slide-active")
            $(this).parent().addClass("swiper-slide-active")
        })
  </script>
</body>
</html>