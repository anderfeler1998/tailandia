<?php
    require './app/controller/12.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Busqueda</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v4.2">
    <link rel="stylesheet" href="/public/assets/web/css/12.css?v4.2">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <section class='s1 headline'>
            <div class='w-100 d-flex'>
                <div class='c-img-1' style='background-image:url(/public/img/web/12/img-1.png)'>
                    <div class='txt-2 h-100 d-flex align-items-center position-relative' style='margin-left:5%'><strong>
                        BÚSQUEDA</strong>
                    </div>
                </div>
            </div>
        </section>
        <section class='s2 pt-5 pb-5'>
            <div class='m-auto' style='max-width:85%'>
                <div class='txt-1 headline-bottom'>Resultados de Búsqueda: <text id='txt-buscar-html'></text></div>
                <div class='c-resultados mt-5' style='max-width:800px'>
                        <?php
                            if(array_key_exists("error",$json)){
                        ?>
                            <div class='mt-3 mb-2 headline-bottom'>
                                <div class='t-resultado'>
                                    <a>SIN RESULTADOS</a>
                                </div>
                            </div>
                        <?php
                            }else{
                                foreach($json as $key=>$value){
                                    $href="";
                                    switch($value["tipo"]){
                                        case 1: 
                                            $href="/receta"."/".$value["id"]."?".str_replace(" ","_",$value["titulo"]);
                                        break;
                                        case 2: 
                                            $href="/que_es_la_cocina_tailandesa"."/".$value["id"]."/"."#slide";
                                        break;
                                        case 3: 
                                            $href="/turismo_gastronomico_en_tailandia"."/".$value["id"]."/"."#ruta";
                                        break;
                                        default:
                                        break;
                                    }
                        ?>
                                <div class='mt-3 mb-2 headline-bottom'>
                                    <div class='t-resultado'>
                                        <a href='<?php echo $href; ?>'>
                                            <?php echo (($value["tipo"]==1)?"RECETA":(($value["tipo"]==2)?"ICÓNICO":""))." ".$value["titulo"].(($value["subTitulo"])?" - ".$value["subTitulo"]:"")?>
                                        </a>
                                    </div>
                                    <div class='txt-resultado' style='max-width:750px'>
                                        <?php echo $value["descripcion"]?>
                                    </div>
                                </div>
                        <?php
                                }
                            }
                        ?>
                </div>
            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>

    <script>
        let buscar=""
            <?php
            if(isset($_GET["buscar"])){
            ?>
                buscar='<?php echo $_GET["buscar"]?>';
            <?php
            }
            ?>
            $("#txt-buscar-html").html(buscar)
    </script>
</body>
</html>