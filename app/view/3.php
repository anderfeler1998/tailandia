<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title>Foodlandia - El arte de la cocina</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v7.5">
    <link rel="stylesheet" href="/public/assets/web/css/3.css?v8.3">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
</head>
<body>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <section class='headline'>
            <div class='w-100 d-flex s1'>
                <div class='cc-all d-block'>
                    <div class='c-img-1' style='background-image:url(/public/img/web/3/img-1.jpg)'>
                        <!-- <img src="/public/img/web/11/img-1.png" class='w-100 h-100' style='opacity: 0.70;'> --> 
                    </div>
                    <div class='c-form d-flex justify-content-center align-items-center' style=''>
                        <div class='w-100 c-f text-right'>
                            <div class='txt-1'>
                                EL ARTE DE LA COCINA
                            </div>
                            <div class='txt-2'>
                                Tailandesa
                            </div>
                            <div class="txt-3">
                            La identidad única de la cocina tailandesa proviene de la sabiduría local del pasado que seleccionó y cosechó verduras y hierbas tailandesas y las transformó en un delicioso menú apetitoso lleno de valor nutritivo y beneficios para la salud.
                            <br><br>
                            Desde el pasado hasta el día de hoy, las verduras y hierbas tailandesas han sido el corazón de la cocina tailandesa. La cocina tailandesa está repleta de hierbas tailandesas como ingredientes clave que hacen que un plato tenga mucho sabor y brinden una variedad de sabores agrios, dulces, aceitosos, salados y picantes, que provienen de la mezcla de ingredientes, vegetales y hierbas en cada plato. Por ejemplo, la acidez y el picante de la popular sopa de Tom Yam Gung proviene de la hierba luisa y las hojas de lima kaffir.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class='p-5 d-flex justify-content-center flex-wrap s2' id='hierbas'>
            <div class='w-100'>
                <div class='txt-1 text-center headline-bottom' style='margin-bottom:20px; padding:0px;8%'>SECRETOS DETRÁS DE LA MAGIA<br> CULINARIA TAILANDESA LOCAL</div>
            </div>
            <div class='w-100 d-flex justify-content-center'>
                <div class='pt-2 pb-2 text-center headline-bottom tx-list' style='padding:0px;max-width:800px;font-size:14px;line-height:17px'>Tailandia tiene
                    abundancia de fuentes de alimentos debido a su ubicación en la zona tropical y la riqueza de las
                    condiciones agrícolas. La cocina tailandesa es típicamente conocida por la combinación
                    armoniosa de sabores que utilizan diferentes ingredientes, varios estilos de cocina y el uso
                    entusiasta de hierbas y especias frescas para crear un equilibrio entre los sabores dulce, ácido,
                    picante y salado.</div>
            </div>
            <div class='w-100 mt-3 d-flex justify-content-around flex-wrap' style='overflow-y:hidden'>
                <div class='d-flex flex-wrap justify-content-between m-2 w-100 headline-bottom c-comodin-list' style='max-width:400px'>
                    <div class='mr-1 cel-l' style='min-width:106.23px'>
                        1. Bai horapha <br>
                        2. Bai kraphrao <br>
                        3. Bai maeng lak <br>
                        4. Phak chi <br>
                        5. Rak Phak Chi <br>
                        6. Bai saranae <br>
                        7. Takrai <br>
                        8. Bai Makrut <br>
                        9. Manao <br>
                        10. Makrut <br>
                        11. Khing <br>
                        12. Krachai <br>
                        13. Khamin <br>
                        14. Prik khi nu <br>
                        15. Prik yuak
                    </div>
                    <div class='cel-r cel-r1' style='font-weight:600'>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/1.png'/>">Albahaca dulce</a> <br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/2.png'/>">Hojas de albahaca santa</a> <br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/3.png'/>">Albahaca limón o albahaca peluda </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/4.png'/>">Cilantro </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/5.png'/>">Raíz de cilantro </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/6.png'/>">Menta </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/7.png'/>">Hierba luisa </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/8.png'/>">Hoja de Lima kaffir </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/9.png'/>">Lima </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/10.png'/>">Lima kaffir </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/11.png'/>">Jengibre </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/12.png'/>">Raíz de dedo </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/13.png'/>">Cúrcuma </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/14.png'/>">Chile ojo de pájaro </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/15.png'/>">Pimienta de plátano</a>
                    </div>
                </div>
                <div class='d-flex flex-wrap justify-content-between m-2 w-100 headline-bottom c-comodin-list' style='max-width:400px'>
                    <div class='mr-1 cel-l'>
                        16. Prik lueang <br>
                        17. Prik chi fa daeng <br>
                        18. Prik haeng <br>
                        19. Kha <br>
                        20. Prik Thai on <br>
                        21. Hom daeng <br>
                        22. Kratiam <br>
                        23. Bai krawan <br>
                        24. Krachai <br>
                        25. Makham <br>
                        26. Bai toei <br>
                        27. Op choei <br>
                        28. Poi kuk <br>
                        29. Nam tan pip <br>
                        30. Krapi Thai
                    </div>
                    <div class='cel-r' style='min-width:179px;font-weight:600'>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/16.png'/>">Chile naranja grande tailandés </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/17.png'/>">Chile rojo grande tailandés </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/18.png'/>">Chile seco </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/19.png'/>">Galangal </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/20.png'/>">Granos de pimienta verde </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/21.png'/>">Chalota </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/22.png'/>">Ajo </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/23.png'/>">Hojas de laurel </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/24.png'/>">Jengibre Krachai </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/25.png'/>">Tamarindo </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/26.png'/>">Hoja de pandan </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/27.png'/>">Canela </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/28.png'/>">Anís estrella </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/29.png'/>">Azúcar de palma </a><br>
                        <a data-toggle="tooltip" class='tool-white' title="<img width='120px' height='120px' src='/public/img/web/3/30.png'/>">Pasta de camarones </a><br>
                    </div>
                </div>
            </div>
        </section>
        <section class='s3 d-flex justify-content-around align-items-center tecnicas_de_cocina' id='tecnicas_de_cocina' style='overflow:hidden'>
            <div class='headline-bottom cc-titulo' style='max-width:450px;width:100%'>
                <div class='mb-3' style='font-size: 21px; line-height: 25px; text-transform: uppercase; font-weight: 400; margin-top: 23px;'>
                    Técnicas <br>Cocina Tailandesa 
                </div>
                
                <div style='min-height:250px;min-width:270px'>
                    <div class='txt-1' id='txt-slide-titulo' style='max-width:300px;'>
                        A LA CARTA
                    </div>
                    <div id='txt-slide-subTitulo mb-3'></div>
                    <div id='txt-slide-texto' class='mt-3' style='max-width:450px'>
                        Los platos tailandeses a la carta se pueden dividir en varias categorías; por ejemplo, arroz como Khao Kaphrao (arroz con albahaca), Khao Khluk Kapi (arroz mezclado con pasta de camarones), Khao Yam Pak Tai (arroz con ensalada picante), Khao Moo Daeng (arroz con cerdo rojo asado), Khao Mun Gai (arroz con pollo de Hainan), o platos de fideos como Pad Thai (fideos fritos tailandeses), Pad Mi Kati (fideos de arroz fritos con leche de coco), etc.
                    </div>
                </div>
                
                <div class='mt-3 d-flex mb-3'>
                    <button class='btn-prev'>
                        <svg class="bi" width="20" height="20" fill="currentColor">
                            <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-left"/>
                        </svg>
                    </button>
                    <button class='btn-next'>
                        <svg class="bi" width="20" height="20" fill="currentColor">
                            <use xlink:href="/public/library/bootstrap/bootstrap-icons/bootstrap-icons.svg#chevron-right"/>
                        </svg>
                    </button>
                </div>
            </div>
            <div class='c-swiper1 d-block'>
                <div class='slide-delete' style='position:absolute;width:155px;z-index:20;height:430px;background-color:var(--color2)'>
                    
                </div>
                <div class="swiper-container swiper1 headline-bottom ">
                    <div class="swiper-wrapper"  >
                        <!-- <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-1.jpg)"></div> -->
                        <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-2.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-3.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-4.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-5.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-6.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(/public/img/web/3/slide-7.jpg)"></div>
                    </div>
                    <!-- Add Pagination -->
                    <!-- <div class="swiper-pagination"></div> -->
                </div>
            </div>
        </section>
        <section id='frutas_tailandes' class='s4 c-img-2 d-flex justify-content-around frutas_tailandes' style='background-image:url(/public/img/web/3/img-2.png);overflow-y:hidden'>
            <div style='max-width:550px' class='headline-bottom w-100 c-temporadas'>
                <div style='font-size: 21px; text-transform: uppercase; font-weight: 400;'>Frutas tailandesas</div>
                <div class='mt-3 txt-texto' id='txt-texto' style='max-width:500px'>
                    Tailandia es conocida como un paraíso para los amantes de las frutas y también ha recibido reconocimiento por sus frutas de clima tropical y fresco. Las frutas tailandesas tienen un sabor agridulce que se puede comer durante todo el año debido a las condiciones fértiles del suelo. Las frutas populares incluyen longans, durian, lichi, mango, pomelo, piña, coco aromático y tamarindo. 
                    <br>
                    <br>
                    Hay numerosos huertos y plantaciones en las
provincias de Rayong, Chanthaburi y Trat que abren sus puertas para que los visitantes vean y degusten frutas que se presentan en estilo buffet.
                </div>
                <div class='mt-3 txt-1 font-weight-bold' id='txt-titulo'>
                    TEMPORADA <br> CÁLIDA
                </div>
                <div class='mt-4 mb-3 d-flex'>
                    <div class='text-right d-flex align-items-end pl-2' style='height:60px;width:100%;max-width:60px'>
                        <div class='w-100'>
                            <div class='num clickable num-activate' data-num='1'>01</div>
                            <hr>
                        </div>
                    </div>
                    <div class='text-right d-flex align-items-end pl-2' style='height:60px;width:100%;max-width:60px'>
                        <div class='w-100'>
                            <div class='num clickable' data-num='2'>02</div>
                            <hr>
                        </div>
                    </div>
                    <div class='text-right d-flex align-items-end pl-2' style='height:60px;width:100%;max-width:60px'>
                        <div class='w-100'>
                            <div class='num clickable' data-num='3'>03</div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <div class='w-100 headline-bottom c-lista-frutas' style='max-width:580px;min-height:450px'>
                <div id='list-frutas'>
                    <li>Mangostán</li>
                    <li>Plátano</li>
                    <li>Mango</li>
                    <li>Durián</li>
                    <li>Papaya</li>
                    <li>Lichi</li>
                    <li>Sandía</li>
                    <li>Pomarrosa</li>
                </div>
                <div class='mt-1 txt-texto' style='max-width:400px' id='txt-auxiliar'>
                    En la última parte de esta temporada, pruebe rambután, marang, salacca, yaca y plátano.
                </div>
            </div>
        </section>
        <section class='d-flex justify-content-around align-items-center s5 arroz_tailandes' id='arroz_tailandes' style='overflow-y:hidden'>
            <div style='max-width:530px' class='headline-bottom cs-5'>
                <div class='txt-1'>
                    ARROZ TAILANDÉS
                </div>
                <br>
                <div style='font-size:14px;line-height:17px' class='txt-arroz'>
                    El arroz es uno de los pilares de la dieta tailandesa al punto que el verbo tailandés “comer” o "kin khao" significa "comer arroz". Hay muchas variedades de arroz. El arroz de mejor calidad se conoce como khao hom mali (arroz aromático de jazmín). Tailandia es famosa por su distintivo arroz jazmín de grano largo, conocido por su color blanco perlado y su aroma dulce. Suele servirse al vapor y se considera el mejor arroz para acompañar los platos tailandeses.<br><br>Los habitantes de la región noreste prefieren el Khao Niew (arroz pegajoso o glutinoso) al estilo Lao. Khao Niew, el "arroz glutinoso" se come a mano cuando se sirve con platos de influencia nororiental, como el gai yang (pollo a la parrilla) y la ensalada de papaya picante (som tam). El arroz glutinoso es un ingrediente crucial en el postre tailandés favorito, el arroz glutinoso con mango.<br><br>Para el desayuno, el Khao Tom o "papilla de arroz" se sirve con aderezos como hierbas, ajo, encurtidos y maní. El Khao Pad o "arroz frito" se hace con cerdo o pollo frito, chiles y salsa de pescado, típicamente con sobras de arroz. 
                
                </div>
            </div>
            <div class='img-4 ml-2 mr-2 headline-bottom'>
                <img src="/public/img/web/3/img-3.png" height="100%" width='100%'>
            </div>
            <div class='txt-1 t-oc d-none'>
                ARROZ TAILANDÉS
            </div>
        </section>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>
    <script>
        let jsonSlide={
            0:{
                "titulo":`A LA CARTA`,
                "subTitulo":``,
                "texto":`Los platos tailandeses a la carta se pueden dividir en varias categorías; por ejemplo, arroz como Khao Kaphrao (arroz con albahaca), Khao Khluk Kapi (arroz mezclado con pasta de camarones), Khao Yam Pak Tai (arroz con ensalada picante), Khao Moo Daeng (arroz con cerdo rojo asado), Khao Mun Gai (arroz con pollo de Hainan), o platos de fideos como Pad Thai (fideos fritos tailandeses), Pad Mi Kati (fideos de arroz fritos con leche de coco), etc.`
            },
            1:{
                "titulo":`COCIDOS A FUEGO <br> LENTO Y PASTA DE CHILE`,
                "subTitulo":``,
                "texto":`Los platos de pasta de chile y hervidos a fuego lento se sirven con guarniciones de varios vegetales crudos, hervidos o fritos.`
            },
            2:{
                "titulo":`HERVIDO`,
                "subTitulo":``,
                "texto":`Los ingredientes se pueden hervir en leche de coco, leche o agua.`
            },
            3:{
                "titulo":`CURRY`,
                "subTitulo":``,
                "texto":`Los diferentes tipos de curry se pueden dividir en aquellos que no usan leche de coco y curry que sí lo hacen, así como los que se clasifican por su sabor; tales como el Gaeng Phed (curry picante) y Gaeng Som (curry amargo), o también pueden ser divididos por su color tales como el Gaeng Daeng (curry rojo) y Gaeng Keaw Waan (curry verde).`
            },
            4:{
                "titulo":`APERITIVOS`,
                "subTitulo":``,
                "texto":`Se trata de alimentos que se consumen entre las comidas principales o en cualquier momento. Incluye platos tradicionales y modernos contemporáneos; tales como, Khao Kriap Pak Mo (pasteles de arroz crujientes), Po Pia Thot (rollitos de primavera), Khao Tom Mat (arroz pegajoso envuelto en hoja de plátano), Sakhu Sai Mu (palma de sagú con cerdo), Khao Tang Na Tang (arroz con salsa picante), entre otros.`
            },
            5:{
                "titulo":`POSTRES`,
                "subTitulo":``,
                "texto":`Después de comer una comida tailandesa, no debe perderse el postre. Los postres tailandeses enfatizan la dulzura, y hay de muchos tipos. Los métodos principales son cocidos al vapor, revueltos y hervidos. Además, Tailandia es un país caluroso, por lo que los postres tailandeses a menudo se sirven fríos para refrescarte; por ejemplo, un postre de frutas como Loi Gaew (salacca en almíbar), Ruam Mitr (leche de coco fría con frutas variadas, frijoles, perlas de tapioca, raíz de loto) y Thap Tim Krop (castañas de agua en leche de coco) y Gaeng Buat (fruta en leche de coco).`
            },
        }
        let jsonTemporada={
            0:{
                "titulo":`TEMPORADA <br> CÁLIDA`,
                "texto":`Tailandia es conocida como un paraíso para los amantes de las frutas y también ha recibido reconocimiento por sus frutas de clima tropical y fresco. Las frutas tailandesas tienen un sabor agridulce que se puede comer durante todo el año debido a las condiciones fértiles del suelo. Las frutas populares incluyen longans, durian, lichi, mango, pomelo, piña, coco aromático y tamarindo.`,
                "lista":`<li>Mangostán</li>
                         <li>Plátano</li>
                         <li>Mango</li>
                         <li>Durián</li>
                         <li>Papaya</li>
                         <li>Lichi</li>
                         <li>Sandía</li>
                         <li>Pomarrosa</li>`,
                "auxiliar":`En la última parte de esta temporada, pruebe rambután, marang, salacca, yaca y plátano.`,
            },
            1:{
                "titulo":`TEMPORADA <br> DE LLUVIAS`,
                "texto":`Tailandia es conocida como un paraíso para los amantes de las frutas y también ha recibido reconocimiento por sus frutas de clima tropical y fresco. Las frutas tailandesas tienen un sabor agridulce que se puede comer durante todo el año debido a las condiciones fértiles del suelo. Las frutas populares incluyen longans, durian, lichi, mango, pomelo, piña, coco aromático y tamarindo. `,
                "lista":`<li>Rambután</li>
                         <li>Santol</li>
                         <li>Plátano</li>
                         <li>Salacca</li>
                         <li>Pomelo</li>
                         <li>Chirimoya</li>
                         <li>Longan</li>
                         <li>Langsat</li>`,
                "auxiliar":`Pero luego, deléitese con mangostán, mandarina tailandesa, guayaba, piña y carambola.`,
            },
            2:{
                "titulo":`TEMPORADA <br> FRÍA`,
                "texto":`Tailandia es conocida como un paraíso para los amantes de las frutas y también ha recibido reconocimiento por sus frutas de clima tropical y fresco. Las frutas tailandesas tienen un sabor agridulce que se puede comer durante todo el año debido a las condiciones fértiles del suelo. Las frutas populares incluyen longans, durian, lichi, mango, pomelo, piña, coco aromático y tamarindo. `,
                "lista":`<li>Plátano</li>
                         <li>Caña</li>
                         <li>Mandarina tailandesa</li>
                         <li>Papaya</li>
                         <li>Sandía</li>
                         <li>Guayaba</li>`,
                "auxiliar":`Pueden probar también azufaifo, pomarrosa, zapote.`,
            },
        }
        $(".num").on("click",function(){
            let num=$(this).data("num")
            $(".num").removeClass("num-activate")
            $(this).addClass("num-activate")
            $("#txt-titulo").hide().html(jsonTemporada[num-1]["titulo"]).fadeIn(500)
            $("#list-frutas").hide().html(jsonTemporada[num-1]["lista"]).fadeIn(500)
            $("#txt-auxiliar").hide().html(jsonTemporada[num-1]["auxiliar"]).fadeIn(500)
            // $("#txt-texto").hide().html(jsonTemporada[num-1]["texto"]).fadeIn(500)
        })
        var swiper = new Swiper('.swiper1', {
            effect: 'coverflow',
            grabCursor: true,
            // spaceBetween:2,
            // centradoSlidesBounds:true,
            simulateTouch:false,
            centeredSlides: true,
            slidesPerView: "auto",
            coverflowEffect: {
                rotate: 0,
                stretch: -100,
                depth: 300,
                modifier: 1,
                slideShadows: false,
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
        let numActual=0;
        $(".btn-next").on("click",function(){
            if(jsonSlide[numActual+1]){
                numActual++;
                swiper.slideNext();
                $("#txt-slide-titulo").hide().html(jsonSlide[numActual]["titulo"]).fadeIn(300)
                $("#txt-slide-subTitulo").hide().html(jsonSlide[numActual]["subTitulo"]).fadeIn(300)
                $("#txt-slide-texto").hide().html(jsonSlide[numActual]["texto"]).fadeIn(300)
            }
        })
        $(".btn-prev").on("click",function(){
            if(jsonSlide[numActual-1]){
                numActual--;
                swiper.slidePrev();
                $("#txt-slide-titulo").hide().html(jsonSlide[numActual]["titulo"]).fadeIn(300)
                $("#txt-slide-subTitulo").hide().html(jsonSlide[numActual]["subTitulo"]).fadeIn(300)
                $("#txt-slide-texto").hide().html(jsonSlide[numActual]["texto"]).fadeIn(300)
            }
        })
        $('[data-toggle="tooltip"]').tooltip({
            animated: 'fade',
            placement: 'top',
            html: true
        });
        $(".swiper1 *").on("touchstart mousedown", function(e) {
            // Prevent carousel swipe
            e.stopPropagation();
        })
    </script>
</body>
</html>