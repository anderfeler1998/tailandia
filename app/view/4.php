<?php
    require './app/controller/4.php';
?>
<!DOCTYPE html>
<html lang="en">
<head><meta charset="gb18030">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Receta</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
       echo VIDEOJS;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v5.6">
    <link rel="stylesheet" href="/public/assets/web/css/4.css?v5.8">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        <section class='s1' style='overflow-y:hidden'>
            <div class='w-100 d-flex'>
                <div class='c-img-1'>
                    <!-- <img src="/public/img/web/4/img-1.png" width='100%' height="100%"> -->
                    <?php
                        if(array_key_exists("error",$json)){
                            // echo "NO SE ENCONTRO LA RECETA";
                        }else{
                            if($json["tipFile"]==1){
                                ?>  
                                    <style>
                                        #fm-video{
                                            display:none;
                                        }
                                        #poster-video{
                                            object-fit: cover;
                                        }
                                        #frame-video{
                                            display:none;
                                        }
                                    </style>
                                    <video id='poster-video' src="/public/presentacion/<?php echo $json["filePresentacion"] ?>" autoplay='true' loop="true" muted='true' style='object-fit: cover;' width='100%' height='100%'></video>
                                    <!-- <video id='fm-video' class='video-js vjs-big-play-centered'>
                                        <source src="/public/video/web/<?php echo $json["vdoReceta"] ?>">
                                    </video> -->
                                    <iframe id='frame-video'  width="100%" height="100%" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <?php
                            }else{
                                ?>
                                    <video id='poster-video' poster="/public/presentacion/<?php echo $json["filePresentacion"] ?>" style='object-fit: cover;' width='100%' height='100%' id='fm-video' class='video-js vjs-big-play-centered'>
                                        <!-- <source src="/public/video/web/<?php echo $json["vdoReceta"] ?>"> -->
                                    </video>
                                    <!-- <img src="/public/presentacion/<?php echo $json["filePresentacion"] ?>"> -->
                                    <iframe id='frame-video'  width="100%" height="100%" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                <?php
                            }
                        }
                    ?>
                    <div class='h-100 align-items-center c-receta-info'>
                        <div class='c-titulo-main headline-bottom'>
                            <?php
                               if(array_key_exists("error",$json)){
                                    echo "NO SE ENCONTRO LA RECETA";
                               }else{
                            ?>
                                <div class='txt-1'><?php echo $json["preTitulo"];?></div>
                                <div class='txt-2'><?php echo $json["titulo"];?></div>
                                <div class='d-flex'>
                                    <div class='t-gion'>
                                        ______________ 
                                    </div>
                                    <div class='txt-3' id='tt-3' style='    margin: 8px;'><?php echo $json["subTitulo"];?></div>
                                </div>
                                <div class='mt-3'>
                                <button id='btn-rReceta' class='btn btn-ver-video d-flex align-items-center justify-content-around'>VER VIDEO 
                                    <img class='ml-1' src="/public/img/web/4/play.png" width="8px">
                                </button>
                            </div>
                            <?php
                               
                               }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            if(array_key_exists("error",$json)){
                // echo "NO SE ENCONTRO LA RECETA";
            }else{
        ?>
            <section class='s2'>
                <div class='c-img-2 p-5 d-flex justify-content-center align-items-start' style='background-image:url(/public/img/data/<?php echo $json["imgReceta"] ?>)'>
                    <div class='c-cuadro'>
                        <div class='titulo-receta headline-bottom'><?php echo $json["preTitulo"]." ".$json["titulo"];?></div>
                        <div class='subtitulo-receta headline-bottom'><?php echo $json["subTitulo"];?></div>
                    </div>
                </div>
                <div class='c-container'>
                    <div class='c-ingredientes'>
                        <div class='t-ingredientes headline'>Ingredientes</div>
                        <div class='c-columnas headline-bottom'>
                            <div class="l-izquierdo">
                                <?php echo nl2br($json["rightIngredientes"]) ;?>
                            </div>
                            <div class="l-derecho">
                                <?php echo nl2br($json["leftIngredientes"]) ;?>
                            </div>
                        </div>
                    </div>
                    <div class='c-preparacion headline-bottom'>
                        <div class='t-preparacion'>Preparación</div>
                        <br>
                        <div class='t-texto' style='font-size:14px;line-height:17px'>
                            <?php
                            $info=explode("\n\n",$json["preparacion"]);
                            //    var_dump($info);
                            foreach($info as $k => $v){
                                    echo "<text style='margin-left:-10px;font-size:11px'>+ </text>".$v."<br><br>";
                            }
                            ?>
                            <?php //echo nl2br($json["preparacion"]) ;?>
                        </div>
                    </div>
                </div>
            </section>
            <section style='padding:2rem 1rem' class='seccion4'>
            <div class='d-flex justify-content-between s4-comodin flex-wrap align-items-center'> 
                <div style='color:white;padding-left:6rem;padding-right:6rem' class='headline-bottom'>
                    Descubra la <br>
                    <h2>Cocina<br>Tailandesa</h2>
                    <div class="swiper-pagination-3"></div>
                </div>
                <div class='c-slide-three' style='overflow-y:hidden'>
                    <div class="swiper-container swiper3 headline-bottom">
                        <div class="swiper-wrapper">
                            <?php
                               if(array_key_exists("error",$jsonR)){
                            ?>

                            <?php
                               }else{
                                   foreach($jsonR as $key=>$value){
                            ?>
                                <div class="swiper-slide <?php echo (count($jsonR)==($key+1))?"mr-1":"" ?>" >
                                    <!-- Slide 1 -->
                                    <div class='s-receta noselect'  style='margin-left:11px;z-index:<?php echo count($jsonR)-intval($key);?>'>
                                        <div class='s-img-logo'>
                                            <img src="/public/img/web/LogoReceta.png">
                                        </div>
                                        <div class='s-img-receta'>
                                            <img src="/public/img/data/<?php echo $value["imgPlato"] ?>">
                                        </div>
                                        <!-- -------------------------------------- -->
                                        <div class='s-detalle'>
                                            <div class='w-100'></div>
                                            <div class='txt-detalle w-100'>
                                                <li>
                                                    <img src="/public/img/web/reloj.png">
                                                    <text><?php echo $value["duracion"] ?></text>
                                                </li>
                                                <li>
                                                    <img src="/public/img/web/persona.png">
                                                    <text><?php echo ($value["personas"])?($value["personas"]." "."persona(s)"):"" ?></text>
                                                </li>
                                            </div>
                                            <div class='w-100'>
                                                <div class='mt-3 w-100 txt-nameReceta'>
                                                    <?php
                                                       echo $value["preTitulo"]." ".$value["titulo"]
                                                    ?>
                                                </div>
                                                <div class='mb-4 w-100 txt-desReceta'>
                                                    <?php
                                                       echo ($value["subTitulo"])?$value["subTitulo"]:"";
                                                    ?>
                                                </div>
                                                <?php
                                                   $parametroAdorno=str_replace(" ","_",$value["preTitulo"]." ".$value["titulo"]);
                                                ?>
                                                <a class='btn btn-verRecta' href='/receta/<?php echo $value["id"]."?".$parametroAdorno?>' >VER RECETA</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                   }
                               }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            
            }
        ?>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>
    <?php
        if(array_key_exists("error",$json)){
            // echo "NO SE ENCONTRO LA RECETA";
        }else{
            if($json["tipFile"]==1){
                ?>  
                    <script>
                        let videoLink="<?php echo $json["vdoReceta"];?>";
                        $("#btn-rReceta").on("click",function(){
                            $(".c-receta-info").fadeOut(300)
                            $(".c-buscador").fadeOut(300)
                            $("#poster-video").fadeOut(300)
                            setTimeout(function(){
                                $(".c-titulo-main").addClass("d-none")
                            },200)
                            setTimeout(() => {
                                console.log(videoLink)
                                $("#frame-video").fadeIn(500);
                                // $("#frame-video").attr("src","https://www.youtube.com/embed/3ZANAcZtrs0?autoplay=1&rel=0")
                                $("#frame-video").attr("src",videoLink+"?autoplay=1&rel=0")
                            }, 500);
                        })
                    </script>
                <?php
            }else{
                ?>
                   <script>
                       let html=`<video poster="/public/presentacion/<?php echo $json["filePresentacion"] ?>" id='fm-video' class='video-js vjs-big-play-centered'>
                                        <source src="/public/video/web/<?php echo $json["vdoReceta"] ?>">
                                    </video>`
                        let videoLink="<?php echo $json["vdoReceta"];?>";
                       $("#btn-rReceta").on("click",function(){
                            $(".c-receta-info").fadeOut(300)
                            $(".c-buscador").fadeOut(300)
                            $("#poster-video").fadeOut(300)
                            setTimeout(function(){
                                $(".c-titulo-main").addClass("d-none")
                            },200)
                            $("#poster-video").fadeOut(300)
                            setTimeout(function(){
                                $(".c-titulo-main").addClass("d-none")
                            },200)
                            setTimeout(() => {
                                console.log(videoLink)
                                $("#frame-video").fadeIn(500);
                                // $("#frame-video").attr("src","https://www.youtube.com/embed/3ZANAcZtrs0?autoplay=1&rel=0")
                                $("#frame-video").attr("src",videoLink+"?autoplay=1&rel=0")
                            }, 500);
                        })
                   </script>
                <?php
            }
        }
    ?>

    <script>
        var s3 = new Swiper('.swiper3', {
            type: 'bullets',
            slidesPerView: 'auto',
            // slidesPerView: 4,
            <?php
                if(!array_key_exists("error",$json)){
                    if(count($jsonR)>3){
                        // echo "loop: true,";
                    }
                }
            ?>
            pagination: {
                el: '.swiper-pagination-3',
                clickable: true,
            },
        });
        setTimeout(function(){
            s3.update(true);
            s3.slideTo(0, 0)
        }, 1);
        $(".s-receta").on("mouseenter",function(){
            $(".c-slide-three .swiper-slide").removeClass("swiper-slide-active")
            $(this).parent().addClass("swiper-slide-active")
        })
    </script>
</body>
</html>