<?php
    require './app/controller/7.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foodlandia - Noticia</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo SCROLLREVEAL;
    ?>
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <link rel="stylesheet" href="/public/assets/web/css/general.css?v3.0">
    <link rel="stylesheet" href="/public/assets/web/css/7.css?v4.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>


</head>
<body style='overflow-x:hidden'>
    <main>
        <?php
            require './app/view/buscador.html';
            require './app/view/menu.php';
        ?>
        
            <?php
                  if(array_key_exists("error",$json)){
            ?>
                No se encontro la noticia
            <?php
                }else{
            ?>
        <section class='s1'>
            <div class='w-100 d-flex'>
                <div class='c-img-1' style='background-image:url(/public/img/data/<?php echo $json["img1"]; ?>)'>
                    <div class='h-100 d-flex align-items-center position-relative' style='margin-left:5%'>
                        <!-- <div class='c-titulo-main headline-bottom'>
                                <div class='txt-1'></div>
                                    <div class='txt-2'>NOTICIAS</div>
                                </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>
        <section class='p-5 s2'>
                <div>
                    <div class='d-flex justify-content-around c-content headline-bottom'>
                            <div class='comodin'>
                                <div class='d-flex  flex-wrap p-1 mb-5 comodin'>
                                <div>Noticias</div>
                                <div>
                                    <img src="/public/img/web/7/poligon.png" class="ml-2 mr-2">
                                </div>
                                <div>Gastronomia</div>
                                <div>
                                    <img src="/public/img/web/7/poligon.png" class="ml-2 mr-2">
                                </div>
                                <div style='text-transform:capitalize'><?php echo strtolower($json["nombre"]); ?></div>
                            </div>
                            <div class='txt-1'><?php echo $json["nombre"]; ?></div>
                            <div class='txt-3' id='fecha'>19 de Marzo 2020</div>
                            <div class='txt-3 mt-5' id='detalles'>
                                <?php echo nl2br($json["detalles"]); ?>
                            </div>
                        </div>
                        <div class='ml-2 mr-2 container-imagen headline-bottom-fixed' style=''>
                            <img src="/public/img/data/<?php echo $json["img2"]; ?>" class='img-cover' height='100%' width='100%'>
                        </div>
                    </div>
                </div>
        </section>
                <script>
                    let fec="<?php echo $json["fecIngreso"] ?>";
                    moment.locale("es");
                    $("#fecha").html(moment(fec).format('D [de] MMMM [del] YYYY'))
                </script>
            <?php
                }
            ?>
        <?php
           require_once './app/view/footer.html';
        ?>
    </main>
</body>
</html>