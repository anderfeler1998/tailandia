
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        if(isset($_SESSION["usuario"])){
            echo '<meta http-equiv="refresh" content="0;url=/admin/gestion">';
            die();
        }
    ?>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./public/assets/web/css/general.css?v3.2">
    <?php
       echo BOOTSTRAP;
       echo ALERTIFYJS;
    ?>
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body style='background-color:var(--color1)'>
    <div class='d-flex justify-content-center align-items-center' style='height:100vh;width:100vw'>
        <div style='max-width:450px' class='p-3 w-100'>
            <div class='w-100 d-flex justify-content-center mb-3'>
                <img src='/public/img/web/Logo.svg' style='width:100px;height:100px'>
            </div>
            <h2 class='text-center'>INGRESO ADMINISTRATIVO</h2>
            <div class='form-row'>
                <div class="form-group col-12 col-sm-12">
                    <label for="">Usuario:</label>
                    <input type="text" autocomplete="off" class="form-control" id="txtUsuario" placeholder="Ingresar Usuario">
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <div class="form-group col-12 col-sm-12 mb-4">
                    <label for="txtUrl">Contraseña :</label>
                    <input type="password" autocomplete="off" class="form-control" id="txtPass" placeholder="Ingresar Contraseña">
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <button class="btn btn-warning m-auto" id='btn-logear'> Ingresar</button>
            </div>
        </div>
    </div>
</body>
    <script>
        $(document).ready(function(){
            let txtUsuario=$("#txtUsuario")
            let txtPass=$("#txtPass")
            function logear(){
                $("input").removeClass("is-invalid")
                switch (false) {
                    case ((txtUsuario.val())?true:false):
                        txtUsuario.addClass("is-invalid").focus()
                        alertify.error("Debe ingresar el usuario")
                    break;
                    case ((txtPass.val())?true:false):
                        txtPass.addClass("is-invalid").focus()
                        alertify.error("Debe ingresar la contraseña")
                    break;
                    default:
                        $.ajax({
                            url:"/app/ajax/login.php",
                            method:"POST",
                            data:{usuario:txtUsuario.val(),pass:txtPass.val()},
                            // dataType:"JSON",
                        }).done(function(res){
                            if(res==1){
                                alertify.success("Bienvenido");
                                setTimeout(() => {
                                    location.reload();
                                }, 500);
                            }else{
                                alertify.error("Datos no validos, intente de nuevo")
                            }
                        })
                    break;
                }
            }
            $("#btn-logear").click(function(){
                logear()
            })
            txtPass.keypress(function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code==13){
                    logear()
                }
            });
        })
    </script>
</html>