<?php
    require './app/controller/admin_recetas.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="/public/img/web/favico.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrar</title>
    <?php
       echo BOOTSTRAP;
       echo SWIPER;
       echo PAGINATIONJS;
       echo ALERTIFYJS;
    ?>
    <link rel="stylesheet" href="\public\assets\admin\css\general.css?v3.0">
    <link rel="stylesheet" href="\public\assets\admin\css\recetas.css?v5.5">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
</head>
<body style='background-color:#571845;'>
<style>
    .ucontainer{
        background-color:#fff;
    }
</style>
    <div class='capa-oscura'>
        <div class='d-flex justify-content-center align-items-center w-100 h-100'>
            <div class="spinner-grow m-1" style='' role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-grow m-1" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-grow m-1" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <main class='w-100'>
        <div class="w-100 d-flex justify-content-between align-items-center p-3" style='background-color:#571845;color:#fff;font-size:30px'>
            <div></div>
            <div><img src='/public/img/web/Logo.svg' style='width:80px;height:60px'></div>
            <div style='float:right'>
                <div class="dropdown">
                    <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-wrench" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M.102 2.223A3.004 3.004 0 0 0 3.78 5.897l6.341 6.252A3.003 3.003 0 0 0 13 16a3 3 0 1 0-.851-5.878L5.897 3.781A3.004 3.004 0 0 0 2.223.1l2.141 2.142L4 4l-1.757.364L.102 2.223zm13.37 9.019L13 11l-.471.242-.529.026-.287.445-.445.287-.026.529L11 13l.242.471.026.529.445.287.287.445.529.026L13 15l.471-.242.529-.026.287-.445.445-.287.026-.529L15 13l-.242-.471-.026-.529-.445-.287-.287-.445-.529-.026z"/>
                        </svg>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" id='opt-cambiar'  type="button">Cambiar Contraseña</button>
                        <button class="dropdown-item" id='opt-cerrar' type="button">Cerrar Sesión</button>
                    </div>
                </div>
            </div>
        </div>
        <ul class='ulMenu'>
            <li>
                <a class='clickable a-option' data-id='1'>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                    </svg>
                    RECETAS
                </a>
                <ul id='u1' class='ucontainer'>
                    <div class='info-nav'>
                        <div class='p-3'>
                            <div class='d-flex justify-content-end'>
                                <button class='btn btn-primary' id='btnNuevaReceta'>
                                    <svg style='font-size:18px;margin-right:5px;margin-top:-2px' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                        <path fill-rule="evenodd" d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
                                    </svg>

                                    Añadir Receta
                                </button>
                            </div>
                            <div class='d-flex justify-content-center w-100'>
                                <div id='pag1'></div>
                            </div>
                            <div class='table-responsive'>
                                <table class='table table-bordered table-striped table-hover' id='tRecetas'>
                                    <thead>
                                        <tr>
                                            <th style='width:50px;'>N°</th>
                                            <th style='width:180px;'>Opciones</th>
                                            <th>Titulo Receta</th>
                                            <th>Subtitulo</th>
                                            <th>Detalles</th>
                                            <!-- <th>Vizualizar</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class='text-center' colspan='100%'>Sin registros</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </ul>
            </li>
            <li>
                <a class='clickable a-option' data-id='2'>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                    </svg>
                    ICÓNICOS
                </a>
                <ul id='u2' class='ucontainer'>
                    <div class='info-nav'>
                        <div class='p-3'>
                            <div class='d-flex justify-content-end'>
                                <button id='btnNuevoIconico' class='btn btn-primary'>
                                    <svg style='font-size:18px;margin-right:5px;margin-top:-2px' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                        <path fill-rule="evenodd" d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
                                    </svg>

                                    Añadir Icónico
                                </button>
                            </div>
                            <div class='d-flex justify-content-center w-100'>
                                <div id='pag2'></div>
                            </div>
                            <div class='table-responsive'>
                                <table class='table table-bordered table-striped table-hover' id='tIconicos'>
                                    <thead>
                                        <tr>
                                            <th style='width:50px;'>N°</th>
                                            <th style='width:180px;'>Opciones</th>
                                            <th>Titulo Iconico</th>
                                            <th>Detalles</th>
                                            <!-- <th>Vizualizar</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class='text-center' colspan='100%'>Sin registros</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </ul>
            </li>
            <li>
                <a class='clickable a-option' data-id='3'>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                    </svg>
                    NOTICIAS
                </a>
                <ul id='u3' class='ucontainer'>
                    <div class='info-nav'>
                        <div class='p-3'>
                            <div class='d-flex justify-content-end'>
                                <button id='btnNuevaNoticia' class='btn btn-primary'>
                                    <svg style='font-size:18px;margin-right:5px;margin-top:-2px' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                        <path fill-rule="evenodd" d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
                                    </svg>

                                    Añadir Noticias
                                </button>
                            </div>
                            <div class='d-flex justify-content-center w-100'>
                                <div id='pag3'></div>
                            </div>
                            <div class='table-responsive'>
                                <table class='table table-bordered table-striped table-hover' id='tNoticias'>
                                    <thead>
                                        <tr>
                                            <th style='width:50px;'>N°</th>
                                            <th style='width:180px;'>Opciones</th>
                                            <th>Titulo Noticia</th>
                                            <th>Detalles</th>
                                            <th>Fecha Noticia</th>
                                            <!-- <th>Vizualizar</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class='text-center' colspan='100%'>Sin registros</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </ul>
            </li>
        </ul>
    </main>
    <div id='modalEditar' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog pr-sm-2 pl-sm-2" style='max-width:1000px'>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel"><text id='mdt'>EDITAR</text> RECETA</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class='modal-body'>
                    <div class='container-fluid p-0'>
                        <form>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                    <div class="form-group col-12 col-sm-6">
                                        <label for="txtTitulo1">1° Título :</label>
                                        <input type="text" autocomplete="off" class="form-control" id="txtTitulo1" placeholder="Ingresar 1° título">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group col-12 col-sm-6">
                                        <label for="txtTitulo2">2° Título :</label>
                                        <input type="text" autocomplete="off" class="form-control" id="txtTitulo2" placeholder="Ingresar 2° título">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group col-12 col-sm-6">
                                        <label for="txtSubTitulo">Subtítulo :</label>
                                        <input type="text" autocomplete="off" class="form-control" id="txtSubTitulo" placeholder="Ingresar subtítulo">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <label for="txtDuracion">Duración :</label>
                                        <input type="text" maxlength='10' autocomplete="off" class="form-control" id="txtDuracion" placeholder="Ingresar duración">
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group col-6 col-sm-3">
                                        <label for="exampleInputEmail1">Personas :</label>
                                        <div>
                                            <select id="cboPersonas" class='form-control'>
                                                <option selected disabled>Seleccione</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                            </div>
                            <div class='form-row'>
                                <div class="form-group col-12 col-sm-6 d-flex align-items-center flex-wrap">
                                    <div class='w-100'>
                                        <label for="exampleInputEmail1">Imagen Plato :</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="fileImgPlato" accept="image/png, image/x-png, image/jpg, image/jpeg" required>
                                            <label class="custom-file-label" for="fileImgPlato">Seleccionar</label>
                                            <!-- <div class="invalid-feedback">Example invalid custom file feedback</div> -->
                                        </div>
                                    </div>
                                    <div class='w-100 d-flex justify-content-center mt-3'>
                                        <div class='container-image1 rounded'>

                                        </div>
                                    </div>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-6 d-flex align-items-center flex-wrap">
                                    <div class='w-100'>
                                        <label for="exampleInputEmail1">Imagen Receta :</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="fileImgReceta" accept="image/png, image/x-png, image/jpg, image/jpeg" required>
                                            <label class="custom-file-label" for="fileImgReceta">Seleccionar</label>
                                            <!-- <div class="invalid-feedback">Example invalid custom file feedback</div> -->
                                        </div>
                                    </div>
                                    <div class='w-100 d-flex justify-content-center mt-3'>
                                        <div class='container-image2 rounded'>

                                        </div>
                                    </div>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class='form-row'>
                                <div class="form-group col-12 col-sm-6">
                                    <label for="">Video o imagen inicial :</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="filePresentacion" accept="image/png, image/x-png, image/jpg, image/jpeg, video/mp4" required>
                                        <label class="custom-file-label" for="filePresentacion">Seleccionar</label>
                                        <!-- <div class="invalid-feedback">Example invalid custom file feedback</div> -->
                                    </div>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-6">
                                    <label for="txtUrl">URL del video :</label>
                                    <input type="text" autocomplete="off" class="form-control" id="txtUrl" placeholder="Ingresar duración">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class='form-row'>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="">Ingredientes 1ra Columna :</label>
                                    <textarea class='form-control' id="txtIngredientes1" cols="10" rows="10" placeholder='Ingresar texto'></textarea>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="">Ingredientes 2da Columna :</label>
                                    <textarea class='form-control' id="txtIngredientes2" cols="10" rows="10" placeholder='Ingresar texto'></textarea>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="">Preparación :</label>
                                    <textarea class='form-control' id="txtPreparacion" cols="10" rows="10" placeholder='Ingresar texto'></textarea>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class='c-btn c-btn-registrar'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                        <button type="button" class="btn btn-primary" id='btn-receta-registrar'>REGISTRAR</button>
                    </div>

                    <div class='c-btn c-btn-actualizar'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                        <button type="button" class="btn btn-primary" id='btn-receta-actualizar'>ACTUALIZAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id='modalEditarIco' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog pr-sm-2 pl-sm-2" style='max-width:700px'>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel"><text id='mdtIco'>EDITAR</text> PLATO ICÓNICO</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class='modal-body'>
                    <div class='container-fluid p-0'>
                        <form>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="exampleInputEmail1">Título :</label>
                                    <input type="text" autocomplete="off" class="form-control" id="txtTituloIco" placeholder="Ingresar 1° título">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                
                            </div>
                            <div class='form-row'>
                                <div class="form-group col-12 col-sm-6">
                                    <label for="">Descripción :</label>
                                    <textarea class='form-control' id="txtDescripcionIco" cols="12" rows="10" placeholder='Ingresar texto'></textarea>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-6 d-flex align-items-center flex-wrap">
                                    <div class='w-100'>
                                        <label for="exampleInputEmail1">Imagen Plato :</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="txtFileIco" accept="image/png, image/x-png, image/jpg, image/jpeg" required>
                                            <label class="custom-file-label" for="txtFileIco">Seleccionar</label>
                                            <!-- <div class="invalid-feedback">Example invalid custom file feedback</div> -->
                                        </div>
                                    </div>
                                    <div class='w-100 d-flex justify-content-center mt-3'>
                                        <div class='container-image3 rounded'>

                                        </div>
                                    </div>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                <div class='c-btn c-btn-registrar'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                        <button type="button" class="btn btn-primary" id='btn-iconico-registrar'>REGISTRAR</button>
                    </div>

                    <div class='c-btn c-btn-actualizar'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                        <button type="button" class="btn btn-primary" id='btn-iconico-actualizar'>ACTUALIZAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id='modalEditarNoti' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog pr-sm-2 pl-sm-2" style='max-width:700px'>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel"><text id='mdtNoti'>EDITAR</text> NOTICIA</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class='modal-body'>
                    <div class='container-fluid p-0'>
                        <form>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="exampleInputEmail1">Título :</label>
                                    <input type="text" autocomplete="off" class="form-control" id="txtTituloNoti" placeholder="Ingresar 1° título">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-6">
                                    <label for="txtSubtituloNoti">Subtítulo :</label>
                                    <input type="text" autocomplete="off" class="form-control" id="txtSubtituloNoti" placeholder="Ingresar subtitulo">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-6">
                                    <label for="txtFecha">Fecha :</label>
                                    <input type="date" autocomplete="off" class="form-control" id="txtFechaNoti" placeholder="Ingresar fecha">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class='form-row'>
                                <div class="form-group col-12 col-sm-12">
                                    <label for="">Descripción :</label>
                                    <textarea class='form-control' id="txtDescripcionNoti" cols="12" rows="10" placeholder='Ingresar texto'></textarea>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group col-12 col-sm-6 d-flex align-items-center flex-wrap">
                                    <div class='w-100'>
                                        <label for="exampleInputEmail1">Imagen Presentacion:</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="txtFileNoticiaPresentacion" accept="image/png, image/x-png, image/jpg, image/jpeg" required>
                                            <label class="custom-file-label" for="txtFileNoticiaPresentacion">Seleccionar</label>
                                            <!-- <div class="invalid-feedback">Example invalid custom file feedback</div> -->
                                        </div>
                                    </div>
                                    <div class='w-100 d-flex justify-content-center mt-3'>
                                        <div class='container-image4 rounded'>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-sm-6 d-flex align-items-center flex-wrap">
                                    <div class='w-100'>
                                        <label for="exampleInputEmail1">Imagen Noticia:</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="txtFileNoticia" accept="image/png, image/x-png, image/jpg, image/jpeg" required>
                                            <label class="custom-file-label" for="txtFileNoticia">Seleccionar</label>
                                            <!-- <div class="invalid-feedback">Example invalid custom file feedback</div> -->
                                        </div>
                                    </div>
                                    <div class='w-100 d-flex justify-content-center mt-3'>
                                        <div class='container-image5 rounded'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                <div class='c-btn c-btn-registrar'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                        <button type="button" class="btn btn-primary" id='btn-noticia-registrar'>REGISTRAR</button>
                    </div>

                    <div class='c-btn c-btn-actualizar'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                        <button type="button" class="btn btn-primary" id='btn-noticia-actualizar'>ACTUALIZAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id='modalVerContacto' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog pr-sm-2 pl-sm-2" style='max-width:500px'>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">VER CONTACTO</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class='modal-body'>
                    <div class='container-fluid p-0'>
                        <form>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtFechaCon">Fecha :</label>
                                    <input type="text" autocomplete="off" disabled class="form-control" id="txtFechaCon" placeholder="Nombres">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtNombresCon">Nombres :</label>
                                    <input type="text" autocomplete="off" disabled class="form-control" id="txtNombresCon" placeholder="Nombres">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtApellidosCon">Apellidos:</label>
                                    <input type="text" autocomplete="off" disabled class="form-control" id="txtApellidosCon" placeholder="Apellidos">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                
                            </div>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtCorreoCon">Correo :</label>
                                    <input type="text" autocomplete="off" disabled class="form-control" id="txtCorreoCon" placeholder="Confirmar contraseña">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtDescripcionCon">Descripcion :</label>
                                    <textarea cols="10" class='form-control' disabled rows="10" id='txtDescripcionCon'></textarea>
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                </div>
            </div>
        </div>
    </div>
    <div id='modalCambiarContraseña' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog pr-sm-2 pl-sm-2" style='max-width:500px'>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">CAMBIO DE CONTRASEÑA</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class='modal-body'>
                    <div class='container-fluid p-0'>
                        <form>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtContraseñaActual">Contraseña Actual :</label>
                                    <input type="password" autocomplete="off" class="form-control" id="txtContraseñaActual" placeholder="Contraseña actual">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                
                            </div>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtNuevaContraseña">Nueva Contraseña:</label>
                                    <input type="password" autocomplete="off" class="form-control" id="txtNuevaContraseña" placeholder="Nueva contraseña">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                
                            </div>
                            <div class='form-row'>
                                <!-- <div class='col-12 col-sm-6'> -->
                                <div class="form-group col-12 col-sm-12">
                                    <label for="txtConfirmarNuevaContraseña">Confirmar nueva contraseña :</label>
                                    <input type="password" autocomplete="off" class="form-control" id="txtConfirmarNuevaContraseña" placeholder="Confirmar contraseña">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                    <button type="button" class="btn btn-primary" id='btnCambiar'>CAMBIAR CONTRASEÑA</button>
                </div>
            </div>
        </div>
    </div>
</body>
    <script src="\public\assets\admin\js\general.js"></script>
    <script>
        // const cantidad='<?php# echo $json["cantidad"]?>';
        let idHistory=1
        $(".a-option").click(function(){
            let id=$(this).data("id")
            // alert(id)
            if(id!=idHistory){
                $(".ucontainer").slideUp("slow")
            }
            idHistory=id
            $("#u"+id).slideToggle("slow")
        })

        $("#opt-cerrar").on("click",function(){
            alertify.confirm("Cerrar Sessión","Esta a punto de cerrar sesión<br> ¿Desea continuar?",
            function(){
                $.ajax({
                    url:"/app/ajax/logout.php",
                    method:"POST",
                    // dataType:"JSON",
                }).done(function(res){
                    if(res==1){
                        alertify.success("Cerrando...");
                        setTimeout(() => {
                            location.reload();
                        }, 500);
                    }else{
                        // alertify.error("Datos no validos, intente de nuevo")
                    }
                })
            },
            function(){

            })
        })
        $("#opt-cambiar").on("click",function(){
            $("input").removeClass("is-invalid")
            $("input").val("");
            $("#modalCambiarContraseña").modal("show")
        })
        $("#btnCambiar").on("click",function(){
            $("input").removeClass("is-invalid")
            let txtContraseñaActual=$("#txtContraseñaActual")
            let txtNuevaContraseña=$("#txtNuevaContraseña")
            let txtConfirmarNuevaContraseña=$("#txtConfirmarNuevaContraseña")
            switch(false){
                case (txtContraseñaActual.val())?true:false:
                    txtContraseñaActual.addClass("is-invalid").focus()
                break;
                case (txtNuevaContraseña.val())?true:false:
                    txtNuevaContraseña.addClass("is-invalid").focus()
                break;
                case (txtConfirmarNuevaContraseña.val())?true:false:
                    txtConfirmarNuevaContraseña.addClass("is-invalid").focus()
                break;
                case (txtNuevaContraseña.val()==txtConfirmarNuevaContraseña.val())?true:false:
                    txtNuevaContraseña.addClass("is-invalid").focus()
                    txtConfirmarNuevaContraseña.addClass("is-invalid").focus()
                    alertify.error("No coinciden");
                break;
                default:
                    alertify.confirm("Cambio de contraseña","Esta a punto cambiar la contraseña<br> ¿Desea continuar?",
                    function(){
                        showLoading();
                        $.ajax({
                            url:"/app/ajax/cambiarContraseña.php",
                            method:"POST",
                            dataType:"JSON",
                            data:{contraseña:txtContraseñaActual.val(),nuevaContraseña:txtNuevaContraseña.val()}
                        }).done(function(res){
                            if(!res.error){
                                $("#modalCambiarContraseña").modal("hide")
                                alertify.success("Contraseña actualizada")
                            }else{
                                alertify.error("No se pudo modificar la contraseña")
                            }
                            hideLoading();
                        })
                    },
                    function(){

                    }).set('movable', false); 
                break;
            }
        })
    </script>
    <script src="\public\assets\admin\js\recetas.js?v3.0"></script>
</html>