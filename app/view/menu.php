<div class='h-100 c-cintaMenu d-flex justify-content-center flex-wrap align-content-between'>
    <a href="/">
        <div class='mt-3 p-2 clickable bt-menu' >
            <img src='/public/img/web/Logo.svg' class="i-logo" height="35">
        </div>
    </a>
    <div class='p-3 mt-3 clickable bt-menu' id='btn-menu'>
        <img src='/public/img/web/Btons.svg' class="i-menu" height="25">
    </div>
    <div class='mb-3'>
        <div class='p-2 mt-1 clickable bt-menu'>
            <a href=" https://www.instagram.com/thaiemb.lima" target="_blank"">
                <img src='/public/img/web/Ig.svg' class="i-social">
            </a>
        </div>
        <div class='p-2 mt-1 clickable bt-menu'>
            <a href="https://www.youtube.com/channel/UCksuduBZ9yn3syb9b7LP_Rw" target="_blank">
                <img src='/public/img/web/Youtube.svg' class="i-social">
            </a>
        </div>
        <div class='p-2 mt-1 clickable bt-menu'>
             <a href="https://www.facebook.com/thaiembassylima" target="_blank">
                <img src='/public/img/web/Facebook.svg' class="i-social">
            </a>
        </div>
        <div class='p-2 mt-1 clickable bt-menu'>
             <a href="https://twitter.com/ThaiEmbLima" target="_blank">
                <img src='/public/img/web/twitter.svg' class="i-social">
            </a>
        </div>
    </div>
</div>
<div class='menu-pc'>
    <div class='c-menu-pc w-100 h-100 d-flex justify-content-between align-items-center'>
        <div></div>
        <div class='c-info-menu pt-1 pb-3'>
            <div class='col-12 mb-3 c-menu-b'>
                <div class='col-12 col-sm-6 col-md-5 d-flex align-items-center p-2 c-menu-buscador'>
                    <img src="/public/img/web/lupa1.png" width="20px" height='16px' class='pl-1 pr-1 clickable'>
                    <input type="text" class='w-100 input-menu-buscador' placeholder='Buscar' id='txt-buscar-menu'>
                </div>
            </div>
            <div class="col-12 pt-1 c-info-scroll">
                <div class='w-100 font-weight-bold' style='font-size:30px'>MENÚ</div>
                <div class='d-flex flex-wrap justify-content-center justify-content-md-start'>
                    <div class='pt-2 d-flex flex-wrap justify-content-around cm-1'>
                        <div class='menu-image menu-hv-image' style='background-image:url(/public/img/web/2/img-1.jpg);position: relative;'>
                            <div class='p-3'><a href='/que_es_la_cocina_tailandesa'>¿Qué es la Cocina Tailandesa?</a></div>
                        </div>
                        <div class='menu-image menu-hv-image' style='background-image:url(/public/img/web/3/img-1.jpg);position: relative;'>
                            <div class='p-3'><a href='/el_arte_de_la_cocina_tailandesa'>El Arte de la Cocina Tailandesa</a></div>
                        </div>
                        <div class='menu-image menu-v-image' style='background-image:url(/public/img/web/menu/img-3.png);position: relative;'>
                            <div class='p-3'><a href='/#descubre_la_cocina_tailandesa' onclick='$("#btn-menu").trigger("click")'>Descubra la Cocina Tailandesa</a></div>
                        </div>
                    </div>
                    <div class='pt-2 d-flex flex-wrap justify-content-center'>
                        <div class='menu-image menu-h-image' style='background-image:url(/public/img/web/menu/img-4.png);position: relative;'>
                            <div class='p-3'><a href='/turismo_gastronomico_en_tailandia'>Turismo Gastronómico en Tailandia</a></div>
                        </div>
                    </div>
                    <div class='pt-2 d-flex flex-wrap justify-content-around cm-2'>
                        <div class='menu-image menu-hv-image' style='background-image:url(/public/img/web/menu/img-5.png);position: relative;'>
                            <div class='p-3'><a href='/noticias'>Noticias</a></div>
                        </div>
                        <div class='menu-image menu-hv-image' style='background-image:url(/public/img/web/menu/img-6.png);position: relative;'>
                            <div class='p-3'><a href='/contactanos'>Contáctanos</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class= 'c-btn-menu-close d-flex align-items-center h-100 clickable' id='btn-menu-close'>
            <div class='btn-menu-close'>
                
            </div>
        </div>
    </div>
</div>
<script>
    $("#btn-menu").on("click",function(){
        if($(this).hasClass("btn-menu-active")){
            $(this).removeClass("btn-menu-active")
            $(".menu-pc").removeClass("menu-pc-active")
        }else{
            $(this).addClass("btn-menu-active")
            $(".menu-pc").addClass("menu-pc-active")
        }
    })
    $("#btn-menu-close").on("click",function(){
        $("#btn-menu").removeClass("btn-menu-active")
        $(".menu-pc").removeClass("menu-pc-active")
    })
    $("#txt-buscar-menu").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            location.href='/buscador/?buscar='+$(this).val()
        }
    });
</script>