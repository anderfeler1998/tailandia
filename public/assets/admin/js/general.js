function showLoading(){
    $(".capa-oscura").fadeIn("fast")
}

function hideLoading(){
    $(".capa-oscura").fadeOut("fast")
}
var changeImgPlato=false;
var changeImgReceta=false;
var changeFilePresentacion=false;
var changeImgIconico=false;
var changeImgNoticia1=false;
var changeImgNoticia2=false;
$("#modalEditar #fileImgPlato").change(function () {
    let imagen=readImage(this)
    let name=readFakePath(this)
    $("label[for=fileImgPlato]").html(name)
    $(".container-image1").html(imagen)
    changeImgPlato=true;
});
$("#modalEditar #fileImgReceta").change(function () {
    let imagen=readImage(this)
    let name=readFakePath(this)
    $("label[for=fileImgReceta]").html(name)
    $(".container-image2").html(imagen)
    changeImgReceta=true
});
$("#modalEditar #filePresentacion").change(function () {
    let imagen=readImage(this)
    let name=readFakePath(this)
    $("label[for=filePresentacion]").html(name)
    // $(".container-image2").html(imagen)
    changeFilePresentacion=true
});

$("#modalEditarIco #txtFileIco").change(function () {
    let imagen=readImage(this)
    let name=readFakePath(this)
    $("label[for=txtFileIco]").html(name)
    $(".container-image3").html(imagen)
    changeImgIconico=true;
});

$("#modalEditarNoti #txtFileNoticiaPresentacion").change(function () {
    let imagen=readImage(this)
    let name=readFakePath(this)
    $("label[for=txtFileNoticiaPresentacion]").html(name)
    $(".container-image4").html(imagen)
    changeImgIconico=true;
});

$("#modalEditarNoti #txtFileNoticia").change(function () {
    let imagen=readImage(this)
    let name=readFakePath(this)
    $("label[for=txtFileNoticia]").html(name)
    $(".container-image5").html(imagen)
    changeImgIconico=true;
});

function readImage(input) {
    let img=$("<img>");
    // console.log(input)
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            img.attr({'src':e.target.result,style:"background:#fff"}); // Renderizamos la imagen
        }
        reader.readAsDataURL(input.files[0]);
    }
    // alert(1)
    return img;
}
function readFakePath(input) {
    let path="Seleccionar"
    if (input.files && input.files[0]) {
        path="C:\\fackepath\\"+input.files[0]["name"]
    }
    return path
}

function validarInputFile(files) {
    // console.log(10)
    var resultado = true;
    $.each(files, function (key, file) {
        if (!(["image/x-png", "image/png", "image/jpeg", "image/jpg"].includes(file.type))) {
            resultado = false;
            return false;
        }
    })
    return resultado;
}

function validarInputFile2(files) {
    // console.log(10)
    var resultado = true;
    $.each(files, function (key, file) {
        if (!(["image/x-png", "image/png", "image/jpeg", "image/jpg","video/mp4"].includes(file.type))) {
            resultado = false;
            return false;
        }
    })
    return resultado;
}

function limpiar(){
    $("input,select,textarea").removeClass("is-invalid")
}

function ajax(){
    let ajax={
        $id:null,
        nuevaReceta:function(form){
            return $.ajax({
                url:"/app/ajax/recetas_admin/nuevaReceta.php",
                method:"POST",
                data:form,
                dataType:"JSON",
                processData: false,
                cache: false,
                contentType: false
            })
        },
        actualizarReceta:function(form){
            return $.ajax({
                url:"/app/ajax/recetas_admin/actualizarReceta.php",
                method:"POST",
                data:form,
                dataType:"JSON",
                processData: false,
                cache: false,
                contentType: false
            })
        },
        eliminarReceta:function(id){
            return $.ajax({
                url:"/app/ajax/recetas_admin/eliminarReceta.php",
                method:"POST",
                data:{id:id},
                dataType:"JSON"
            })
        },
        nuevoIconico:function(form){
            return $.ajax({
                url:"/app/ajax/iconicos_admin/nuevoIconico.php",
                method:"POST",
                data:form,
                dataType:"JSON",
                processData: false,
                cache: false,
                contentType: false
            })
        },
        actualizarIconico:function(form){
            return $.ajax({
                url:"/app/ajax/iconicos_admin/actualizarIconico.php",
                method:"POST",
                data:form,
                dataType:"JSON",
                processData: false,
                cache: false,
                contentType: false
            })
        },
        eliminarIconico:function(id){
            return $.ajax({
                url:"/app/ajax/iconicos_admin/eliminarIconico.php",
                method:"POST",
                data:{id:id},
                dataType:"JSON"
            })
        },
        nuevaNoticia:function(form){
            return $.ajax({
                url:"/app/ajax/noticias_admin/nuevaNoticia.php",
                method:"POST",
                data:form,
                dataType:"JSON",
                processData: false,
                cache: false,
                contentType: false
            })
        },
        actualizarNoticia:function(form){
            return $.ajax({
                url:"/app/ajax/noticias_admin/actualizarNoticia.php",
                method:"POST",
                data:form,
                dataType:"JSON",
                processData: false,
                cache: false,
                contentType: false
            })
        },
        eliminarNoticia:function(id){
            return $.ajax({
                url:"/app/ajax/noticias_admin/eliminarNoticia.php",
                method:"POST",
                data:{id:id},
                dataType:"JSON"
            })
        },
    }
    return ajax;
}