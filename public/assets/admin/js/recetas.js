$(document).ready(function(){
    let paginaReceta=0;
    let paginaIconico=0;
    let paginaNoticia=0;
    let iconDelete=`<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
  </svg>`;
    let iconEdit=`<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
  </svg>` 
    showLoading()
    let tRecetas=$("#tRecetas tbody");
    let tIconicos=$("#tIconicos tbody");
    let tNoticias=$("#tNoticias tbody");
    // let tContactanos=$("#tContactanos tbody");
    $("#btnNuevaReceta").on("click",function(){
        $("#mdt").html("NUEVA")
        LimpiarCajas();
        $("#modalEditar").modal("show")
        $(".c-btn").addClass("d-none")
        $(".c-btn-registrar").removeClass("d-none")
    })
    $("#btn-receta-registrar").on("click",function(){
        limpiar();
        let txtTitulo1=$("#txtTitulo1")
        let txtTitulo2=$("#txtTitulo2")
        let txtSubTitulo=$("#txtSubTitulo")
        let txtDuracion=$("#txtDuracion")
        let cboPersonas=$("#cboPersonas")
        let fileImgPlato=$("#fileImgPlato")
        let fileImgReceta=$("#fileImgReceta")
        let filePresentacion=$("#filePresentacion")
        let txtUrl=$("#txtUrl")
        let txtIngredientes1=$("#txtIngredientes1")
        let txtIngredientes2=$("#txtIngredientes2")
        let txtPreparacion=$("#txtPreparacion")
    
        var fileP = fileImgPlato.prop("files");
        var fileR = fileImgReceta.prop("files");
        var filePr = filePresentacion.prop("files");
        console.log(fileP)
        switch (false) {
            case ((txtDuracion.val())?true:false):
                txtDuracion.addClass("is-invalid").focus()
            break;
            case ((cboPersonas.val())?true:false):
                cboPersonas.addClass("is-invalid").focus()
            break;
            case (fileP.length && (fileP && validarInputFile(fileP))) ? true:false:
                // alert("falta Imagen")
                fileImgPlato.addClass("is-invalid").focus()
            break;
            case (fileR.length && (fileR && validarInputFile(fileR))) ? true:false:
                // alert("falta Imagen")
                fileImgReceta.addClass("is-invalid").focus()
            break;
            case (filePr.length && (filePr && validarInputFile2(filePr))) ? true:false:
                // alert("falta Imagen")
                filePresentacion.addClass("is-invalid").focus()
            break;
            case ((txtUrl.val())?true:false):
                txtUrl.addClass("is-invalid").focus()
            break;
            default:
                alertify.confirm("REGISTRAR RECETA","Se añadiran los datos ingresados <br> ¿Desea continuar?",
                async function(){
                    let formdata= new FormData();
                    formdata.append("txtTitulo1",txtTitulo1.val());
                    formdata.append("txtTitulo2",txtTitulo2.val());
                    formdata.append("txtSubTitulo",txtSubTitulo.val());
                    formdata.append("txtDuracion",txtDuracion.val());
                    formdata.append("cboPersonas",cboPersonas.val());
                    $.each(fileImgPlato.prop("files"), function (i, file) {
                        formdata.append(0, file);
                    });
                    $.each(fileImgReceta.prop("files"), function (i, file) {
                        formdata.append(1, file);
                    });
                    $.each(filePresentacion.prop("files"), function (i, file) {
                        formdata.append(2, file);
                    });
                    formdata.append("txtUrl",txtUrl.val());
                    formdata.append("txtIngredientes1",txtIngredientes1.val());
                    formdata.append("txtIngredientes2",txtIngredientes2.val());
                    formdata.append("txtPreparacion",txtPreparacion.val());
    
                    showLoading()
                    let aj = new ajax();
                    let json = await aj.nuevaReceta(formdata)
                    // if(!json.status){
                        if(json.res){
                            alertify.success('Se registro correctamente');
                            $("#modalEditar").modal("hide");
                            setTimeout(function(){
                                pagRecetas();
                                hideLoading();
                            },500)
                        }else{
                            alertify.error('No se pudo registrar');
                            hideLoading();
                        }
                    // }
                },
                function(){
    
                })
            break;
        }
    
    })
    $("#btn-receta-actualizar").on("click",function(){
        limpiar();
        let thisBtn=$(this);
        let txtTitulo1=$("#txtTitulo1")
        let txtTitulo2=$("#txtTitulo2")
        let txtSubTitulo=$("#txtSubTitulo")
        let txtDuracion=$("#txtDuracion")
        let cboPersonas=$("#cboPersonas")
        let fileImgPlato=$("#fileImgPlato")
        let fileImgReceta=$("#fileImgReceta")
        let filePresentacion=$("#filePresentacion")
        let txtUrl=$("#txtUrl")
        let txtIngredientes1=$("#txtIngredientes1")
        let txtIngredientes2=$("#txtIngredientes2")
        let txtPreparacion=$("#txtPreparacion")
    
        var fileP = fileImgPlato.prop("files");
        var fileR = fileImgReceta.prop("files");
        var filePr = filePresentacion.prop("files");
        // console.log(fileP)
        switch (false) {
            case ((txtDuracion.val())?true:false):
                txtDuracion.addClass("is-invalid").focus()
            break;
            case ((cboPersonas.val())?true:false):
                cboPersonas.addClass("is-invalid").focus()
            break;
            case (!changeImgPlato || (fileP.length && (fileP && validarInputFile(fileP)))) ? true:false:
                // alert("falta Imagen")
                fileImgPlato.addClass("is-invalid").focus()
            break;
            case (!changeImgReceta || (fileR.length && (fileR && validarInputFile(fileR)))) ? true:false:
                // alert("falta Imagen")
                fileImgReceta.addClass("is-invalid").focus()
            break;
            case (!changeFilePresentacion || (filePr.length && (filePr && validarInputFile2(filePr)))) ? true:false:
                // alert("falta Imagen")
                filePresentacion.addClass("is-invalid").focus()
            break;
            case ((txtUrl.val())?true:false):
                txtUrl.addClass("is-invalid").focus()
            break;
            default:
                alertify.confirm("ACTUALIZAR RECETA","Se actualizaran los datos de la receta <br> ¿Desea continuar?",
                async function(){
                    let formdata= new FormData();
                    formdata.append("id",$(thisBtn).attr("data-id"));
                    formdata.append("txtTitulo1",txtTitulo1.val());
                    formdata.append("txtTitulo2",txtTitulo2.val());
                    formdata.append("txtSubTitulo",txtSubTitulo.val());
                    formdata.append("txtDuracion",txtDuracion.val());
                    formdata.append("cboPersonas",cboPersonas.val());
                    if(changeImgPlato){
                        $.each(fileImgPlato.prop("files"), function (i, file) {
                            formdata.append(0, file);
                            formdata.append("img1",1)
                        });
                    }else{
                            formdata.append("img1",0)
                    }
                    if(changeImgReceta){
                        $.each(fileImgReceta.prop("files"), function (i, file) {
                            formdata.append(1, file);
                            formdata.append("img2",1)
                        });
                    }else{
                            formdata.append("img2",0)
                    }
                    if(changeFilePresentacion){
                        $.each(filePresentacion.prop("files"), function (i, file) {
                            formdata.append(2, file);
                            formdata.append("img3",1)
                        });
                    }else{
                            formdata.append("img3",0)
                    }
                    formdata.append("txtUrl",txtUrl.val());
                    formdata.append("txtIngredientes1",txtIngredientes1.val());
                    formdata.append("txtIngredientes2",txtIngredientes2.val());
                    formdata.append("txtPreparacion",txtPreparacion.val());
    
                    showLoading()
                    let aj = new ajax();
                    let json = await aj.actualizarReceta(formdata)
                    // if(!json.status){
                        if(json.res){
                            alertify.success('Se registro correctamente');
                            $("#modalEditar").modal("hide");
                            setTimeout(function(){
                                // pagRecetas();
                                $("#pag1").pagination(paginaReceta)
                                hideLoading();
                            },500)
                        }else{
                            alertify.error('No se pudo registrar');
                            hideLoading();
                        }
                    // }
                },
                function(){
    
                })
            break;
        }
    
    })
    pagRecetas();
    pagIconicos();
    pagNoticias();
    // pagContactanos();
    function pagRecetas(){
        showLoading();
        $("#pag1").pagination({
            dataSource: function(done) {
                $.ajax({
                    url:"/app/ajax/recetas_admin/readNumberRecetas.php",
                    method:"POST",
                    dataType:"JSON"
                }).done(function(json){
                    var result = [];
                    for (var i = 0; i < json.cantidad; i++) {
                        result.push(i);
                    }
                    done(result);
                })
            },
            header: function (currentPage, totalPage, totalNumber) {
                return "<div class='pagination-header'><b>Total:</b><span>&nbsp;" + totalNumber +
                    "&nbsp;</span></div>";
            },
            callback: function(data, pagination) {
                // console.log(pagination)
                paginaReceta=pagination.pageNumber
                showLoading();
                $.ajax({
                    url:"/app/ajax/recetas_admin/readTableRecetas.php",
                    method:"POST",
                    data:{pageNumber:pagination.pageNumber,pageSize:pagination.pageSize},
                    dataType:"JSON"
                }).done(function(json){
                    tRecetas.empty();
                    let n=pagination.totalNumber-(pagination.pageSize*(pagination.pageNumber-1))
                    $.each(json,function(k,v){
                        let tr=$("<tr>")
                        let tdNum=$("<td>",{text:n--,class:'text-center align-center'})
                        let btnVer=$("<button>",{html:iconEdit+"",
                                                style:"font-size:18px;line-height:18px",
                                                class:'btnEditar btn m-1 btn-success',
                                                "data-id":v.id})
                        let btnEliminar=$("<button>",{html:iconDelete+"",
                                                    style:"font-size:18px;line-height:18px",
                                                    class:'btnEliminar btn m-1 btn-danger',
                                                    "data-id":v.id})
                        let div=$("<div>",{class:'d-flex justify-content-center align-items-center'}).append(btnVer,btnEliminar)
                        let tdOpciones=$("<td>",{class:"text-center"}).append(div)
                        let tdTitulo=$("<td>",{class:"text-center",text:v.preTitulo+" "+v.titulo})
                        let tdSubTitulo=$("<td>",{class:"text-center",text:(v.subTitulo)?v.subTitulo:"--"})
                        let tdDetalles=$("<td>",{class:"text-center",text:v.duracion+", "+v.personas+" personas"})
                        // let tdVisualizar=$("<td>",{})
                        tr.append(tdNum,tdOpciones,tdTitulo,tdSubTitulo,tdDetalles)
                        tRecetas.append(tr)
                    })
                    eventEditar();
                    eventEliminar();
                    hideLoading();
                })
            }
        })
    }
    function eventEditar(){
        $(".btnEditar").on("click",function(){
            showLoading()
            $("#mdt").html("EDITAR")
            $(".c-btn").addClass("d-none")
            $(".c-btn-actualizar").removeClass("d-none")
            changeImgPlato=false;
            changeImgReceta=false;
            changeFilePresentacion=false;
            LimpiarCajas();
            let id=$(this).data("id")
            $("#btn-receta-actualizar").attr("data-id",id);
            $.ajax({
                url:"/app/ajax/recetas_admin/readReceta.php",
                method:"POST",
                data:{id:id},
                dataType:"JSON"
            }).done(function(json){
                if(!json.error){
                    // $.each(json,function(k,v){
                        $("#txtTitulo1").val(json.preTitulo)
                        $("#txtTitulo2").val(json.titulo)
                        $("#txtSubTitulo").val(json.subTitulo)
                        $("#txtDuracion").val(json.duracion)
                        $("#cboPersonas option[value='"+json.personas+"']").prop("selected",true)
                        let img1 = $("<img>",{src:"/public/img/data/"+json.imgPlato,style:"background:#fff"})
                        $(".container-image1").html(img1)
                        let img2 = $("<img>",{src:"/public/img/data/"+json.imgReceta,style:"background:#fff"})
                        $(".container-image2").html(img2)
                        
                        $("label[for=fileImgPlato]").html(json.imgPlatoReal)
                        $("label[for=fileImgReceta]").html(json.imgRecetaReal)
                        $("label[for=filePresentacion]").html(json.filePresentacionReal)
                        $("#txtIngredientes1").val(json.rightIngredientes)
                        $("#txtIngredientes2").val(json.leftIngredientes)
                        $("#txtPreparacion").val(json.preparacion)
                        $("#txtUrl").val(json.vdoReceta)
                    // })
                    setTimeout(function(){
                        hideLoading();
                        $("#modalEditar").modal("show")
                    },500)
                }else{

                }
            })
        })
    }
    function eventEliminar(){
        $(".btnEliminar").on("click",function(){
            let id=$(this).data("id")
            // console.log(id)
            alertify.confirm("CONFIRMACIÓN","Se eliminara la receta <br>¿Desea continuar?",
            async function(){
                showLoading();
                let aj = new ajax();
                let json = await aj.eliminarReceta(id)
                // if(!json.status){
                if(json.res){
                    alertify.success('Se elimino correctamente');
                    // $("#modalEditar").modal("hide");
                    setTimeout(function(){
                        // pagRecetas();
                        $("#pag1").pagination(paginaReceta)
                        hideLoading();
                    },500)
                }else{
                    alertify.error('No se pudo Eliminar');
                    hideLoading();
                }
            },
            function(){

            }).set('movable', false);
        })
    }
    function LimpiarCajas(){
        $("#modalEditar .modal-body input").val("")
        $("#modalEditar .modal-body textarea").val("")
        $("#modalEditar .modal-body select option:selected").prop("selected", false);
        $("#modalEditar .modal-body select option:first").prop("selected", true);
        $(".custom-file-label").html("Seleccionar")
        $(".container-image1").empty()
        $(".container-image2").empty()
    }
// ------------------------------------ICONICO------------------------------
    $("#btnNuevoIconico").on("click",function(){
        $("#mdtIco").html("NUEVO")
        LimpiarCajasIco();
        $("#modalEditarIco").modal("show")
        $(".c-btn").addClass("d-none")
        $(".c-btn-registrar").removeClass("d-none")
    })
    $("#btn-iconico-registrar").on("click",function(){
        limpiar();
        let txtTituloIco=$("#txtTituloIco")
        let txtDescripcionIco=$("#txtDescripcionIco")
        let fileImgPlato=$("#txtFileIco")
        var file = fileImgPlato.prop("files");
        switch (false) {
            case ((txtTituloIco.val())?true:false):
                txtTituloIco.addClass("is-invalid").focus()
            break;
            case ((txtDescripcionIco.val())?true:false):
                txtDescripcionIco.addClass("is-invalid").focus()
            break;
            case (file.length && (file && validarInputFile(file))) ? true:false:
                // alert("falta Imagen")
                fileImgPlato.addClass("is-invalid").focus()
            break;
            
            default:
                alertify.confirm("REGISTRAR ICÓNICO","Se añadiran los datos ingresados <br> ¿Desea continuar?",
                async function(){
                    showLoading()
                    let formdata= new FormData();
                    formdata.append("txtTitulo",txtTituloIco.val());
                    formdata.append("txtDescripcion",txtDescripcionIco.val());
                    $.each(fileImgPlato.prop("files"), function (i, file) {
                        formdata.append(0, file);
                    });
                    let aj = new ajax();
                    let json = await aj.nuevoIconico(formdata)
                    // if(!json.status){
                        if(json.res){
                            alertify.success('Se registro correctamente');
                            $("#modalEditarIco").modal("hide");
                            setTimeout(function(){
                                pagIconicos();
                                hideLoading();
                            },500)
                        }else{
                            alertify.error('No se pudo registrar');
                            hideLoading();
                        }
                    // }
                },
                function(){
    
                })
            break;
        }
    
    })
    $("#btn-iconico-actualizar").on("click",function(){
        limpiar();
        let thisBtn=$(this);
        let txtTituloIco=$("#txtTituloIco")
        let txtDescripcionIco=$("#txtDescripcionIco")
        let fileImgPlato=$("#txtFileIco")
        var file = fileImgPlato.prop("files");
    
        // console.log(fileP)
        switch (false) {
            case ((txtTituloIco.val())?true:false):
                txtTituloIco.addClass("is-invalid").focus()
            break;
            case ((txtDescripcionIco.val())?true:false):
                txtDescripcionIco.addClass("is-invalid").focus()
            break;
            case (!changeImgIconico || (file.length && (file && validarInputFile(file)))) ? true:false:
                // alert("falta Imagen")
                fileImgPlato.addClass("is-invalid").focus()
            break;
            default:
                alertify.confirm("ACTUALIZAR ICÓNICO","Se actualizaran los datos <br> ¿Desea continuar?",
                async function(){
                    showLoading()
                    let formdata= new FormData();
                    formdata.append("id",$(thisBtn).attr("data-id"));
                    formdata.append("txtTitulo",txtTituloIco.val());
                    formdata.append("txtDescripcion",txtDescripcionIco.val());
                    if(changeImgIconico){
                        $.each(fileImgPlato.prop("files"), function (i, file) {
                            formdata.append(0, file);
                            formdata.append("img1",1)
                        });
                    }else{
                            formdata.append("img1",0)
                    }
                    let aj = new ajax();
                    let json = await aj.actualizarIconico(formdata)
                    // if(!json.status){
                        if(json.res){
                            alertify.success('Se registro correctamente');
                            $("#modalEditarIco").modal("hide");
                            setTimeout(function(){
                                // pagRecetas();
                                $("#pag2").pagination(paginaIconico)
                                hideLoading();
                            },500)
                        }else{
                            alertify.error('No se pudo registrar');
                            hideLoading();
                        }
                    // }
                },
                function(){
    
                })
            break;
        }
    })
    function pagIconicos(){
        $("#pag2").pagination({
            dataSource: function(done) {
                $.ajax({
                    url:"/app/ajax/iconicos_admin/readNumberIconicos.php",
                    method:"POST",
                    dataType:"JSON"
                }).done(function(json){
                    var result = [];
                    for (var i = 0; i < json.cantidad; i++) {
                        result.push(i);
                    }
                    done(result);
                })
            },
            header: function (currentPage, totalPage, totalNumber) {
                return "<div class='pagination-header'><b>Total:</b><span>&nbsp;" + totalNumber +
                    "&nbsp;</span></div>";
            },
            callback: function(data, pagination) {
                // console.log(pagination)
                paginaIconico=pagination.pageNumber
                $.ajax({
                    url:"/app/ajax/iconicos_admin/readTableIconicos.php",
                    method:"POST",
                    data:{pageNumber:pagination.pageNumber,pageSize:pagination.pageSize},
                    dataType:"JSON"
                }).done(function(json){
                    tIconicos.empty();
                    let n=pagination.totalNumber-(pagination.pageSize*(pagination.pageNumber-1))
                    $.each(json,function(k,v){
                        let tr=$("<tr>")
                        let tdNum=$("<td>",{text:n--,class:'text-center align-center'})
                        let btnVer=$("<button>",{html:iconEdit+"",
                                                style:"font-size:18px;line-height:18px",
                                                class:'btnEditarIco btn m-1 btn-success',
                                                "data-id":v.id})
                        let btnEliminar=$("<button>",{html:iconDelete+"",
                                                    style:"font-size:18px;line-height:18px",
                                                    class:'btnEliminarIco btn m-1 btn-danger',
                                                    "data-id":v.id})
                        let div=$("<div>",{class:'d-flex justify-content-center align-items-center'}).append(btnVer,btnEliminar)
                        let tdOpciones=$("<td>",{class:"text-center"}).append(div)
                        let tdTitulo=$("<td>",{class:"text-center",text:v.titulo})
                        // let tdSubTitulo=$("<td>",{class:"text-center",text:(v.subTitulo)?v.subTitulo:"--"})
                        let tdDetalles=$("<td>",{class:"text-center ellipsis",text:v.descripcion,nowrap:''})
                        // let tdVisualizar=$("<td>",{})
                        tr.append(tdNum,tdOpciones,tdTitulo,tdDetalles)
                        tIconicos.append(tr)
                    })
                    eventEditarIco();
                    eventEliminarIco();
                    hideLoading();
                })
            }
        })
    }

    function eventEditarIco(){
        $(".btnEditarIco").on("click",function(){
            changeImgIconico=false;
            $("#mdtIco").html("EDITAR")
            $(".c-btn").addClass("d-none")
            $(".c-btn-actualizar").removeClass("d-none")
            LimpiarCajasIco();
            showLoading()
            let id=$(this).data("id")
            $("#btn-iconico-actualizar").attr("data-id",id);
            $.ajax({
                url:"/app/ajax/iconicos_admin/readIconico.php",
                method:"POST",
                data:{id:id},
                dataType:"JSON"
            }).done(function(json){
                if(!json.error){
                    $("#txtTituloIco").val(json.titulo)
                    let img3 = $("<img>",{src:"/public/img/data/"+json.imgPlato,style:"background:#fff"})
                    $(".container-image3").html(img3)
                    $("label[for=txtFileIco]").html(json.imgPlatoReal)
                    $("#txtDescripcionIco").val(json.descripcion)
                    // $("#txtUrl").val(json.vdoReceta)
                }else{

                }
                setTimeout(function(){
                    hideLoading();
                    $("#modalEditarIco").modal("show")
                },500)
            })
        })
    }
    function eventEliminarIco(){
        $(".btnEliminarIco").on("click",function(){
            let id=$(this).data("id")
            alertify.confirm("CONFIRMACIÓN","Se eliminara el plato icónico <br>¿Desea continuar?",
            async function(){
                showLoading();
                let aj = new ajax();
                let json = await aj.eliminarIconico(id)
                // if(!json.status){
                if(json.res){
                    alertify.success('Se elimino correctamente');
                    // $("#modalEditar").modal("hide");
                    setTimeout(function(){
                        // pagRecetas();
                        $("#pag2").pagination(paginaIconico)
                        hideLoading();
                    },500)
                }else{
                    alertify.error('No se pudo Eliminar');
                    hideLoading();
                }
            },
            function(){

            }).set('movable', false); 
        })
    }
    function LimpiarCajasIco(){
        $("#modalEditarIco .modal-body input").val("")
        $("#modalEditarIco .modal-body textarea").val("")
        $(".custom-file-label").html("Seleccionar")
        // $("#modalEditarIco .modal-body select:selected").prop("selected", false);
        $(".container-image3").empty()
    }

    // $()
    $("#btnNuevaNoticia").on("click",function(){
        $("#mdtNoti").html("NUEVA")
        LimpiarCajasNoti();
        $("#modalEditarNoti").modal("show")
        $(".c-btn").addClass("d-none")
        $(".c-btn-registrar").removeClass("d-none")
    })
    $("#btn-noticia-registrar").on("click",function(){
        limpiar();
        let txtTituloNoti=$("#txtTituloNoti")
        let txtSubtituloNoti=$("#txtSubtituloNoti")
        let txtFechaNoti=$("#txtFechaNoti")
        let txtDescripcionNoti=$("#txtDescripcionNoti")
        let fileImg1=$("#txtFileNoticiaPresentacion")
        let fileImg2=$("#txtFileNoticia")
        var f1 = fileImg1.prop("files");
        var f2 = fileImg2.prop("files");
        switch (false) {
            case ((txtTituloNoti.val())?true:false):
                txtTituloNoti.addClass("is-invalid").focus()
            break;
            case ((txtSubtituloNoti.val())?true:false):
                txtSubtituloNoti.addClass("is-invalid").focus()
            break;
            case ((txtFechaNoti.val())?true:false):
                txtFechaNoti.addClass("is-invalid").focus()
            break;
            case ((txtDescripcionNoti.val())?true:false):
                txtDescripcionNoti.addClass("is-invalid").focus()
            break;
            case (f1.length && (f1 && validarInputFile(f1))) ? true:false:
                // alert("falta Imagen")
                fileImg1.addClass("is-invalid").focus()
            break;
            case (f2.length && (f2 && validarInputFile(f2))) ? true:false:
                // alert("falta Imagen")
                fileImg2.addClass("is-invalid").focus()
            break;
            default:
                alertify.confirm("REGISTRAR NOTICIA","Se añadiran los datos ingresados <br> ¿Desea continuar?",
                async function(){
                    showLoading()
                    let formdata= new FormData();
                    formdata.append("txtTitulo",txtTituloNoti.val());
                    formdata.append("txtSubtitulo",txtSubtituloNoti.val());
                    formdata.append("txtFecha",txtFechaNoti.val());
                    formdata.append("txtDescripcion",txtDescripcionNoti.val());
                    $.each(fileImg1.prop("files"), function (i, file) {
                        formdata.append(0, file);
                    });
                    $.each(fileImg2.prop("files"), function (i, file) {
                        formdata.append(1, file);
                    });
                    let aj = new ajax();
                    let json = await aj.nuevaNoticia(formdata)
                    // if(!json.status){
                        if(json.res){
                            alertify.success('Se registro correctamente');
                            $("#modalEditarNoti").modal("hide");
                            setTimeout(function(){
                                pagNoticias();
                                hideLoading();
                            },500)
                        }else{
                            alertify.error('No se pudo registrar');
                            hideLoading();
                        }
                    // }
                },
                function(){
    
                })
            break;
        }
    
    })
    $("#btn-noticia-actualizar").on("click",function(){
        limpiar();
        let thisBtn=$(this);
        let txtTituloNoti=$("#txtTituloNoti")
        let txtSubtituloNoti=$("#txtSubtituloNoti")
        let txtFechaNoti=$("#txtFechaNoti")
        let txtDescripcionNoti=$("#txtDescripcionNoti")
        let fileImg1=$("#txtFileNoticiaPresentacion")
        let fileImg2=$("#txtFileNoticia")
        var f1 = fileImg1.prop("files");
        var f2 = fileImg2.prop("files");
    
        // console.log(fileP)
        switch (false) {
            case ((txtTituloNoti.val())?true:false):
                txtTituloNoti.addClass("is-invalid").focus()
            break;
            case ((txtSubtituloNoti.val())?true:false):
                txtSubtituloNoti.addClass("is-invalid").focus()
            break;
            case ((txtFechaNoti.val())?true:false):
                txtFechaNoti.addClass("is-invalid").focus()
            break;
            case ((txtDescripcionNoti.val())?true:false):
                txtDescripcionNoti.addClass("is-invalid").focus()
            break;
            case (!changeImgNoticia1 || (f1.length && (f1 && validarInputFile(f1)))) ? true:false:
                fileImg1.addClass("is-invalid").focus()
            break;
            case (!changeImgNoticia2 || (f2.length && (f2 && validarInputFile(f2)))) ? true:false:
                fileImg2.addClass("is-invalid").focus()
            break;
            default:
                alertify.confirm("ACTUALIZAR RECETA","Se actualizaran los datos de la receta <br> ¿Desea continuar?",
                async function(){
                    let formdata= new FormData();
                    formdata.append("id",$(thisBtn).attr("data-id"));
                    formdata.append("txtTitulo",txtTituloNoti.val());
                    formdata.append("txtSubtitulo",txtSubtituloNoti.val());
                    formdata.append("txtFecha",txtFechaNoti.val());
                    formdata.append("txtDescripcion",txtDescripcionNoti.val());
                    if(changeImgNoticia1){
                        $.each(f1.prop("files"), function (i, file) {
                            formdata.append(0, file);
                            formdata.append("img1",1)
                        });
                    }else{
                            formdata.append("img1",0)
                    }
                    if(changeImgNoticia2){
                        $.each(f2.prop("files"), function (i, file) {
                            formdata.append(1, file);
                            formdata.append("img2",1)
                        });
                    }else{
                            formdata.append("img2",0)
                    }
                   
                    showLoading()
                    let aj = new ajax();
                    let json = await aj.actualizarNoticia(formdata)
                    // if(!json.status){
                        if(json.res){
                            alertify.success('Se registro correctamente');
                            $("#modalEditarNoti").modal("hide");
                            setTimeout(function(){
                                // pagRecetas();
                                $("#pag3").pagination(paginaNoticia)
                                hideLoading();
                            },500)
                        }else{
                            alertify.error('No se pudo registrar');
                            hideLoading();
                        }
                    // }
                },
                function(){
    
                })
            break;
        }
    
    })
    function pagNoticias(){
        $("#pag3").pagination({
            dataSource: function(done) {
                $.ajax({
                    url:"/app/ajax/noticias_admin/readNumberNoticias.php",
                    method:"POST",
                    dataType:"JSON"
                }).done(function(json){
                    var result = [];
                    for (var i = 0; i < json.cantidad; i++) {
                        result.push(i);
                    }
                    done(result);
                })
            },
            header: function (currentPage, totalPage, totalNumber) {
                return "<div class='pagination-header'><b>Total:</b><span>&nbsp;" + totalNumber +
                    "&nbsp;</span></div>";
            },
            callback: function(data, pagination) {
                // console.log(pagination)
                paginaNoticia=pagination.pageNumber
                $.ajax({
                    url:"/app/ajax/noticias_admin/readTableNoticias.php",
                    method:"POST",
                    data:{pageNumber:pagination.pageNumber,pageSize:pagination.pageSize},
                    dataType:"JSON"
                }).done(function(json){
                    let n=pagination.totalNumber-(pagination.pageSize*(pagination.pageNumber-1))
                    if(!json.error){
                        tNoticias.empty();
                        moment.locale("es")
                        $.each(json,function(k,v){
                            let tr=$("<tr>")
                            let tdNum=$("<td>",{text:n--,class:'text-center align-center'})
                            let btnVer=$("<button>",{html:iconEdit+"",
                                                    style:"font-size:18px;line-height:18px",
                                                    class:'btnEditarNoti btn m-1 btn-success',
                                                    "data-id":v.id})
                            let btnEliminar=$("<button>",{html:iconDelete+"",
                                                        style:"font-size:18px;line-height:18px",
                                                        class:'btnEliminarNoti btn m-1 btn-danger',
                                                        "data-id":v.id})
                            let div=$("<div>",{class:'d-flex justify-content-center align-items-center'}).append(btnVer,btnEliminar)
                            let tdOpciones=$("<td>",{class:"text-center"}).append(div)
                            let tdTitulo=$("<td>",{class:"text-center",text:v.nombre})
                            // let tdSubTitulo=$("<td>",{class:"text-center",text:(v.subTitulo)?v.subTitulo:"--"})
                            let tdDetalles=$("<td>",{class:"text-center ellipsis",text:v.detalles,nowrap:''})
                            let tdFecha=$("<td>",{class:"text-center",text:moment(v.fecIngreso).format('D [de] MMMM [del] YYYY')})
                            tr.append(tdNum,tdOpciones,tdTitulo,tdDetalles,tdFecha)
                            tNoticias.append(tr)
                        })
                        eventEditarNoti();
                        eventEliminarNoti();
                    }
                    hideLoading();
                })
            }
        })
    }
    function eventEditarNoti(){
        $(".btnEditarNoti").on("click",function(){
            changeImgNoticia1=false;
            changeImgNoticia2=false;
            $("#mdtIco").html("EDITAR")
            $(".c-btn").addClass("d-none")
            $(".c-btn-actualizar").removeClass("d-none")
            LimpiarCajasNoti();
            showLoading()
            let id=$(this).data("id")
            $("#btn-noticia-actualizar").attr("data-id",id);
            $.ajax({
                url:"/app/ajax/noticias_admin/readNoticia.php",
                method:"POST",
                data:{id:id},
                dataType:"JSON"
            }).done(function(json){
                if(!json.error){
                    $("#txtTituloNoti").val(json.nombre)
                    $("#txtSubtituloNoti").val(json.subTitulo)
                    let img4 = $("<img>",{src:"/public/img/data/"+json.img1,style:"background:#fff"})
                    $(".container-image4").html(img4)
                    let img5 = $("<img>",{src:"/public/img/data/"+json.img2,style:"background:#fff"})
                    $(".container-image5").html(img5)
                    $("#txtDescripcionNoti").val(json.detalles)
                    $("label[for=txtFileNoticiaPresentacion]").html(json.img1Real)
                    $("label[for=txtFileNoticia]").html(json.img2Real)
                    $("#txtFechaNoti").val(json.fecIngreso);
                    // $("#txtUrl").val(json.vdoReceta)
                }else{

                }
                setTimeout(function(){
                    hideLoading();
                    $("#modalEditarNoti").modal("show")
                },500)
            })
        })
    }
    function LimpiarCajasNoti(){
        $("#modalEditarNoti .modal-body input").val("")
        $("#modalEditarNoti .modal-body textarea").val("")
        $(".custom-file-label").html("Seleccionar")
        // $("#modalEditarIco .modal-body select:selected").prop("selected", false);
        $(".container-image4").empty()
        $(".container-image5").empty()
    }
    function eventEliminarNoti(){
        $(".btnEliminarNoti").on("click",function(){
            let id=$(this).data("id")
            alertify.confirm("CONFIRMACIÓN","Se eliminara la noticia <br>¿Desea continuar?",
            async function(){
                showLoading();
                let aj = new ajax();
                let json = await aj.eliminarNoticia(id)
                // if(!json.status){
                if(json.res){
                    alertify.success('Se elimino correctamente');
                    // $("#modalEditar").modal("hide");
                    setTimeout(function(){
                        // pagRecetas();
                        $("#pag3").pagination(paginaNoticia)
                        hideLoading();
                    },500)
                }else{
                    alertify.error('No se pudo Eliminar');
                    hideLoading();
                }
            },
            function(){

            }).set('movable', false); 
        })
    }
    // function pagContactanos(){
    //     $("#pag4").pagination({
    //         dataSource: function(done) {
    //             $.ajax({
    //                 url:"/app/ajax/contactanos_admin/readNumberContactanos.php",
    //                 method:"POST",
    //                 dataType:"JSON"
    //             }).done(function(json){
    //                 var result = [];
    //                 for (var i = 0; i < json.cantidad; i++) {
    //                     result.push(i);
    //                 }
    //                 done(result);
    //             })
    //         },
    //         header: function (currentPage, totalPage, totalNumber) {
    //             return "<div class='pagination-header'><b>Total:</b><span>&nbsp;" + totalNumber +
    //                 "&nbsp;</span></div>";
    //         },
    //         callback: function(data, pagination) {
    //             // console.log(pagination)
    //             // paginaNoticia=pagination.pageNumber
    //             $.ajax({
    //                 url:"/app/ajax/contactanos_admin/readTableContactanos.php",
    //                 method:"POST",
    //                 data:{pageNumber:pagination.pageNumber,pageSize:pagination.pageSize},
    //                 dataType:"JSON"
    //             }).done(function(json){
    //                 let n=pagination.totalNumber-(pagination.pageSize*(pagination.pageNumber-1))
    //                 if(!json.error){
    //                     tContactanos.empty();
    //                     moment.locale("es")
    //                     $.each(json,function(k,v){
    //                         let tr=$("<tr>")
    //                         let tdNum=$("<td>",{text:n--,class:'text-center align-center'})
    //                         let btnVer=$("<button>",{html:iconEdit+"",
    //                                                 style:"font-size:18px;line-height:18px",
    //                                                 class:'btnVer btn m-1 btn-success',
    //                                                 "data-id":v.id,
    //                                                 "data-nombres":v.nombres,
    //                                                 "data-apellidos":v.apellidos,
    //                                                 "data-email":v.email,
    //                                                 "data-descripcion":v.descripcion,
    //                                                 "data-fecha":v.fechaIngreso})
    //                         // let btnEliminar=$("<button>",{html:iconDelete+"",
    //                         //                             style:"font-size:18px;line-height:18px",
    //                         //                             class:'btnEliminarNoti btn m-1 btn-danger',
    //                         //                             "data-id":v.id})
    //                         let div=$("<div>",{class:'d-flex justify-content-center align-items-center'}).append(btnVer)
    //                         let tdOpciones=$("<td>",{class:"text-center"}).append(div)
    //                         let tdNombres=$("<td>",{class:"text-center",text:v.nombres})
    //                         let tdApellidos=$("<td>",{class:"text-center",text:v.apellidos})
    //                         let tdDetalles=$("<td>",{class:"text-center ellipsis",text:v.descripcion,nowrap:''})
    //                         let tdFecha=$("<td>",{class:"text-center",text:moment(v.fechaIngreso).format('D [de] MMMM [del] YYYY')})
    //                         tr.append(tdNum,tdOpciones,tdNombres,tdApellidos,tdDetalles,tdFecha)
    //                         tContactanos.append(tr)
    //                     })
    //                     eventVer();
    //                 }
    //                 hideLoading();
    //             })
    //         }
    //     })
    // }
    // function eventVer(){
    //     $(".btnVer").on("click",function(){
    //         let nombres=$(this).attr("data-nombres")
    //         let apellidos=$(this).attr("data-apellidos")
    //         let email=$(this).attr("data-email")
    //         let descripcion=$(this).attr("data-descripcion")
    //         let fecha=$(this).attr("data-fecha")
    //         $("#txtNombresCon").val(nombres)
    //         $("#txtApellidosCon").val(apellidos)
    //         $("#txtCorreoCon").val(email)
    //         $("#txtNombresCon").val(nombres)
    //         $("#txtDescripcionCon").val(descripcion)
    //         $("#txtFechaCon").val(moment(fecha).format('D [de] MMMM [del] YYYY [|] h:mm:ss a'))
    //         $("#modalVerContacto").modal("show");
    //     })
    // }
})