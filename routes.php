<?php
   ob_start();
?>
    <link rel="stylesheet" href="/public/library/bootstrap/css/bootstrap.css">
    <script src="/public/library/bootstrap/js/jquery.min.js"></script>
    <script src="/public/library/bootstrap/js/popper.min.js"></script>
    <script src="/public/library/bootstrap/js/bootstrap.min.js"></script>
<?php
   $bootstrap=ob_get_clean();
   DEFINE("BOOTSTRAP",$bootstrap);
?>
<?php
   ob_start();
?>
    <link rel="stylesheet" href="/public/library/swiper/css/swiper.min.css">
    <script src="/public/library/swiper/js/swiper.min.js"></script>
<?php
   $swiper=ob_get_clean();
   DEFINE("SWIPER",$swiper);
?>
<?php
   ob_start();
?>
    <script src="/public/library/scrollreveal/scrollreveal.js"></script>
<?php
   $bootstrap=ob_get_clean();
   DEFINE("SCROLLREVEAL",$bootstrap);
?>
<?php
   ob_start();
?>
    <!-- <link rel="stylesheet" href="/public/library/videojs/video-js.css">
    <script src="/public/library/videojs/video-js.js"></script> -->
    <link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">
    <script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>
<?php
   $bootstrap=ob_get_clean();
   DEFINE("VIDEOJS",$bootstrap);
?>

<?php
   ob_start();
   ?>
       <link rel="stylesheet" href="/public/library/pagination/pagination.css?v=0.3"/>
       <script src="/public/library/pagination/pagination.min.js?v=0.1"></script>
   <?php
   $paf=ob_get_contents();
   ob_end_clean();
   DEFINE('PAGINATIONJS',$paf);
?>

<?php
   ob_start();
   ?>
       <link rel="stylesheet" href="/public/library/alertify/alertify.min.css?v=0.3"/>
       <script src="/public/library/alertify/alertify.min.js?v=0.1"></script>
   <?php
   $paf=ob_get_contents();
   ob_end_clean();
   DEFINE('ALERTIFYJS',$paf);
?>